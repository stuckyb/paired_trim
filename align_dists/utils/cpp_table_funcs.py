#!/usr/bin/python3

# Functions for parsing CSV probability table file names and converting them to
# C++ code.

import csv


MULTI_TABLE_COMMENT = (
"""// Default gap penalties and alignment score percentile cutoff tables for
// various gap penalties and percentiles.  For each table, cutoff values are
// provided for all sequence lengths from 1 to 100 (inclusive), and then for
// every multiple of 10 greater than 100, up to 1,000 (inclusive).
""")

SINGLE_TABLE_COMMENT = (
"""// Default gap penalty and corresponding alignment score percentile cutoff
// table for testing alignment significance.  Cutoff values are provided for
// all sequence lengths from 1 to 100 (inclusive), and then for every multiple
// of 10 greater than 100, up to 1,000 (inclusive).
""")

def parseTableFileName(fname):
    """
    Given a probability table CSV file name, returns a tuple containing
    (gap_penalty, percentile, sample_size).
    """
    numparts = fname.replace('.csv', '').split('-')
    vals = numparts[1].split('_')

    return (int(vals[0]) * -1, float('0.' + vals[1]), int(vals[2]))

def buildNumStrList(num_strs):
    """
    Generates a multi-line string representing a comma-separated list of
    numbers, with each line indented by 4 spaces and no line longer than 80
    characters.
    """
    result = ''
    cur_line = '    '
    for num_str in num_strs:
        if len(cur_line) + len(num_str) + 1 > 80:
            result += cur_line + '\n'
            cur_line = '    '

        cur_line += num_str + ','

    return result + cur_line[:-1]
        
def generateCode(fnames, ostream):
    if len(fnames) > 1:
        print(MULTI_TABLE_COMMENT, file=ostream)
    else:
        print(SINGLE_TABLE_COMMENT, file=ostream)

    for fname in fnames:
        g_penalty, percentile, samp_size = parseTableFileName(fname)

        print(
            '// {0} percentile, gap penalty = {1}, sample size = {2:,}.'.format(
            percentile, g_penalty, int(samp_size)),
            file=ostream
        )

        print(
            '#define DEFAULT_GAP_PENALTY {0}\n'.format(g_penalty), file=ostream
        )

        # Get the cutoff values.
        cvals = ['0']
        with open(fname) as fin:
            reader = csv.DictReader(fin)
            for row in reader:
                cvals.append(row['cutoff'])

        print(
            'const int PairedReadAligner::score_pr_table[{0}] = '
            '{{'.format(len(cvals)), file=ostream
        )
        print(buildNumStrList(cvals), file=ostream)
        print('};\n', file=ostream)

