#***
# Examine samples of alignment score distributions.
#***
data = read.csv('score_vals.csv')

# Sample size, mean, standard deviation.
nrow(data)
mean(data$score)
sd(data$score)

# Number of unique values.
length(unique(data$score))
# All unique values.
unique(data$score)

# Barplot of sample frequencies.
plot(as.factor(data$score))

# As the sequence length increases, the distribution appears to get close-ish
# to normal.
plot(sort(unique(data$score)), summary(as.factor(data$score)))
xfit = seq(min(data$score), max(data$score), length=100)
yfit = dnorm(xfit, mean=mean(data$score), sd=sd(data$score))
scalefact = max(summary(as.factor(data$score))) / max(yfit)
lines(xfit, yfit * scalefact, lwd=2, col='blue')
qqnorm(data$score)


#***
# Examine the distribution of alignment score cutoff values for various
# interior gap penalties and percentiles.  For sequence lengths > 100, the
# pattern appears to be almost perfectly linear and becomes a constant with
# gap penalties > 6.
#***
fnames = list.files('.', pattern='.*csv')
par(mfrow=c(length(fnames) / 4,4))
for (fname in fnames) {
    num_parts = sub('.csv', '', strsplit(fname, '-')[[1]][2])
    num_parts = strsplit(num_parts, '_')[[1]]
    gap_penalty = num_parts[1]
    percentile = num_parts[2]
    samp_size = num_parts[3]

    data = read.csv(fname)

    plot(
        data$seq_len, data$cutoff, t='l', xlab='Sequence length',
        ylab='Alignment score cutoff',
        main=paste0('Interior gap penalty: ', gap_penalty, '; Percentile: 0.', percentile)
    )

    # Use linear regression to analyze the pattern for sequence lengths > 100.
    # Linear regression models the pattern almost perfectly, at least for the
    # sequence lengths analyzed by simulation.  This strongly suggests we could
    # accurately estimate cutoff values for longer sequence lengths.
    data100 = data[data$seq_len >= 100,]
    mod = lm(cutoff ~ seq_len, data=data100)
    print(summary(mod))

    # Plot the regression line.
    xvals = seq(min(data100$seq_len), max(data100$seq_len), length=100)
    yvals = predict(mod, data.frame(seq_len=xvals))
    lines(xvals, yvals, lwd=2, col='blue')

    fstat = summary(mod)[['fstatistic']]
    pval = pf(fstat[1],fstat[2],fstat[3],lower.tail=F)
    text(
        20, max(data$cutoff) / 2, paste0(
            'R-squared: ', round(summary(mod)[['r.squared']], 3), '; p: ',
            round(pval, 4)
        ),
        pos=4
    )
}
