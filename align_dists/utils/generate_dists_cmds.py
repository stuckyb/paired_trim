#!/usr/bin/python3

# Generates sim_align_scores commands to create alignment score distribution
# tables.

from argparse import ArgumentParser
import os.path


argp = ArgumentParser(
    description='Generates sim_align_scores commands to create alignment '
    'score distribution tables.'
)
argp.add_argument(
    '-o', '--output_dir', type=str, required=False,
    default='.', help='The output directory for the tables (default: ".").'
)
argp.add_argument(
    '-p', '--sim_scores_path', type=str, required=False,
    default=None,
    help='The path to sim_align_scores (default: '
    '"SCRIPT_LOCATION/../../build/src/sim_align_scores").'
)
argp.add_argument(
    '-a', '--alpha', type=float, required=False,
    default=None,
    help='The value of alpha to use (default: range from 0.99 - 0.99999).'
)

args = argp.parse_args()

# Get all required directory and file paths.
script_dir = os.path.dirname(__file__)

sim_path = args.sim_scores_path
if sim_path is None:
    sim_path = os.path.normpath(
        os.path.join(script_dir, '../../build/src/sim_align_scores')
    )

if not(os.path.isfile(sim_path)):
    exit(
        '\nERROR: Could not find the distribution simulator at '
        '{0}.\n'.format(sim_path)
    )

if args.alpha is None:
    alphas = [0.99, 0.999, 0.9999, 0.99999]
else:
    alphas = [args.alpha]

g_penalties = [-6, -12, -18, -24, -30, -36, -42, -48, -96, -192]

cmd_template = sim_path + ' -g {0} -p {1} -z {2} -m 1000 > {3} &'
fname_template = 'align_score_table-{0}_{1}_{2}.csv'

for g_penalty in g_penalties:
    for alpha in alphas:
        if alpha > 0.9999:
            samp_size = 1000000
        else:
            samp_size = 100000

        fout_path = os.path.normpath(os.path.join(
            args.output_dir,
            fname_template.format(g_penalty * -1, str(alpha)[2:], samp_size)
        ))

        cmd_str = cmd_template.format(g_penalty, alpha, samp_size, fout_path)
        print(cmd_str)

    print()

