#!/usr/bin/python3

# Uses the probability cutoff CSV file tables to generate documented C++ code
# for testing various gap penalty/percentile combinations when implementing the
# hypothesis test that an alignment is an alignment of random sequences.

import sys
from cpp_table_funcs import parseTableFileName, generateCode


fnames = sys.argv[1:]

if len(fnames) < 1:
    exit('\nERROR: Please specify one or more CSV probability table files.\n')

# First sort the file names alphabetically, then sort them by gap penalty.
fnames.sort(reverse=True)
fnames.sort(reverse=True, key=lambda fname:
    parseTableFileName(fname)[0]
)

generateCode(fnames, sys.stdout)

