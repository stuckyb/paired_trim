#!/usr/bin/python3

from argparse import ArgumentParser
import os.path
import subprocess
import csv
import math


argp = ArgumentParser(
    description='Fits various classifier models using liblinear.'
)
argp.add_argument(
    '--liblinear_dir', type=str, required=True,
    help='The location of liblinear.'
)
argp.add_argument(
    '--libsvm_dir', type=str, required=True,
    help='The location of libsvm.'
)
argp.add_argument(
    '-o', '--output_dir', type=str, required=False, default=None,
    help='The output directory '
    '(default: "SCRIPT_LOCATION/../output/liblinear").'
)
argp.add_argument(
    'datafile', type=str, nargs='+',
    help='An input data file in libSVM format.'
)

args = argp.parse_args()

# Get all required directory and file paths.
script_dir = os.path.dirname(__file__)

output_dir = args.output_dir
if output_dir is None:
    output_dir = os.path.normpath(
        os.path.join(script_dir, '../output/liblinear')
    )

if not(os.path.exists(output_dir)):
    os.makedirs(output_dir)

train_path = os.path.join(args.liblinear_dir, 'train')
grid_path = os.path.join(args.libsvm_dir, 'tools/grid.py')

cv_cmd_template = grid_path + ' -log2c {3},{4},{5} -log2g null -v 10 -out {1} -svmtrain ' + train_path + ' -s {0} -B 1 -n 2 {2}'

train_cmd_template = train_path + ' -s {0} -c {1} -B 1 -n 4 {2} {3}'

predict_path = os.path.join(args.liblinear_dir, 'predict')
predict_cmd_template = predict_path + ' {0} {1} {2}'

model_nums = [0, 2, 5, 6]

csv_path = os.path.join(output_dir, 'results.csv')
csv_out = open(csv_path, 'w')
writer = csv.DictWriter(csv_out, ['data_file', 'model', 'c', 'err_cnt'])
writer.writeheader()
row_out = {}

for df_path in args.datafile:
    output_subdir = os.path.join(
        output_dir, os.path.splitext(os.path.basename(df_path))[0]
    )
    if not(os.path.exists(output_subdir)):
        os.mkdir(output_subdir)

    sumf_path = os.path.join(output_subdir, 'cv_results.txt')
    sumf_out = open(sumf_path, 'w')

    for model_num in model_nums:
        # Use cross-validation to estimate C.
        print(
            'Coarse estimation of C for model {0} via '
            'cross-validation...'.format(model_num)
        )

        fout_path = os.path.join(
            output_subdir, 'cv-model_{0}.txt'.format(model_num)
        )
        cv_cmd = cv_cmd_template.format(
            model_num, fout_path, df_path, -35, 14, 1
        )

        result = subprocess.run(
            cv_cmd, shell=True, check=True, stdout=subprocess.PIPE,
            universal_newlines=True
        )

        sumf_out.write(result.stdout)

        # Get the value of C from the cross-validation results.
        cv_lines = result.stdout.splitlines()
        c_est = cv_lines[-1].split(' ')[0]

        # Now do a fine-grained search for C.
        fout_path = os.path.join(
            output_subdir, 'cv-model_{0}-fine_grained.txt'.format(model_num)
        )
        mid_val = round(math.log2(float(c_est)))
        cv_cmd = cv_cmd_template.format(
            model_num, fout_path, df_path, mid_val - 2, mid_val + 2, 0.25
        )

        print(
            'Fine-grained estimation of C for model {0} via cross-validation '
            'over the range 2^[{1},{2}]...'.format(
                model_num, mid_val-2, mid_val+2
            )
        )

        result = subprocess.run(
            cv_cmd, shell=True, check=True, stdout=subprocess.PIPE,
            universal_newlines=True
        )

        sumf_out.write(result.stdout)

        # Get the value of C from the cross-validation results.
        cv_lines = result.stdout.splitlines()
        c_est = cv_lines[-1].split(' ')[0]

        print(
            'Fitting full model for model {0} with C = '
            '{1}...'.format(model_num, c_est)
        )

        # Fit the model with the full data set, using the estimated value of C.
        model_path = os.path.join(
            output_subdir, 'model_{0}.fit'.format(model_num)
        )
        model_stdout_path = os.path.join(
            output_subdir, 'model_{0}-stdout.txt'.format(model_num)
        )
        train_cmd = train_cmd_template.format(
            model_num, c_est, df_path, model_path
        )

        result = subprocess.run(
            train_cmd, shell=True, check=True, stdout=subprocess.PIPE,
            universal_newlines=True
        )
        with open(model_stdout_path, 'w') as fout:
            fout.write(result.stdout)

        # Get the full model's accuracy.
        pvals_path = os.path.join(
            output_subdir, 'model_{0}-predictions.txt'.format(model_num)
        )
        predict_cmd = predict_cmd_template.format(
            df_path, model_path, pvals_path
        )
        result = subprocess.run(
            predict_cmd, shell=True, check=True, stdout=subprocess.PIPE,
            universal_newlines=True
        )
        print(result.stdout)

        # Extract the accuracy and calculate the error count.
        acc_str = result.stdout.splitlines()[0].split(' ')[-1]
        acc_parts = acc_str.replace('(', '').replace(')', '').split('/')
        err_cnt = int(acc_parts[1]) - int(acc_parts[0])

        row_out['data_file'] = df_path
        row_out['model'] = model_num
        row_out['c'] = c_est
        row_out['err_cnt'] = err_cnt
        writer.writerow(row_out)

    sumf_out.close()

