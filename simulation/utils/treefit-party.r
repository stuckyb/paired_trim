#!/usr/bin/Rscript

library('dplyr')
library('party')

source('utils/data_funcs.r')


input_dir = 'output/trimmed_reads'
input_rds = file.path(input_dir, 'learning_data.rds')

output_dir = 'output/tree_fit-party'
if (!dir.exists(output_dir)) {
    dir.create(output_dir)
}

tr_data = readRDS(file=input_rds)

cat('\n\n** All alignment-trimmable instances **')
# Paired reads with significant alignments that are *not* alignment-trimmable
# (i.e., when there is no alignment overhang to trim) almost never require 
# trimming, so we now focus on cases where the sequences could be trimmed.
cat(paste0(
    '\n', nrow(tr_data),
    ' records to analyze after removing untrimmable cases.'
))
cat(
    '\nStarting accuracy:',
    sum(tr_data$trim_required == tr_data$seq_trimmed) / nrow(tr_data)
)
cat('\nConfusion matrix:\n')
print(table(select(tr_data, seq_trimmed, trim_required)))


outputTree = function(est_tree, tname, output_dir, data, tr_pred) {
    cat('\n\n**', tname, '**\n')
    cat('\nNo. of leaf nodes:', length(unique(where(est_tree))))

    # Write the tree decision rules.
    tr_path = file.path(output_dir, paste0(tname, '-rules.txt'))
    sink(tr_path)
    print(est_tree)
    sink()
    
    # Generate a graph of the tree.
    tg_path = file.path(output_dir, paste0(tname, '-graph.svg'))
    svg(filename=tg_path, width=24, height=14)
    plot(est_tree)
    dev.off()
    
    # Calculate the confusion matrix and accuracy.
    y = response(est_tree)$trim_required
    cat('\nAccuracy:', mean(tr_pred == y))
    cat('\nConfusion matrix:\n')
    print(table(data.frame('predict'=tr_pred, 'trim_required'=y)))

    # Write a report of the reads that were incorrectly classified.
    fr_path = file.path(output_dir, paste0(tname, '-incorrect.csv'))
    write.csv(data[tr_pred != y,], fr_path, row.names=FALSE)

    return(sum(tr_pred != y))
}


# Convert factors to numeric values (N == 0, Y == 1).
tr_data$align_trim_mismatch = as.numeric(tr_data$align_trim_mismatch) - 1

# Explore different alternative alignment hyperparameter values to inspect
# their impact on cross-validation accuracy.
alt_align_lens= c(16, 18, 20, 22, 24)
alt_align_tols = c(0, 1, 2)
# Even with an extremely stringent alpha, the tree still grows quite large
# (e.g., 50-90 leaf nodes).
alpha = 0.99999
# So we instead explore explicitly limiting the tree height.  0 means the tree
# height is unconstrained.
maxdepths = c(0, 3, 4, 5, 6, 8)

alt_align_results = NULL

for (alt_align_len in alt_align_lens) {
    for (alt_align_tol in alt_align_tols) {
        for (maxdepth in maxdepths) {
            alt_align_col = paste0('X', alt_align_len, '_', alt_align_tol)

            tr_data$alt_align_possible = as.numeric(tr_data[[alt_align_col]]) - 1

            tr_data = addFeatureCrosses(tr_data)

            # There seems to be a really weird bug in party such that including
            # both of the factors align_trim_mismatch and alt_align_possible,
            # or including all of the feature crosses, always results in a tree
            # with nothing but a root node.  Evidence from other analyses
            # suggests that align_trim_mismatch is the less useful of the two,
            # so it is not included in the model formula, and we only include
            # the interactions identified as useful from CART tree estimation.
            est_tree = ctree(
                trim_required ~ read_len + align_ident + align_len_prop +
                    align_score_norm +
                    alt_align_possible + adapt_align_len_prop +
                    adapt_align_score_norm + adapt_align_ident +
                    align_len_prop.adapt_align_len_prop +
                    align_score_norm.adapt_align_score_norm +
                    align_trim_mismatch.adapt_align_score_norm,
                    data=tr_data,
                control=ctree_control(
                    mincriterion=alpha, maxdepth=maxdepth, minbucket=20,
                    savesplitstats=FALSE
                )
            )
            
            tr_pred = predict(est_tree)
            misclass_cnt = sum(tr_pred != response(est_tree)$trim_required)
            
            outputTree(
                est_tree, paste0(alt_align_col, '-', maxdepth), output_dir,
                tr_data, tr_pred
            )
            
            alt_align_results = rbind(
                alt_align_results, data.frame(
                    alt_align_len=alt_align_len,
                    tolerance=alt_align_tol,
                    max_depth=maxdepth,
                    num_leaf_nodes=length(unique(where(est_tree))),
                    errors=misclass_cnt
                )
            )

            rm(est_tree)
        }
    }
}

aar_path = file.path(output_dir, 'fit_results.csv')
write.csv(alt_align_results, file=aar_path, row.names=FALSE)

