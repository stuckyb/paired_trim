#!/usr/bin/python3

import sys
from argparse import ArgumentParser
import os.path
from trim_stats import TrimStats


# Analyzes detailed performance characteristics of read trimming operations by
# inspecting reads in one or more FASTQ files with trimmed sequencing reads.
# The title lines of the reads must conform to the following format:
#
# @[A-z0-9:]+START_POS:END_POS .*
#
# where START_POS and END_POS are integers that indicate the inclusive starting
# and ending positions of the actual insert sequence in the source genome.
#
# If the input file names are formatted like this:
#
# READ_LEN-MEAN_INSERT_LEN-SD_INSERT_LEN-.*
#
# then the read length, mean insert length, and insert length standard
# deviation will automatically be parsed from the file name.


def getNextRead(f_in):
    """
    Gets the next read from a FASTQ file.  Returns None on EOF or failure.
    """
    lines = []
    for cnt in range(4):
        lines.append(f_in.readline())

    if '' in lines:
        return None
    else:
        return lines

def getFileInfo(file_path, args):
    """
    Returns a tuple containing
    (read_length, mean_insert_length, standard_dev_insert_length).  If
    possible, these values will be parsed from the provided file name.  If that
    fails, getFileInfo() will attempt to get them from the command-line
    arguments.  Empty strings will be returned as the value for any parameters
    that cannot be determined using either method.
    """
    read_len = mean_ins_len = sd_ins_len = ''

    fname = os.path.basename(file_path)
    parts = fname.split('-')

    success = False
    if len(parts) > 2:
        try:
            read_len = int(parts[0])
            mean_ins_len = int(parts[1])
            sd_ins_len = int(parts[2])
            success = True
        except:
            success = False

    if not(success):
        if args.read_length is not None:
            read_len = args.read_length
        if args.mean_insert_size is not None:
            mean_ins_len = args.mean_insert_size
        if args.insert_std_dev is not None:
            sd_ins_len = args.insert_std_dev

    return (read_len, mean_ins_len, sd_ins_len)


argp = ArgumentParser(
    description='Calculates statistics to estimate the effectiveness of '
    'adapter-trimming methods.'
)
argp.add_argument(
    '-l', '--read_length', type=int, required=False,
    help='The sequence read length.  This setting will only be used if the '
    'read length cannot be parsed from the FASTQ file name.'
)
argp.add_argument(
    '-s', '--mean_insert_size', type=int, required=False,
    help='The mean actual insert size.  This setting will only be used if the '
    'mean insert size cannot be parsed from the FASTQ file name.'
)
argp.add_argument(
    '-d', '--insert_std_dev', type=int, required=False,
    help='The insert size standard deviation.  This setting will only be used '
    'if the insert standard deviation cannot be parsed from the FASTQ file '
    'name.'
)
argp.add_argument(
    '--seq_report', type=str, required=False, default='',
    help='A file path to use for saving a sequence-level results report in '
    'CSV format.'
)
argp.add_argument(
    '--file_report', type=str, required=False, default='',
    help='A file path to use for saving a file-level results report in CSV '
    'format.'
)
argp.add_argument(
    'fastq_input', type=str, nargs='+', help='An input FASTQ file.'
)

args = argp.parse_args()

f_out_seq_r = None
f_out_file_r = None
if args.seq_report != '':
    f_out_seq_r = open(args.seq_report, 'w')
if args.file_report != '':
    f_out_file_r = open(args.file_report, 'w')

total_reads = 0
row_out = {}
stats_per_file = TrimStats(
    args.read_length, f_out_seq_r=f_out_seq_r, f_out_file_r=f_out_file_r
)
stats_global = TrimStats(
    args.read_length, file_path='OVERALL', f_out_file_r=f_out_file_r,
    write_csv_headers=False
)

for fastq_path in args.fastq_input:
    print('Analyzing {0}...'.format(fastq_path))

    with open(fastq_path) as f_in:
        read_len, mean_ins_len, sd_ins_len = getFileInfo(fastq_path, args)
        if read_len == '':
            exit(
                '\nERROR: No read length was provided for the file '
                '"{0}".\n'.format(fastq_path)
            )
        stats_per_file.reset(read_len, mean_ins_len, sd_ins_len, fastq_path)
        stats_global.setReadLength(read_len)

        lines = getNextRead(f_in)
        while lines is not None:
            # Calculate the expected trimmed read size.
            parts = lines[0].split('insert_size=')
            insert_len = int(parts[1])
            # The following lines are for PERsim data.
            #parts = lines[0].split(' ')
            #parts = parts[0].split(':')
            #exp_len = int(parts[-1]) - int(parts[-2]) + 1

            # The sequence data line.
            actual_len = len(lines[1].strip())
            #print(exp_len, actual_len)

            stats_per_file.addRead(
                insert_len, actual_len, lines[0].strip()[1:]
            )
            stats_global.addRead(insert_len, actual_len)

            total_reads += 1

            # Read the next record.
            lines = getNextRead(f_in)

stats_per_file.complete()
stats_global.complete(print_seq_len_info=False)
stats_global.printReport()

