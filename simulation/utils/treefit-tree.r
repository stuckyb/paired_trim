#!/usr/bin/Rscript

library('dplyr')
library('ggplot2')
library('tree')

source('utils/data_funcs.r')


input_dir = 'output/trimmed_reads'
input_rds = file.path(input_dir, 'learning_data.rds')

output_dir = 'output/tree_fit-tree'
if (!dir.exists(output_dir)) {
    dir.create(output_dir)
}

tr_data = readRDS(file=input_rds)

cat('\n\n** All alignment-trimmable instances **')
cat(paste0(
    '\n', nrow(tr_data),
    ' records to analyze after removing untrimmable cases.'
))
cat(
    '\nStarting accuracy:',
    sum(tr_data$trim_required == tr_data$seq_trimmed) / nrow(tr_data)
)
cat('\nConfusion matrix:\n')
print(table(select(tr_data, seq_trimmed, trim_required)))

# Shuffle the rows to ensure we get random samples for cross-validation.
tr_data = sample_n(tr_data, size=nrow(tr_data), replace=FALSE)


outputTree = function(ctree, tname, output_dir, data) {
    cat('\n\n**', tname, '**\n')
    print(summary(ctree))

    # Write the tree decision rules.
    tr_path = file.path(output_dir, paste0(tname, '-rules.txt'))
    sink(tr_path)
    print(ctree)
    sink()
    
    # Generate a graph of the tree.
    tg_path = file.path(output_dir, paste0(tname, '-graph.svg'))
    svg(filename=tg_path, width=24, height=14)
    plot(ctree, type='uniform')
    text(ctree, pretty=0)
    dev.off()
    
    # Calculate the confusion matrix and accuracy.
    tr_pred = predict(ctree, type='class')
    cat('\nAccuracy:', mean(tr_pred == ctree$y))
    cat('\nConfusion matrix:\n')
    print(table(data.frame('predict'=tr_pred, 'trim_required'=ctree$y)))

    # Write a report of the reads that were incorrectly classified.
    fr_path = file.path(output_dir, paste0(tname, '-incorrect.csv'))
    write.csv(data[tr_pred != ctree$y,], fr_path, row.names=FALSE)
}

doTreeCV = function(ctree, tname, output_dir, data) {
    # Estimate the optimal tree complexity using cost-complexity pruning and
    # cross-validation.
    cv_res = cv.tree(ctree, FUN=prune.misclass, K=10)

    cv_path = file.path(output_dir, paste0(tname, '-cv_results.txt'))
    sink(cv_path)
    print(cv_res)
    sink()

    cvg_path = file.path(output_dir, paste0(tname, '-cv_results.png'))
    ggplot(
        data=data.frame('size'=cv_res$size, 'dev'=cv_res$dev),
        aes(x=size, y=dev)
    ) + geom_point(alpha=0.6) + geom_line() + xlab('No. of leaf nodes') +
        ylab('Sum of cross-validation misclassifications') + theme_bw()
    ggsave(cvg_path, width=5, height=4)

    # Get the index of the smallest tree size with minimum cross-validation error.
    i = tail(which(cv_res$dev == min(cv_res$dev)), n=1)
    opt_size = cv_res$size[i]
    
    cat('Optimal CV tree size (# of leaf nodes):', opt_size)

    # Prune the tree and inspect the results.
    pruned_tree = prune.misclass(ctree, best=opt_size)
    outputTree(pruned_tree, tname, output_dir, data)

    return(pruned_tree)
}


# Convert factors to numeric values (N == 0, Y == 1).
tr_data$align_trim_mismatch = as.numeric(tr_data$align_trim_mismatch) - 1

# Explore different alternative alignment hyperparameter values to inspect
# their impact on cross-validation accuracy.
alt_align_lens= c(16, 18, 20, 22, 24)
alt_align_tols = c(0, 1, 2)

alt_align_results = NULL

for (alt_align_len in alt_align_lens) {
    for (alt_align_tol in alt_align_tols) {
        alt_align_col = paste0('X', alt_align_len, '_', alt_align_tol)

        tr_data$alt_align_possible = as.numeric(tr_data[[alt_align_col]]) - 1

        tr_data = addFeatureCrosses(tr_data)

        # Build an (almost) unconstrained tree with all predictors.
        ctree = tree(
            trim_required ~ read_len + align_ident + align_len_prop +
                align_score_norm + align_trim_mismatch +
                alt_align_possible + adapt_align_len_prop +
                adapt_align_score_norm + adapt_align_ident +
                align_len_prop.adapt_align_len_prop +
                align_score_norm.adapt_align_score_norm +
                align_trim_mismatch.align_score_norm +
                align_trim_mismatch.adapt_align_score_norm +
                alt_align_possible.align_score_norm +
                alt_align_possible.align_len_prop +
                alt_align_possible.adapt_align_score_norm +
                alt_align_possible.adapt_align_len_prop,
            control=tree.control(nrow(tr_data), mindev=0, minsize=20),
            data=tr_data
        )
        
    #    library('rpart')
    #    ctree = rpart(
    #        trim_required ~ align_ident + align_len_prop + alt_align_possible + 
    #            align_score_norm + read_len + adapt_align_len_prop +
    #            adapt_align_score_norm, data=tr_data, method='class',
    #            control=rpart.control(cp=0, minbucket=4, usesurrogate=0, xval=10)
    #    )
    #    ctree
    #    summary(ctree)
    #    printcp(ctree)
    #    plotcp(ctree)
        
        outputTree(
            ctree, paste0('basic-unconstrained-', alt_align_col), output_dir,
            tr_data
        )
        pruned_tree = doTreeCV(
            ctree, paste0('basic-CV_optimal-', alt_align_col), output_dir, tr_data
        )
        
        alt_align_results = rbind(
            alt_align_results, data.frame(
                align_len=alt_align_len,
                tolerance=alt_align_tol,
                unconstrained_errors=summary(ctree)$misclass[1],
                unconstrained_leaf_nodes=length(unique(ctree$where)),
                cv_optimal_errors=summary(pruned_tree)$misclass[1],
                cv_num_leaf_nodes=length(unique(pruned_tree$where))
            )
        )

        rm(ctree)
        rm(pruned_tree)
    }
}

aar_path = file.path(output_dir, 'fit_results.csv')
write.csv(alt_align_results, file=aar_path, row.names=FALSE)

