#
# Functions used by multiple utility programs.
#

import os.path
import glob


def getReadsFiles(reads_dir):
    """
    Returns a list of matched forward/reverse read file pairs.
    """
    fnames = glob.glob(os.path.join(reads_dir, '*.fastq'))

    # First sort the files alphabetically, then by read length.
    fnames.sort()
    fnames.sort(key=lambda fname: int(os.path.basename(fname).split('-')[0]))

    if (len(fnames) %2 != 0):
        raise Exception('Invalid number of simulated read files.')

    pairs = []
    for cnt in range(0, len(fnames), 2):
        prefix1 = os.path.basename(fnames[cnt]).rsplit('-', 1)[0]
        prefix2 = os.path.basename(fnames[cnt + 1]).rsplit('-', 1)[0]

        suffix1 = os.path.splitext(os.path.basename(fnames[cnt]))[0].split('-')[-1]
        suffix2 = os.path.splitext(os.path.basename(fnames[cnt + 1]))[0].split('-')[-1]

        if (
            prefix1 != '' and prefix1 == prefix2 and
            suffix1 == 'R1' and suffix2 == 'R2'
        ):
            pairs.append((fnames[cnt], fnames[cnt + 1]))
        else:
            raise Exception(
                'Simulated read files {0} and {1} do not appear to be '
                'matching forward/reverse read files.'.format(
                    fnames[cnt], fnames[cnt + 1]
                )
            )

    return pairs

