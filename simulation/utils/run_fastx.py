#!/usr/bin/python3

# fastx_clipper automatically discards all sequences it thinks are
# adapter-only, and there is no way to force it to save them, so this script
# runs fastx_clipper twice -- first, using normal trimming, and second, in
# adapter-only report mode -- then combines the results into a single FASTQ
# file for analysis.  It also adds two required options:
#   --dir_out: Sets the output directory so for automatically generating the
#     output paths for fastx_clipper; and
#   --cmd_path: The path to fastx_clipper.
# All other command-line options will be passed on unmodified to fastx_clipper.


import subprocess
import sys
import os.path


# Get the location and name of the fastx_clipper path, the location and name of
# the output directory, and the name of the input file in the argument list.
cmd_path = ''
cmd_path_i = 0
dirout = ''
dirout_i = 0
fin_path = ''
for i in range(len(sys.argv)):
    if sys.argv[i] == '--cmd_path':
        cmd_path_i = i
        cmd_path = sys.argv[i + 1]
    elif sys.argv[i] == '--dir_out':
        dirout_i = i
        dirout = sys.argv[i + 1]
    elif sys.argv[i] == '-i':
        fin_path = sys.argv[i + 1]

if cmd_path == '':
    exit('\nCould not detect the fastx_clipper path.\n')
if dirout == '':
    exit('\nCould not detect the output directory path.\n')
if fin_path == '':
    exit('\nCould not detect the input file path.\n')

parts = os.path.splitext(os.path.basename(fin_path))
fout_path = os.path.join(dirout, parts[0] + '-fastx_clipper' + parts[1])
fout_adapt_path = os.path.join(
    dirout, parts[0] + '-fastx_clipper-adapter_only' + parts[1]
)
#print(dirout, fin_path)
#print(fout_path, fout_adapt_path)

args_list = [cmd_path] + sys.argv[1:]
#print(args_list)
i_sorted = sorted([cmd_path_i, dirout_i])
args_list = args_list[:i_sorted[1]] + args_list[i_sorted[1] + 2:]
args_list = args_list[:i_sorted[0]] + args_list[i_sorted[0] + 2:]
#print(args_list)

print('Generating trimmed reads...')
subprocess.run(args_list + ['-o', fout_path])

print('Generating adapter-only sequence report...')
subprocess.run(args_list + ['-o', fout_adapt_path, '-k'])

print('Merging adapter-only report with trimmed reads...')
with open(fout_path, 'a') as fout, open(fout_adapt_path) as fin:
    line = fin.readline()
    while line != '':
        # The sequence title.
        fout.write(line)

        # Truncate the sequence data line to ''.
        fin.readline()
        fout.write('\n')

        # The '+' line.
        fout.write(fin.readline())

        # The quality scores line.
        fin.readline()
        fout.write('\n')

        # Read the first line of the next record.
        line = fin.readline()

os.unlink(fout_adapt_path)

