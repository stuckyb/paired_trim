library('dplyr')
library('tidyr')
library('ggplot2')


# Analyze the results for conditional inference trees.
data = read.csv('output/tree_fit-party/fit_results.csv')
colnames(data)

ggplot(data=data, aes(x=alt_align_len, y=errors, color=as.factor(tolerance))) +
    geom_point() + geom_line() + facet_wrap(vars(max_depth))
    

# Analyze the results for CART trees.
data = read.csv('output/tree_fit-tree/fit_results.csv')
colnames(data)

data = gather(
    data, key='constraint', value='errors',
    cv_optimal_errors, unconstrained_errors
)
ggplot(data=data, aes(x=align_len, y=errors, color=as.factor(tolerance))) +
    geom_point() + geom_line() + facet_wrap(vars(constraint), labeller=label_both)
    

# Analyze the liblinear results.
data = read.csv('output/liblinear/results.csv')
colnames(data)

ggplot(
    data=data,
    aes(x=as.factor(alt_align_len), y=err_cnt, fill=as.factor(model))
) +
    geom_col(position=position_dodge2()) +
    facet_wrap(vars(data_file), scales='free', labeller=label_both)

