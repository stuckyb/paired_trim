#!/usr/bin/python3

# Given a file containing a FASTA sequence, this script replaces any ambiguous
# nucleotide codes with a randomly chosen unambiguous code that is compatible
# with the ambiguous code.

import sys
import random


AMBIG_CODES = {
    'R': 'AG',
    'Y': 'CT',
    'S': 'GC',
    'W': 'AT',
    'K': 'GT',
    'M': 'AC',
    'B': 'CGT',
    'D': 'AGT',
    'H': 'ACT',
    'V': 'ACG',
    'N': 'ACGT'
}

if len(sys.argv) < 2:
    exit('\nERROR: Please provide a FASTA input file.\n')

with open(sys.argv[1]) as fin:
    for line in fin:
        line = line.strip()

        if line.startswith('>'):
            print(line)
        else:
            adj_line = ''
            for code in line:
                if code in AMBIG_CODES:
                    adj_line += random.choice(AMBIG_CODES[code])
                else:
                    adj_line += code

            print(adj_line)

