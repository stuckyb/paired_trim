
import csv


class TrimStats:
    """
    Calculates detailed statistics for sequence trimming operations, generates
    a summary report, and can optionally generate one or two report files in
    CSV format.  The first report file option is a sequence-level report, and
    the generated file will include the following columns:

    'file': The input FASTQ file name, if known.
    'sequence': The sequence ID, if known.
    'read_len': The raw read length.
    'mean_insert_len': The mean insert length of the read-generating model, if
        known.
    'sd_insert_len': The insert length standard deviation of the
        read-generating model, if known.
    'insert_len': The actual insert length of the sequence.
    'trimmed_len': The trimmed read length.
    'trim_required': 'Y' if trimming was actually required, 'N' otherwise.
    'seq_trimmed': 'Y' if the sequence was trimmed, 'N' otherwise.
    'correct': 'Y' if the sequence was correctly processed, 'N' otherwise.

    The second report file option is a file-level report, and the generated
    file will include the following columns:

    'file': The input FASTQ file name, if known.
    'read_len': The raw read length.
    'mean_insert_len': The mean insert length of the read-generating model, if
        known.
    'sd_insert_len': The insert length standard deviation of the
        read-generating model, if known.
    'total_reads': The total reads in the file.
    'reads_correct': The number of reads correctly processed.
    'reads_incorrect': The number of reads incorrectly processed.
    'accuracy': The overall accuracy as the proportion of reads correctly
        processed.
    'positives': The total number of reads that should have been trimmed.
    'sensitivity': The proportion of actual positives detected.
    'pos_detected': The number of actual positives detected.
    'pos_not_detected': The number of actual positives not detected.
    'pos_correctly_trimmed': The number of positives correctly trimmed.
    'pos_overtrimmed': The number of positives that were over-trimmed.
    'pos_undertrimmed': The number of positives that were under-trimmed.
    'negatives': The total number of reads that should not have been trimmed.
    'specificity': The proportion of actual negatives detected.
    'neg_detected': The number of actual negatives detected.
    'neg_not_detected': The number of actual negatives not detected.
    'total_bases': The total number of bases (nucleotides codes) processed.
    'b_correct': 
    'b_incorrect': 
    'b_accuracy': 
    'b_positives': 
    'b_sensitivity': 
    'b_pos_correct': 
    'b_pos_incorrect': 
    'b_negatives': 
    'b_specificity': 
    'b_neg_correct': 
    'b_neg_incorrect'

    """
    def __init__(
        self, read_length, mean_insert_len='', sd_insert_len='', file_path='',
        f_out_seq_r=None, f_out_file_r=None, write_csv_headers=True
    ):
        if f_out_seq_r is not None:
            self.writer_seq_r = csv.DictWriter(
                f_out_seq_r, [
                    'file', 'sequence', 'read_len', 'mean_insert_len',
                    'sd_insert_len', 'insert_len', 'trimmed_len',
                    'trim_required', 'seq_trimmed', 'correct'
                ]
            )
            if write_csv_headers:
                self.writer_seq_r.writeheader()
        else:
            self.writer_seq_r = None

        if f_out_file_r is not None:
            self.writer_file_r = csv.DictWriter(
                f_out_file_r, [
                    'file', 'read_len', 'mean_insert_len', 'sd_insert_len',
                    'total_reads', 'reads_correct', 'reads_incorrect',
                    'accuracy', 'positives', 'sensitivity', 'pos_detected',
                    'pos_not_detected', 'pos_correctly_trimmed',
                    'pos_overtrimmed', 'pos_undertrimmed', 'negatives',
                    'specificity', 'neg_detected', 'neg_not_detected',
                    'total_bases', 'b_correct', 'b_incorrect', 'b_accuracy',
                    'b_positives', 'b_sensitivity', 'b_pos_correct',
                    'b_pos_incorrect', 'b_negatives', 'b_specificity',
                    'b_neg_correct', 'b_neg_incorrect'
                ]
            )
            if write_csv_headers:
                self.writer_file_r.writeheader()
        else:
            self.writer_file_r = None

        self.total_reads = 0
        self.reset(read_length, mean_insert_len, sd_insert_len, file_path)

    def complete(self, print_seq_len_info=True):
        if self.total_reads != 0 and self.writer_file_r is not None:
            self.writeFileReportRow(print_seq_len_info)

    def setReadLength(self, read_length):
        """
        Sets a new read length for subsequent sequence analysis, but does not
        reset internal record keeping.
        """
        if read_length != self.read_length:
            self.read_length = read_length
            self.read_len_changed = True

    def reset(
        self, read_length, mean_insert_len='', sd_insert_len='', file_path=''
    ):
        """
        Resets the statistics input parameters and all internal record keeping.
        """
        if self.total_reads != 0 and self.writer_file_r is not None:
            self.writeFileReportRow()

        self.file_path = file_path

        self.read_length = read_length
        self.mean_insert_len = mean_insert_len
        self.sd_insert_len = sd_insert_len

        # Variables to track actual sequence characteristics.
        self.total_reads = self.actual_pos = self.actual_neg = 0

        # Variables to track trimming performance.

        # Overall accuracy (i.e., reads that are processed perfectly).
        self.total_correct = 0

        # True positives, positives overtrimmed and undertrimmed, positives
        # untrimmed (false negatives).
        self.pos_correct = self.pos_over = self.pos_under = self.pos_untrimmed = 0

        # True negatives, negatives trimmed (false positives).
        self.neg_correct = self.neg_trimmed = 0

        # Total base count, actual positives, actual negatives.
        self.total_bases = self.actual_pos_bases = self.actual_neg_bases = 0

        # Incorrectly processed base counts.
        self.neg_bases_incorrect = self.pos_bases_incorrect = 0

        # Tracks whether the read length was changed.  This is only needed for
        # testing the assertions after adding each read.
        self.read_len_changed = False

    def writeFileReportRow(self, print_seq_len_info=True):
        if self.writer_file_r is None:
            return

        row = {}

        row['file'] = self.file_path
        if print_seq_len_info:
            row['read_len'] = self.read_length
            row['mean_insert_len'] = self.mean_insert_len
            row['sd_insert_len'] = self.sd_insert_len
        else:
            row['read_len'] = ''
            row['mean_insert_len'] = ''
            row['sd_insert_len'] = ''

        row['total_reads'] = self.total_reads
        row['reads_correct'] = self.total_correct
        row['reads_incorrect'] = self.total_reads - self.total_correct
        row['accuracy'] = self.total_correct / self.total_reads
        row['positives'] = self.actual_pos
        row['pos_detected'] = self.actual_pos - self.pos_untrimmed
        row['sensitivity'] = row['pos_detected'] / self.actual_pos
        row['pos_not_detected'] = self.pos_untrimmed
        row['pos_correctly_trimmed'] = self.pos_correct
        row['pos_overtrimmed'] = self.pos_over
        row['pos_undertrimmed'] = self.pos_under
        row['negatives'] = self.actual_neg
        row['specificity'] = self.neg_correct / self.actual_neg
        row['neg_detected'] = self.neg_correct
        row['neg_not_detected'] = self.neg_trimmed
        row['total_bases'] = self.total_bases
        row['b_pos_correct'] = self.actual_pos_bases - self.pos_bases_incorrect
        row['b_neg_correct'] = self.actual_neg_bases - self.neg_bases_incorrect
        row['b_correct'] = row['b_pos_correct'] + row['b_neg_correct']
        row['b_incorrect'] = self.neg_bases_incorrect + self.pos_bases_incorrect
        row['b_accuracy'] = row['b_correct'] / self.total_bases
        row['b_positives'] = self.actual_pos_bases
        row['b_sensitivity'] = row['b_pos_correct'] / self.actual_pos_bases
        row['b_pos_incorrect'] = self.pos_bases_incorrect
        row['b_negatives'] = self.actual_neg_bases
        row['b_specificity'] = row['b_neg_correct'] / self.actual_neg_bases
        row['b_neg_incorrect'] = self.neg_bases_incorrect

        self.writer_file_r.writerow(row)

    def addRead(self, insert_len, actual_len, seq_ID=''):
        if actual_len > self.read_length:
            raise Exception(
                'The trimmed sequence length cannot exceed the read length.'
            )

        # Calculated the expected trimmed read size.
        if insert_len > self.read_length:
            exp_len = self.read_length
        else:
            exp_len = insert_len

        self.total_reads += 1
        self.total_bases += self.read_length
        self.actual_pos_bases += exp_len
        self.actual_neg_bases += self.read_length - exp_len

        if exp_len == self.read_length:
            # Actual negative.
            self.actual_neg += 1
            if exp_len == actual_len:
                self.total_correct += 1
                self.neg_correct += 1
            else:
                self.neg_trimmed += 1
                self.neg_bases_incorrect += exp_len - actual_len
        else:
            # Actual positive.
            self.actual_pos += 1
            if exp_len == actual_len:
                self.total_correct += 1
                self.pos_correct += 1
            elif actual_len == self.read_length:
                self.pos_untrimmed += 1
                self.pos_bases_incorrect += actual_len - exp_len
            elif actual_len < exp_len:
                self.pos_over += 1
                self.neg_bases_incorrect += exp_len - actual_len
            else:
                self.pos_under += 1
                self.pos_bases_incorrect += actual_len - exp_len

        if self.writer_seq_r is not None:
            row = {}
            row['file'] = self.file_path
            row['sequence'] = seq_ID
            row['read_len'] = self.read_length
            row['mean_insert_len'] = self.mean_insert_len
            row['sd_insert_len'] = self.sd_insert_len
            row['insert_len'] = insert_len
            row['trimmed_len'] = actual_len
            row['trim_required'] = 'Y' if exp_len != self.read_length else 'N'
            row['seq_trimmed'] = 'Y' if actual_len != self.read_length else 'N'
            row['correct'] = 'Y' if actual_len == exp_len else 'N'
            self.writer_seq_r.writerow(row)

        if not(self.read_len_changed):
            assert self.total_bases == self.total_reads * self.read_length
        assert self.total_reads == self.actual_pos + self.actual_neg
        assert self.total_correct == self.pos_correct + self.neg_correct
        assert self.actual_pos == (
            self.pos_correct + self.pos_over + self.pos_under +
            self.pos_untrimmed
        )
        assert self.actual_neg == self.neg_correct + self.neg_trimmed
        assert self.total_bases == (
            self.actual_pos_bases + self.actual_neg_bases
        )

    def printReport(self):
        pos_detected = self.actual_pos - self.pos_untrimmed

        print('\n*******************************')
        print('*    READ-LEVEL STATISTICS    *')
        print('*******************************')
        print('Total reads: {0:,}'.format(self.total_reads))
        print('Total reads correctly processed: {0:,}'.format(
            self.total_correct
        ))
        print('Total reads incorrectly processed: {0:,}'.format(
            self.total_reads - self.total_correct
        ))
        print('Accuracy: {0}'.format(self.total_correct / self.total_reads))
        print('------------')
        print('Actual positives: {0:,} ({1}%)'.format(
            self.actual_pos, (self.actual_pos / self.total_reads) * 100
        ))
        print('  Sensitivity: {0}'.format(pos_detected / self.actual_pos))
        print('  Positives not detected: {0:,}'.format(self.pos_untrimmed))
        print('  Positives correctly detected: {0:,}'.format(pos_detected))
        print('    True positives correctly trimmed: {0:,}'.format(
            self.pos_correct)
        )
        print('    True positives overtrimmed: {0:,}'.format(self.pos_over))
        print('    True positives undertrimmed: {0:,}'.format(self.pos_under))
        print('------------')
        print('Actual negatives: {0:,} ({1}%)'.format(
            self.actual_neg, (self.actual_neg / self.total_reads) * 100
        ))
        print('  Specificity: {0}'.format(self.neg_correct / self.actual_neg))
        print('  Negatives correctly detected: {0:,}'.format(self.neg_correct))
        print('  Negatives mistakenly trimmed: {0:,}'.format(self.neg_trimmed))

        neg_bases_correct = self.actual_neg_bases - self.neg_bases_incorrect
        pos_bases_correct = self.actual_pos_bases - self.pos_bases_incorrect

        print('\n*************************************')
        print('*    NUCLEOTIDE-LEVEL STATISTICS    *')
        print('*************************************')
        print('Total bases: {0:,}'.format(self.total_bases))
        print('Total bases correctly processed: {0:,}'.format(
            neg_bases_correct + pos_bases_correct
        ))
        print('Total bases incorrectly processed: {0:,}'.format(
            self.neg_bases_incorrect + self.pos_bases_incorrect
        ))
        print('Accuracy: {0}'.format(
            (neg_bases_correct + pos_bases_correct) / self.total_bases
        ))
        print('------------')
        print('Actual positives: {0:,} ({1}%)'.format(
            self.actual_pos_bases,
            (self.actual_pos_bases / self.total_bases) * 100
        ))
        print('  Sensitivity: {0}'.format(
            pos_bases_correct / self.actual_pos_bases
        ))
        print('  Positives trimmed (true positives): {0:,}'.format(
                pos_bases_correct,
        ))
        print(
            '  Positives not trimmed (false negatives): {0:,} ({1} '
            'bases / read)'.format(
                self.pos_bases_incorrect,
                self.pos_bases_incorrect / self.total_reads
            )
        )
        print('------------')
        print('Actual negatives: {0:,} ({1}%)'.format(
            self.actual_neg_bases,
            (self.actual_neg_bases / self.total_bases) * 100
        ))
        print('  Specificity: {0}'.format(
            neg_bases_correct / self.actual_neg_bases
        ))
        print('  Negatives not trimmed (true negatives): {0:,}'.format(
            neg_bases_correct
        ))
        print(
            '  Negatives trimmed (false positives): {0:,} ({1} '
            'bases / read)\n'.format(
                self.neg_bases_incorrect,
                self.neg_bases_incorrect / self.total_reads
            )
        )

