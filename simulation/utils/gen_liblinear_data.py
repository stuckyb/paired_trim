#!/usr/bin/python3

from argparse import ArgumentParser
import os.path
import subprocess
import csv


argp = ArgumentParser(
    description='Generates liblinear data sets for CSV files.'
)
argp.add_argument(
    '-p', '--convert_path', type=str, required=True,
    help='The location of the libSVM convert utility.'
)
argp.add_argument(
    'datafile', type=str, nargs='+', help='An input data file in CSV format.'
)

args = argp.parse_args()

# There appears to be a bug in convert that causes the first line of the output
# file to always be the single character '0'.  We run the output through tail
# to eliminate this line.
cmd_template = args.convert_path + ' {0} | tail -n +2 > {1}'

for df_path in args.datafile:
    print('Converting {0}...'.format(df_path))
    out_path = os.path.splitext(df_path)[0] + '.svm'
    cmd_str = cmd_template.format(df_path, out_path)
    subprocess.run(cmd_str, shell=True, check=True)

