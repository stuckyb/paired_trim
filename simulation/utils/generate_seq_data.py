#!/usr/bin/python3

# Generates simulated paired-end Illumina sequencing read data based on a set
# of Illumina error profile files.  Examines the error profile file names to
# determine the read lengths to simulate and the number of error profiles
# (sequencing instruments) for each read length.  The simulated read files for
# each read length will have an equal number of reads generated from each
# relevant error profile (within rounding error).


from argparse import ArgumentParser
import os
import os.path
import glob
import subprocess
import shutil


def parseProfileName(prof_path):
    """
    Parses the read length, instrument name, and read direction from an error
    profile file name.
    """
    parts = os.path.splitext(os.path.basename(prof_path))[0].split('-')

    return (parts[1], parts[2], parts[3])

def getErrorProfiles(profiles_dir):
    """
    Inspects all error profiles in a target directory and returns a data
    structure that maps read lengths to lists that contain matched pairs of
    R1/R2 error profiles.
    """
    fnames = glob.glob(os.path.join(profiles_dir, 'err_profile*.txt'))

    profiles = {}

    # First, arrange the profiles by read length.
    for fname in fnames:
        read_len, inst_name, read_dir = parseProfileName(fname)
        #print(read_len, inst_name, read_dir)

        if read_len not in profiles:
            profiles[read_len] = []

        profiles[read_len].append(fname)

    prof_pairs = {}

    # Arrange the profiles by read length and matched pairs.
    for read_len in profiles:
        fnames = sorted(profiles[read_len])
        prof_pairs[read_len] = []

        if len(fnames) % 2 == 1:
            raise Exception(
                'At least one error profile for read length {0} does not have '
                'a matching profile of the reverse direction.'.format(read_len)
            )

        # Process each pair of profiles.
        for i in range(0, len(fnames), 2):
            f1_meta = parseProfileName(fnames[i])
            f2_meta = parseProfileName(fnames[i + 1])

            # Verify that the profiles are a matched pair.
            if (
                f1_meta[0] != f2_meta[0] or f1_meta[1] != f2_meta[1] or
                f1_meta[2] != 'R1' or f2_meta[2] != 'R2'
            ):
                raise Exception(
                    'The error profiles {0} and {1} do not appear to be a '
                    'matched pair.'.format(fname)
                )
            else:
                prof_pairs[read_len].append((fnames[i], fnames[i + 1]))

    return prof_pairs

def mergeTempReadFiles(tmp_fout_prefixes, postfix, fout_final, log_fout):
    r_fnames = [prefix + postfix for prefix in tmp_fout_prefixes]

    if len(r_fnames) > 1:
        msg = 'Merging [{0}] into {1}...'.format(
            ', '.join(r_fnames), fout_final
        )
        print(msg)
        log_fout.write(msg + '\n')
        with open(fout_final, 'wb') as fout:
            for fname in r_fnames:
                with open(fname, 'rb') as fin:
                    shutil.copyfileobj(fin, fout)
                os.unlink(fname)
    else:
        msg = 'Moving {0} to {1}...'.format(r_fnames[0], fout_final)
        print(msg)
        log_fout.write(msg + '\n')
        os.rename(r_fnames[0], fout_final)


argp = ArgumentParser(
    description='Generates simulated paired Illumina sequencing read data '
    'based on a set of Illumina error profile files.  Examines the error '
    'profile file names to determine the read lengths to simulate and the '
    'number of error profiles (sequencing instruments) for each read length.'
)
argp.add_argument(
    '-o', '--output_dir', type=str, required=False,
    default='output/sim_read_data',
    help='The output directory for simulated sequencing data (default: '
    '"output/sim_read_data").'
)
argp.add_argument(
    '-i', '--profiles_dir', type=str, required=False,
    default='../error_profiles',
    help='The location of the Illumina error profiles (default: '
    '"../error_profiles").'
)
argp.add_argument(
    '-p', '--art_illumina_path', type=str, required=False,
    default='art_illumina',
    help='The path to the art_illumina read simulator (default: '
    '"art_illumina").'
)
argp.add_argument(
    '-r', '--ref_genome', type=str, required=False,
    default='ref_genome.fasta',
    help='The path to FASTA "reference genome" to use for simulating '
    'reads(default: "ref_genome.fasta").'
)
argp.add_argument(
    '-n', '--samp_size', type=int, required=False, default=1000000,
    help='The number of reads to simulate for each read length '
    '(default: 1,000,000).'
)
argp.add_argument(
    '-a', '--alignments', action='store_true', required=False,
    help='If this flag is set, alignments of reads to the reference sequence '
    'will also be generated.'
)

args = argp.parse_args()

if not(os.path.isfile(args.art_illumina_path)):
    exit(
        '\nERROR: Could not find the read simulater '
        '"{0}".\n'.format(args.art_illumina_path)
    )

if not(os.path.isfile(args.ref_genome)):
    exit(
        '\nERROR: Could not find the reference genome '
        '"{0}".\n'.format(args.ref_genome)
    )

profiles = getErrorProfiles(args.profiles_dir)

if not(os.path.exists(args.output_dir)):
    os.makedirs(args.output_dir)

art_cmd_template = r"""{0} \
  --qprof1 {1} \
  --qprof2 {2} \
  --in {3} --rcount {4} \
  --len {5} --paired --mflen {6} --sdev {7} \
  --id "readlen_{5}_{8}-" --out {9}"""

log_fout = open(os.path.join(args.output_dir, 'LOG.txt'), 'w')

print(
    '\nSimulating {0:,} paired reads for each read '
    'length.\n'.format(args.samp_size)
)

for read_len in profiles:
    pairs = profiles[read_len]
    msg = (
        'Generating simulated read files for length {0}.  Using {1} pair(s) '
        'of error profiles...'.format(read_len, len(pairs))
    )
    print(msg)
    log_fout.write('#\n# ' + msg + '\n#\n\n')

    mean_fraglen = int(read_len)
    sd_fraglen = int(mean_fraglen * 0.4 + 0.5)
    final_fout_prefix = os.path.join(
        args.output_dir,
        '{0}-{1}-{2}-R'.format(read_len, mean_fraglen, sd_fraglen)
    )

    prof_read_cnt = int(args.samp_size / len(pairs) + 0.5)

    # Generate simulated reads for each pair of error profiles.
    tmp_fout_prefixes = []
    for pair_cnt, pair in enumerate(pairs):
        # Calculate the number of reads to generate for this pair to ensure the
        # final total for all pairs is equal to args.samp_size.
        if pair_cnt == len(pairs) - 1:
            read_cnt = args.samp_size - (pair_cnt * prof_read_cnt)
        else:
            read_cnt = prof_read_cnt

        # Generate temporary output file name prefix.
        tmp_fout_prefix = os.path.join(
            args.output_dir, 'tmp_{0}-R'.format(pair_cnt)
        )
        tmp_fout_prefixes.append(tmp_fout_prefix)

        # Run ART and log the output.
        print('  Generating reads for {0} and {1}...'.format(
                os.path.basename(pair[0]), os.path.basename(pair[1])
        ))
        cmd_str = art_cmd_template.format(
            args.art_illumina_path, pair[0], pair[1], args.ref_genome,
            read_cnt, read_len, mean_fraglen, sd_fraglen, pair_cnt,
            tmp_fout_prefix
        )
        if not(args.alignments):
            cmd_str += ' --noALN'
        log_fout.write(cmd_str + '\n\n')
        result = subprocess.run(
            cmd_str, shell=True, check=True, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE, universal_newlines=True
        )
        for line in result.stdout.splitlines():
            log_fout.write('    # ' + line + '\n')
        for line in result.stderr.splitlines():
            log_fout.write('    # ' + line + '\n')
        log_fout.write('\n')

    # Merge the temporary read files into a single read file for the target
    # read length and delete the temporary files.
    mergeTempReadFiles(
        tmp_fout_prefixes, '1.fq', final_fout_prefix + '1.fastq', log_fout
    )
    mergeTempReadFiles(
        tmp_fout_prefixes, '2.fq', final_fout_prefix + '2.fastq', log_fout
    )
    print()
    log_fout.write('\n')

