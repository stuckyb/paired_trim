library('tidyr')
library('dplyr')
library('ggplot2')


# --------
# Examine the aggregated results.
# --------
data_raw = read.csv('output/align_hypers_search/file_report-SUMMARY.csv')
colnames(data_raw)

data = filter(data_raw, !(is.na(total_reads)), gap_penalty < -12) %>%
    mutate(prop_pos_correct = pos_correctly_trimmed / (positives - pos_not_detected)) %>%
    select(
        gap_penalty, percentile, accuracy, reads_incorrect, prop_pos_correct,
        pos_correctly_trimmed, pos_not_detected, neg_not_detected, b_accuracy,
        b_sensitivity, b_specificity
    )

# Place all measurements in long, key/value form for plotting purposes.
l_data = gather(data, key='measurement', value='val', -gap_penalty, -percentile) 

ggplot(data=l_data, aes(x=gap_penalty, y=val, color=as.factor(percentile))) +
    geom_point() + geom_line() + geom_vline(xintercept=-30) +
    facet_wrap(vars(measurement), scales='free_y') + labs(color='percentile')

# Examine the maxima/minima of key variables.
data[which.max(data$prop_pos_correct),]
data[which.max(data$pos_correctly_trimmed),]
data[which.min(data$pos_not_detected),]
data[which.max(data$accuracy),]


# --------
# Examine the results separated out by read length.
# --------
data_raw = read.csv('output/align_hypers_search/file_report-MERGED.csv')
colnames(data_raw)

data = filter(data_raw, gap_penalty < -12) %>%
    mutate(prop_pos_correct = pos_correctly_trimmed / (positives - pos_not_detected))
colnames(data)

ggplot(
    data=data,
    aes(x=gap_penalty, y=pos_not_detected, color=as.factor(percentile))
) + geom_point() + geom_line() + geom_vline(xintercept=-30) +
    facet_wrap(vars(read_len), scales='free_y') + labs(color='percentile')

# See which gap penalty / alpha combinations resulted in the best proportion of
# positives correctly trimmed for each read length.  (The final conversion to a
# data frame is so that full floating-point values are printed.)
group_by(data, read_len) %>%
    filter(prop_pos_correct == max(prop_pos_correct)) %>%
    select(read_len, gap_penalty, percentile, prop_pos_correct) %>%
    arrange(read_len) %>% as.data.frame()

# See which gap penalty / alpha combinations resulted in the minimum number of
# positives not detected.
group_by(data, read_len) %>%
    filter(pos_not_detected == min(pos_not_detected)) %>%
    select(read_len, gap_penalty, percentile, pos_not_detected) %>%
    arrange(read_len) %>% as.data.frame()
