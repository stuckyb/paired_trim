#!/usr/bin/python3

# Uses simulated sequencing read data and a set of CSV probability table files
# to search parameter space for two alignment-related hyperparameters: the gap
# penalty and the value of alpha for testing alignment significance.


from argparse import ArgumentParser
import os
import os.path
import glob
import subprocess
import sys
import tempfile
import csv
from shared_funcs import getReadsFiles

# Modify the search path so we can import code for working with the alignment
# score probability tables.
adist_utils_dir = os.path.realpath(os.path.join(
    os.path.dirname(__file__), '../../align_dists/utils'
))
sys.path.append(adist_utils_dir)
from cpp_table_funcs import parseTableFileName, generateCode


class ChangeWD:
    """
    A simple context manager for changing the working directory that ensures we
    return to the original working directory afterwards.
    """
    def __init__(self, new_wd):
        self.new_wd = new_wd

    def __enter__(self):
        self.old_wd = os.getcwd()
        os.chdir(self.new_wd)

    def __exit__(self, e_type, e_value, tb):
        os.chdir(self.old_wd)


argp = ArgumentParser(
    description='Uses simulated sequencing read data and a set of CSV '
    'probability table files to search parameter space for two '
    'alignment-related hyperparameters: the gap penalty and the value of alpha '
    'for testing alignment significance.'
)
argp.add_argument(
    '-o', '--output_dir', type=str, required=False,
    default='output/align_hypers_search',
    help='The output directory for hyperparameter search results '
    '(default: "output/align_hypers_search").'
)
argp.add_argument(
    '-d', '--align_dists', type=str, required=False, default=None,
    help='The location of the alignment score probability distribution tables '
    '(default: "SCRIPT_LOCATION/../../align_dists").'
)
argp.add_argument(
    '-r', '--reads_dir', type=str, required=False, default=None,
    help='The location of the simulated read FASTQ files '
    '(default: "SCRIPT_LOCATION/../output/sim_read_data").'
)
argp.add_argument(
    '-b', '--build_dir', type=str, required=False, default=None,
    help='The location of the build directory '
    '(default: "SCRIPT_LOCATION/../../build").'
)

args = argp.parse_args()

# Get all required directory and file paths.
script_dir = os.path.dirname(__file__)

src_path = os.path.normpath(
    os.path.join(script_dir, '../../src/_score_pr_table.cpp_')
)
if not(os.path.isfile(src_path)):
    exit('\nERROR: Could not find the source file {0}.\n'.format(src_path))

if args.align_dists is None:
    adists_dir = os.path.normpath(
        os.path.join(script_dir, '../../align_dists')
    )
else:
    adists_dir = args.align_dists

if args.build_dir is None:
    build_dir = os.path.normpath(os.path.join(script_dir, '../../build'))
else:
    build_dir = args.build_dir
trim_path = os.path.join(build_dir, 'src/paired-trim')

if args.reads_dir is None:
    reads_dir = os.path.normpath(
        os.path.join(script_dir, '../output/sim_read_data')
    )
else:
    reads_dir = args.reads_dir

#print(adists_dir, script_dir, src_dir, trim_path, reads_dir)

if not(os.path.isdir(adists_dir)):
    exit(
        '\nERROR: Could not find the alignment score probability distribution '
        'tables directory, "{0}".\n'.format(adists_dir)
    )

if not(os.path.isdir(build_dir)):
    exit(
        '\nERROR: Could not find the build directory, "{0}".\n'.format(
            build_dir
        )
    )

if not(os.path.isdir(reads_dir)):
    exit(
        '\nERROR: Could not find the simulated reads directory, '
        '"{0}".\n'.format(reads_dir)
    )

if not(os.path.exists(args.output_dir)):
    os.makedirs(args.output_dir)

# Get the alignment score probability distribution table files.  Sort them
# alphabetically first, then by gap penalty.
fnames = glob.glob(os.path.join(adists_dir, 'align_score_table*.csv'))
fnames.sort(reverse=True)
fnames.sort(reverse=True, key=lambda fname:
    parseTableFileName(fname)[0]
)

read_pairs = getReadsFiles(reads_dir)

# The template trimmer command.
trim_cmd_template = (
    trim_path + ' --no_id_match -o {0} -a '
    '--R1_adapter AGATCGGAAGAGCACACGTCTGAACTCCAGTCACGAGTTA '
    '--R2_adapter AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTC -l 0 -1 {1} -2 {2}'
)

# The template analysis command.
analyze_cmd_template = (
    os.path.join(script_dir, 'calc_trim_stats.py') + ' --seq_report {0} '
    '--file_report {1} {2}'
)

# Groups of columns used by the output CSV files.
colname_lists = (
    ['gap_penalty', 'percentile', 'dist_samp_size'],
    ['read_len', 'mean_insert_len', 'sd_insert_len'],
    [
        'total_reads', 'reads_correct', 'reads_incorrect', 'accuracy',
        'positives', 'sensitivity', 'pos_detected', 'pos_not_detected',
        'pos_correctly_trimmed', 'pos_overtrimmed', 'pos_undertrimmed',
        'negatives', 'specificity', 'neg_detected', 'neg_not_detected',
        'total_bases', 'b_correct', 'b_incorrect', 'b_accuracy', 'b_positives',
        'b_sensitivity', 'b_pos_correct', 'b_pos_incorrect', 'b_negatives',
        'b_specificity', 'b_neg_correct', 'b_neg_incorrect'
    ]
)

def mergeFileReportRows(
    row_groups, mfr_writer, g_penalty, percentile, samp_size
):
    """
    Merges file report data for rows that share a common read length, mean
    insert length, and s.d. of insert length.  Column values are either summed
    or average, as appropriate for the data.
    """
    int_cols =  set([
        'total_reads', 'reads_correct', 'reads_incorrect', 'positives',
        'pos_detected', 'pos_not_detected', 'pos_correctly_trimmed',
        'pos_overtrimmed', 'pos_undertrimmed', 'negatives', 'neg_detected',
        'neg_not_detected', 'total_bases', 'b_correct', 'b_incorrect',
        'b_positives', 'b_pos_correct', 'b_pos_incorrect', 'b_negatives',
        'b_neg_correct', 'b_neg_incorrect'
    ])
    float_cols = set([
        'accuracy', 'sensitivity', 'specificity', 'b_accuracy',
        'b_sensitivity', 'b_specificity'
    ])

    for row_group_key in row_groups:
        read_len, mean_insert_len, sd_insert_len = row_group_key
        row_group = row_groups[row_group_key]

        col_sums = {}
        for colname in int_cols:
            col_sums[colname] = 0
        for colname in float_cols:
            col_sums[colname] = 0.0

        # Get the column sums for all rows in the group.
        for row in row_group:
            for col in row:
                if col in int_cols:
                    col_sums[col] += int(row[col])
                elif col in float_cols:
                    col_sums[col] += float(row[col])

        # Calculate means of the floating-point columns.
        for col in float_cols:
            col_sums[col] = col_sums[col] / len(row_group)

        # Write the merged output row.
        row_out = {}
        row_out['gap_penalty'] = g_penalty
        row_out['percentile'] = percentile
        row_out['dist_samp_size'] = samp_size
        row_out['read_len'] = read_len
        row_out['mean_insert_len'] = mean_insert_len
        row_out['sd_insert_len'] = sd_insert_len
        row_out.update(col_sums)

        mfr_writer.writerow(row_out)


# Open output file for the CSV merged file report.
mfr_path = os.path.join(args.output_dir, 'file_report-MERGED.csv')
mfr_writer = csv.DictWriter(
    open(mfr_path, 'w'), colname_lists[0] + colname_lists[1] + colname_lists[2]
)
mfr_writer.writeheader()

# Open output file for the CSV summary file report.
sfr_path = os.path.join(args.output_dir, 'file_report-SUMMARY.csv')
sfr_writer = csv.DictWriter(
    open(sfr_path, 'w'), colname_lists[0] + colname_lists[2]
)
sfr_writer.writeheader()

# Process each (gap penalty, percentile/alpha) combination.
for fname in fnames:
    g_penalty, percentile, samp_size = parseTableFileName(fname)

    print(
        'Building trimmer with gap penalty {0} and percentile cutoff of '
        '{1}...'.format(g_penalty, percentile)
    )

    # Generate new C++ code to define the default gap penalty and lookup table.
    with open(src_path, 'w') as fout:
        generateCode([fname], fout)

    # Build the trimmer.
    with ChangeWD(build_dir):
        subprocess.run('make', check=True)

    #tmpdir = tempfile.mkdtemp()
    #if True:
    with tempfile.TemporaryDirectory() as tmpdir:
        # Trim the simulated read data.
        for read_pair in read_pairs:
            print('Trimming {0} and {1}...'.format(
                os.path.basename(read_pair[0]), os.path.basename(read_pair[1]))
            )
            trim_cmd = trim_cmd_template.format(
                tmpdir, read_pair[0], read_pair[1]
            )
            subprocess.run(trim_cmd, shell=True, check=True)

        # Analyze the trimming results for the simulated read data.
        print(
            'Analyzing trimming results for gap penalty {0} and percentile '
            'cutoff of {1}...\n'.format(g_penalty, percentile)
        )
        output_suffix = '{0}-{1}'.format(g_penalty * -1, percentile)
        sr_path = os.path.join(
            args.output_dir, 'seq_report-{0}.csv'.format(output_suffix)
        )
        fr_path = os.path.join(
            args.output_dir, 'file_report-{0}.csv'.format(output_suffix)
        )
        su_path = os.path.join(
            args.output_dir, 'summary-{0}.txt'.format(output_suffix)
        )
        analyze_cmd = analyze_cmd_template.format(
            sr_path, fr_path, os.path.join(tmpdir, '*-paired_trim.fastq')
            
        )
        result = subprocess.run(
            analyze_cmd, shell=True, check=True, stdout=subprocess.PIPE,
            universal_newlines=True
        )
        with open(su_path, 'w') as fout:
            fout.write(result.stdout)

        # Extract the summary from the file-level report and add it to the file
        # report summary CSV file.
        with open(fr_path) as fin:
            row_groups = {}
            reader = csv.DictReader(fin)
            for row in reader:
                if row['file'] == 'OVERALL':
                    # Copy the row, removing the columns we don't need.
                    rowout = {c: v for c,v in row.items() if c not in (
                        'file', 'read_len', 'mean_insert_len', 'sd_insert_len'
                    )}

                    rowout['gap_penalty'] = g_penalty
                    rowout['percentile'] = percentile
                    rowout['dist_samp_size'] = samp_size

                    sfr_writer.writerow(rowout)
                else:
                    # Create a map of rows grouped read length, mean insert
                    # length, and s.d. of insert length.
                    row_group = (
                        row['read_len'], row['mean_insert_len'],
                        row['sd_insert_len']
                    )
                    if row_group not in row_groups:
                        row_groups[row_group] = []
                    row_groups[row_group].append(row)

            mergeFileReportRows(
                row_groups, mfr_writer, g_penalty, percentile, samp_size
            )

