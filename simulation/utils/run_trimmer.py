#!/usr/bin/python3

# Runs a trimmer on a set of simulated paired-end Illumina FASTQ read files and
# analyzes the results.


from argparse import ArgumentParser
import os
import os.path
import glob
import subprocess
import sys
import tempfile
import csv
from shared_funcs import getReadsFiles


argp = ArgumentParser(
    description='Runs the specified trimmer on a set of simulated paired-end '
    'Illumina FASTQ read files and analyzes the results.'
)
argp.add_argument(
    '-o', '--output_dir', type=str, required=False, default=None,
    help='The output directory for the trimmed read files '
    '(default: "SCRIPT_LOCATION/../output/trimmed_reads-TRIMMER").'
)
argp.add_argument(
    '-r', '--reads_dir', type=str, required=False, default=None,
    help='The location of the simulated read FASTQ files '
    '(default: "SCRIPT_LOCATION/../output/sim_read_data").'
)
argp.add_argument(
    '-t', '--trimmer', type=str, required=False, default='paired-trim',
    help='The trimmer to use (default: "paired-trim").'
)
argp.add_argument(
    '-p', '--trimmer_path', type=str, required=False, default=None,
    help='The location of the trimmer executable '
    '(default depends on which trimmer is selected).'
)

args = argp.parse_args()

# Get all required directory and file paths.
script_dir = os.path.dirname(__file__)

if args.reads_dir is None:
    reads_dir = os.path.normpath(
        os.path.join(script_dir, '../output/sim_read_data')
    )
else:
    reads_dir = args.reads_dir

'output/trimmed_reads'
output_dir = args.output_dir
if output_dir is None:
    out_dname = '../output/trimmed_reads'
    if args.trimmer != 'paired-trim':
        out_dname += '-' + args.trimmer

    output_dir = os.path.normpath(
        os.path.join(script_dir, out_dname)
    )

trimmer_path = args.trimmer_path
if trimmer_path is None:
    if args.trimmer == 'paired-trim':
        trimmer_path = os.path.normpath(
            os.path.join(script_dir, '../../build/src/paired-trim')
        )
    elif args.trimmer == 'fastx_clipper':
        trimmer_path = '/usr/bin/fastx_clipper'
    else:
        exit('\nERROR: The trimmer "{0}" is not supported.\n'.format(
            args.trimmer
        ))
#print(script_dir, reads_dir, trimmer_path)

if not(os.path.isdir(reads_dir)):
    exit(
        '\nERROR: Could not find the simulated reads directory, '
        '"{0}".\n'.format(reads_dir)
    )

if not(os.path.isfile(trimmer_path)):
    exit(
        '\nERROR: Could not find the trimmer executable, '
        '"{0}".\n'.format(trimmer_path)
    )

if not(os.path.exists(output_dir)):
    os.makedirs(output_dir)

read_pairs = getReadsFiles(reads_dir)

# Get the template trimmer command.
if args.trimmer == 'paired-trim':
    trim_cmd_templates = [
        (trimmer_path + ' --logging --log_adapt_align --no_id_match -o {0} -a '
        '--R1_adapter AGATCGGAAGAGCACACGTCTGAACTCCAGTCACGAGTTA '
        '--R2_adapter AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTC '
        '-l 0 -1 {1} -2 {2} > {3}')
    ]
elif args.trimmer == 'fastx_clipper':
    run_util_path = os.path.join(script_dir, 'run_fastx.py')
    trim_cmd_templates = [
        run_util_path + ' --cmd_path "' + trimmer_path + '" --dir_out {0} '
        '-l 0 -n -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCACGAGTTA -i {1}',
        run_util_path + ' --cmd_path "' + trimmer_path + '" --dir_out {0} '
        '-l 0 -n -a AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTC -i {1}',
    ]
else:
    exit('\nERROR: The trimmer "{0}" is not supported.\n'.format(args.trimmer))

# The template analysis command.
analyze_cmd_template = (
    os.path.join(script_dir, 'calc_trim_stats.py') + ' --seq_report {0} '
    '--file_report {1} {2}'
)

# Trim each pair of read files.
for read_pair in read_pairs:
    read_prefix = os.path.basename(read_pair[0]).rsplit('-', 1)[0]

    # Trim the simulated read data.
    print('Trimming {0} and {1}...'.format(
        os.path.basename(read_pair[0]), os.path.basename(read_pair[1]))
    )

    log_path = os.path.join(output_dir, 'log_{0}.csv'.format(read_prefix))

    if len(trim_cmd_templates) == 1:
        # Trim both reads in a single command.
        trim_cmd = trim_cmd_templates[0].format(
            output_dir, read_pair[0], read_pair[1], log_path
        )
        subprocess.run(trim_cmd, shell=True, check=True)
    else:
        # Trim the forward and reverse reads in separate commands.
        trim_cmd = trim_cmd_templates[0].format(
            output_dir, read_pair[0], log_path
        )
        subprocess.run(trim_cmd, shell=True, check=True)
        trim_cmd = trim_cmd_templates[1].format(
            output_dir, read_pair[1], log_path
        )
        subprocess.run(trim_cmd, shell=True, check=True)

# Analyze the trimming results.
print('Analyzing all trimming results...')

sr_path = os.path.join(output_dir, 'ANALYSIS-seq_report.csv')
fr_path = os.path.join(output_dir, 'ANALYSIS-file_report.csv')
su_path = os.path.join(output_dir, 'ANALYSIS-summary.txt')
analyze_cmd = analyze_cmd_template.format(
    sr_path, fr_path, os.path.join(output_dir, '*.fastq')
)
result = subprocess.run(
    analyze_cmd, shell=True, check=True, stdout=subprocess.PIPE,
    universal_newlines=True
)
with open(su_path, 'w') as fout:
    fout.write(result.stdout)

