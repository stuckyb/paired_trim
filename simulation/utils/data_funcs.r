
# Shared R functions for data manipulation.

# Adds key feature crosses to a data frame.  Note that the normalized alignment
# scores and proportional alignment length are already effectively feature
# crosses (they combine information about alignment score, length, and read
# length, and alignment score is related to alignment identity).  I.e.:
#
#  align_len_prop = align_len / read_len,
#  align_score_norm = align_score / read_len,
#  adapt_align_len_prop = adapt_align_len / 40,
#  adapt_align_score_norm = adapt_align_score / 40
addFeatureCrosses = function(data) {
    new_data = mutate(data,
        align_len_prop.adapt_align_len_prop = align_len_prop * adapt_align_len_prop,
        align_score_norm.adapt_align_score_norm = align_score_norm * adapt_align_score_norm,
        align_trim_mismatch.align_score_norm = align_trim_mismatch * align_score_norm,
        align_trim_mismatch.adapt_align_score_norm = align_trim_mismatch * adapt_align_score_norm,
        alt_align_possible.align_score_norm = alt_align_possible * align_score_norm,
        alt_align_possible.align_len_prop = alt_align_possible * align_len_prop,
        alt_align_possible.adapt_align_score_norm = alt_align_possible * adapt_align_score_norm,
        alt_align_possible.adapt_align_len_prop = alt_align_possible * adapt_align_len_prop,
    )

    return(new_data)
}

