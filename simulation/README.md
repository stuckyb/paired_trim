
This folder contains data files and utilities for generating simulated Illumina sequencing data and for analyzing trimming performance on those simulated data.

## Reference genome

The "reference genome", `ref_genome.fasta`, is used as the source genome for generating simulated sequencing reads.  It was created by concatenating the following chromosome sequences from various organisms:

  * Drosophila melanogaster chromosome 2L, NCBI Reference Sequence: NT_033779.5, https://www.ncbi.nlm.nih.gov/nuccore/NT_033779.5 (23.51 Mb)
  * Nasonia vitripennis strain AsymCX chromosome 4, Nvit_2.1, whole genome shotgun sequence, NCBI Reference Sequence: NC_015870.2, https://www.ncbi.nlm.nih.gov/nuccore/NC_015870.2 (14.81 Mb)
  * Ictalurus punctatus breed USDA103 chromosome 28, IpCoco_1.2, whole genome shotgun sequence, NCBI Reference Sequence: NC_030443.1, https://www.ncbi.nlm.nih.gov/nuccore/NC_030443.1 (18.42 Mb)
  * Anolis carolinensis chromosome 6, AnoCar2.0, whole genome shotgun sequence, NCBI Reference Sequence: NC_014781.1, https://www.ncbi.nlm.nih.gov/nuccore/NC_014781.1 (80.74 Mb, I only took the first 257,143 lines [18.0 Mb])
  * Mus musculus strain C57BL/6J chromosome 18, GRCm38.p4 C57BL/6J, NCBI Reference Sequence: NC_000084.6, https://www.ncbi.nlm.nih.gov/nuccore/NC_000084.6 (90.7 Mb, I only took the first 257,143 lines [18.0 Mb] after the initial block of 'N's at the beginning)
  * Prunus persica cultivar Lovell chromosome G5, Prunus_persica_NCBIv2, whole genome shotgun sequence, NCBI Reference Sequence: NC_034013.1, https://www.ncbi.nlm.nih.gov/nuccore/NC_034013.1 (18.5 Mb)
  * Sorghum bicolor cultivar BTx623 chromosome 6, Sorghum_bicolor_NCBIv3, whole genome shotgun sequence, NCBI Reference Sequence: NC_012875.2, https://www.ncbi.nlm.nih.gov/nuccore/NC_012875.2 (61.28 Mb, I only took the first 257,143 lines [18.0 Mb])

After assembling the raw sequence data, all ambiguous nucleotide codes were replaced with compatible unambiguous codes using the script `utils/disambiguate.py`.


## Generating simulated Illumina sequencing data

The utility `utils/generate_seq_data.py` automatically generates simulated paired-end Illumina sequencing read data based on a set of Illumina sequencing instrument error profile files.  It examines error profile file names to determine the read lengths to simulate and the number of error profiles (i.e., sequencing instruments) for each read length.  The simulated read files for each read length will have an equal number of reads generated from each relevant error profile (within rounding error).

Note that `generate_seq_data.py` requires my [modified version of the ART sequencing reads simulator](https://gitlab.com/stuckyb/derivative_art) to properly simulate adapter read-through.

To generate simulated read data using the included error profiles, the included reference genome, the default output directory (`sim_read_data`), and the default sample size of 1,000,000 paired reads.:
```
utils/generate_seq_data.py -p /PATH/TO/MODIFIED/ART/art_illumina
```


## Testing trimmer performance

The utility `run_trimmer.py` makes it easy to test the performance of different trimmers on simulated read data.  For example, to trim the simulated read data (assuming it is in the default location) with `paired-trim` and analyze the results:
```
$ utils/run_trimmer.py -t paired-trim
```


## Optimizing alignment-related hyperparameters

Choosing good values for the alignment gap penalty and the value of alpha to use for testing alignment scores requires searching parameter space to estimate how the trimmer performs with various combinations of (gap penalty, alpha) values.  The utility `utils/search_align_hypers.py` does this automatically, using the alignment score distribution tables in `../align_dists` to test the performance of different (gap penalty, alpha) combinations on simulated read data.  Assuming you are using the default locations for everything, you can run it as follows:
```
$ utils/search_align_hypers.py
```

This will generate a bunch of trimming statistics reports in `output/align_hypers_search` (assuming the default output location).  The most important file is `output/align_hypers_search/file_report-SUMMARY.csv`, which summarizes overall trimming performance across all (gap penalty, alpha) combinations.  To visually explore the results, use `utils/visualize_align_hypers.r`.

Generally speaking, it makes sense to choose a (gap penalty, alpha) that produces the best possible trimming results when a read pair actualy does require trimming.  Although detection of "negatives" (i.e., read pairs that don't require trimming) can be improved by decreasing the gap penalty and increasing alpha, this comes at the cost of deteriorating trimming performance on true positives (i.e., read pairs that actually require trimming).  Detection of negatives can instead be improved in other ways (see next section).


## Analysis of alignment-guided trimming performance

For a detailed analysis/optimization of alignment-guided trimming, first, use `utils/run_trimmer.py` to trim all simulated read files with `paired-trim`.  `run_trimmer.py` will automatically run `calc_trim_stats.py` to analyze the results.  For example (using all default settings):
```
$ utils/run_trimmer.py
```

Next, use `utils/merge_trim_logs.py` to merge the trimming log files with the sequence-level results file from calc_trim_stats.py.  E.g. (running from the output directory),
```
$ ../../utils/merge_trim_logs.py -o ANALYSIS-merged.csv ANALYSIS-seq_report.csv log_*.csv
```

