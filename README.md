## Overview

`paired-trim` is a sequence trimmer for paired-end sequencing reads that can be used to precisely trim the "junk" DNA tails that are ligated to the 3' ends of sequences by some library prep kits (e.g., the [Methyl-Seq](https://swiftbiosci.com/products/accel-ngs-methyl-seq-dna-library-kit/) kit from Swift Biosciences), as well as for more general-purpose sequence trimming tasks.  `paired-trim` uses an alignment-based algorithm that tries to preserve as much sequence data as possible, so it should be especially useful for applications where DNA fragments are short, such as when sequencing ancient DNA.  `paired-trim` can also trim low-quality bases from the ends of sequences and do sequence length filtering.

## Usage

### Command-line interface

`paired-trim` is a command-line tool that supports the following arguments (`-1`/`--r1_reads` is required; all other arguments are optional):

#### General options

  * `-h`/`--help`: Print usage information and exit.
  * `-1`/`--r1_reads` *(required)*: A FASTQ file of sequencing reads. If `--r2_reads` is also used, the file specified with `--r1_reads` should contain the forward sequencing reads.  If `--r2_reads` is not also used, then `paired-trim` will run in single-read mode, which means alignment-guided trimming will be disabled.
  * `-2`/`--r2_reads`: A FASTQ file of reverse sequencing reads that are paired with the reads in the file specified with `--r1_reads`.
  * `-q`/`--quality_offset`: The integer offset to use when interpreting encoded quality scores in FASTQ files.  The default is 33, which should work for all recent next-gen sequencing data.
  * `-s`/`--suffix`: A suffix to append to the input file names to generate the output file names.  It the input file names have the `.fastq` extension, the suffix will be appended before the `.fastq` extension.  The default suffix is "-paired_trim".  For example, using the default suffix, the input file `sequencing_data_R1.fastq` would result in output files called `sequencing_data_R1-paired_trim.fastq` and `sequencing_data_R1-paired_trim-singletons.fastq` (if needed).
  * `-o`/`--output_dir`: A path to a folder in which to place the output files.  The default is the current working directory (i.e., `.`).

#### Quality trimming options

  * `-w`/`--window_size`: The sliding window size to use for quality trimming (default: 1).
  * `-m`/`--minimum_quality`: The minimum quality score to use for quality trimming.  Base calls with quality scores below this minimum will be considered low quality (default: 0).
  * `-c`/`--minimum_highqual_count`: The minimum acceptable number of high-quality bases in the sliding window size to use for quality trimming.  The default is 0, which means quality trimming is disabled.

#### Explicit end trimming options

  * `--R1_trim_5p`: The number of bases to trim from the 5' end of all R1 (forward) sequencing reads (default: 0).
  * `--R1_trim_3p`: The number of bases to trim from the 3' end of all R1 (forward) sequencing reads (default: 0).
  * `--R2_trim_5p`: The number of bases to trim from the 5' end of all R2 (reverse) sequencing reads (default: 0).
  * `--R2_trim_3p`: The number of bases to trim from the 5' end of all R2 (reverse) sequencing reads (default: 0).

#### Alignment-guided trimming options:
  * `-a`/`--align_trim`: A flag that enables alignment-guided trimming.

#### Length filtering options:
  * `-l`/`--minimum_length`: The minimum length required to include a sequence in the output files.  Length is calculated after all trimming operations are completed (default: 1).

### Examples

The simplest possible `paired-trim` command looks like this:
```
$ paired-trim -1 sequences_R1.fasta
```
That will produce one output files, called `sequences_R1-paired_trim.fasta`, that contain the same contents as the source FASTQ file.  That's not very useful, so here is an example command that actually does something:
```
$ paired-trim -m 28 -1 sequences_R1.fasta
```
That will produce one output file, again called `sequences_R1-paired_trim.fasta`, that contain the same set of sequences as the source FASTQ file, but this time with the 3' ends of all sequences quality trimmed to remove all trailing bases with quality scores less than 28.  If the quality trimming completely eliminates one or more sequences (i.e., the trimmed sequence length is 0),  the empty sequences will not be included in the output file.

To use a sliding window for quality trimming:
```
$ paired-trim -m 28 -w 8 -p 0.75 -r1 sequences_R1.fasta -r2 sequences_R2.fasta
```
That command will trim the 3' ends of the sequences using a sliding window of 8 bases, and will trim trailing bases until at least 6 bases (75%) in the window have quality scores of 28 or greater.

Here is an example of using `paired-trim` to remove junk DNA tails leftover from sequencing library preparation:
```
$ paired-trim -r2t 10 -r1 sequences_R1.fasta -r2 sequences_R2.fasta
```
The output files will have 10 base pairs trimmed from the 5' ends of all sequences in `sequences_R2.fasta`, and the 3' ends of the sequences in `sequences_R1.fasta` will be trimmed as needed.

As a final example, here we put everything together, including sliding-window quality trimming, removal of junk DNA tails, and deleting all trimmed sequences less than 30 bases long:
```
$ paired-trim -l 30 \
    -m 28 -w 8 -p 0.75 \
    -r2t 10 \
    -r1 sequences_R1.fasta -r2 sequences_R2.fasta
```
For clarity, I've split the command up into multiple lines so that each group of arguments is easy to see.  As with other `paired-trim` commands, this will produce at least two output files to contain the modified, paired read sequences.  If any sequences are deleted, such that only one member of a read pair remains, additional output files will be generated to contain the singleton sequence reads.


## Implementation details

First, some details about the assumptions that `paired-trim` makes about input sequence data: 
  * `paired-trim` will only work with paired-end sequence data.
  * `paired-trim` assumes that the sequencing adapters have already been trimmed from the ends of the input sequences.  You can use a tool like `fastx_clipper` from the [FASTX-Toolkit](http://hannonlab.cshl.edu/fastx_toolkit/) to do this.
  * `paired-trim` assumes that junk DNA tails are on the 5' end of the reverse (R2) read sequences (i.e., at the start), which means that junk DNA tails _might be_ on the 3' end of the forward (R1) read sequences.
 
 Given these assumptions, to remove junk DNA tails, `paired-trim` will trim a user-specified number of bases (e.g., the expected length of the junk DNA tail) from the 5' end of the R2 reads, compute global pairwise alignments of the paired R1 and R2 reads, and then use these alignments to determine whether any of the junk DNA tail is present in the 3' ends of the R1 reads.  The specific steps are as follows:
   1. Quality-trim the 3' ends of all sequences, if desired.  `paired-trim` uses a sliding window quality trimming algorithm with customizable parameters (see usage details, above).
   2. Remove the junk DNA tail from the R2 reads by trimming a specified number of bases from the 5' end of each R2 read.
   3. Trim junk DNA tails from the 3' ends of the R1 reads.  This requires the following steps:
       1. Compute the global pairwise alignments of the paired R1 and R2 reads.
       2. Determine whether any junk DNA tail is present in the 3' ends of the R1 reads by inspecting the aligned sequences.
       3. Trim the 3' ends of the R1 reads as needed.
  4. Any sequences in R1 that cannot be paired with a sequence in R2 will have the same number of bases trimmed from their 3' ends as were trimmed from the 5' ends of the R2 sequences.
  5. Eliminate any sequences that are less than the minimum desired sequence length.


## Building paired-trim

  1. In the main source folder, create a folder called `build` (or whatever you prefer) and make it your working directory.
     ```
     $ mkdir build
     $ cd build
     ```
  2. Use CMake to initialize the build system for your OS.  Note that if you want `paired-trim` to be as fast as possible, specifying the build type is necessary to enable all compiler optimizations.
     ```
     $ cmake -DCMAKE_BUILD_TYPE=RELEASE ..
     ```
  3. Compile `paired-trim`.  (The example is for \*nix-based system.)
     ```
     $ make
     ```

