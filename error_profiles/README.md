
This directory contains error profiles for simulating reads from various Illumina sequencing instruments.  Simulated reads are useful for testing trimming performance and estimating optimal trimming algorithm parameters.  The error profiles are intended for use with the [ART](https://www.niehs.nih.gov/research/resources/software/biostatistics/art/index.cfm) sequencing simulation software.  In particular, to simulate reads with adapter read-through, you will need to use my [modified version of ART](https://gitlab.com/stuckyb/derivative_art).

The file `profile_sources.csv` has detailed information about the [SRA](https://www.ncbi.nlm.nih.gov/sra) datasets that were used to generate the error profiles.


## Generating new error profiles

1. Run `utils/get_sra_counts.py` to generate a new data file with information about the number of records in the SRA for various read lengths, Illumina sequencing instruments, and sequencing strategies.  E.g.:
    ```
    $ utils/get_sra_counts.py -e EMAIL_ADDRESS
    ```

2. Use `utils/view_sra_records.r` to visually explore the SRA record data and generate a report to use for downloading SRA sequencing data (called `platforms_report.csv` by default).

3. Once you have the per-platform SRA record report, use `utils/get_sra_run_samples.py` to generate random samples of SRA sequencing runs for each target read length and sequencing instrument.  This utility will ensure that each run sampled has a sufficient number of reads (4,000,000 by default) and that all runs in a sample are from different taxa, different submitters, and different projects (all according to the metadata, at least).  E.g.:
    ```
    $ mkdir sra_data
    $ utils/get_sra_run_samples.py -e EMAIL_ADDRESS platforms_report.csv
    ```
    It is a good idea to generate samples slightly larger than needed for actually building the error profiles in case some of the runs are unusable (e.g., due to read pre-processing).

4. The script `utils/get_sra_run_samples.py` will generate a master download script in the top-level output directory that you can use to download all run data from the SRA:
    ```
    $ cd sra_data
    $ bash download_all.sh
    ```

5. Once all FASTQ files are downloaded, use `utils/analyze_seq_lens.py` to inspect all of the FASTQ files and verify that, for each run, all reads match the target length, the run has both forward and reverse reads, and the number of forward and reverse reads match.  E.g.:
    ```
    $ utils/analyze_seq_lens.py -d sra_data
    ```
    Any sequencing data files that do not meet the criteria listed above will be moved to a subfolder called `invalid`.  The utility will also output a new data file, `sra_runs-usable.csv`, that lists metadata for all runs with usable sequence data files.

6. The next step is to generate random samples of pairs of reads from the usable read data files to ensure that each data set is contributing equally to the estimated error profile.  The utility `utils/sample_reads.py` automatically generates paired read samples (n = 4,000,000 read pairs by default) from sets of FASTQ files.
    ```
    $ utils/sample_reads.py -p /PATH/TO/art_profiler_illumina -d sra_data/
    ```

7. The final step is to actually generate the error profiles.  ART comes with a tool for building Illumina error profiles, found in `ART_profiler_illumina/art_profiler_illumina`.  The read sampling utility `utils/sample_reads.py` will automatically generate a script for running `art_profiler_illumina`.  The script will be called `make_error_profiles.sh`; all you have to do is run this script. 
    ```
    $ bash make_error_profiles.sh
    ```

