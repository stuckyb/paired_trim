#!/usr/bin/python3

from Bio import Entrez
import csv
import time
from argparse import ArgumentParser
import xml.etree.ElementTree as ET
import os
import os.path
import random
import urllib
import pickle


# The maximum allowed wait time for retrying an HTTP request after failure.
MAX_RETRY_TIME = 16


class SRARecord:
    """
    Represents a single SRA summary record.
    """
    def __init__(self, rec_ID, metadata_str='', runinfo_str=''):
        """
        Instantiates an SRARecord, optionally from XML data.
        """
        self.rec_ID = rec_ID
        self.submitter_id = ''
        self.study_id = ''
        self.exp_id = ''
        self.sci_name = ''
        self.run_infos = []

        # Parse the metadata.
        if metadata_str != '':
            root = ET.fromstring(metadata_str)
            self.submitter_id = root.find('Submitter').get('acc')
            self.study_id = root.find('Study').get('acc')
            self.exp_id = root.find('Experiment').get('acc')
            self.sci_name = root.find('Organism').get('ScientificName')

        # Parse the run information.
        if runinfo_str != '':
            root = ET.fromstring(runinfo_str)
            for run in root.findall('Run'):
                run_info = {}
                run_info['id'] = run.get('acc')
                run_info['read_cnt'] = int(run.get('total_spots'))
                self.run_infos.append(run_info)


class SRARecordCache:
    """
    Implements a local on-disk cache for SRA summary records.
    """
    def __init__(self, cache_dir):
        self.cache_dir = cache_dir

        if not(os.path.exists(cache_dir)):
            os.mkdir(cache_dir)

    def lookup(self, rec_ID):
        record = None
        rec_path = os.path.join(self.cache_dir, rec_ID)

        if os.path.exists(rec_path):
            with open(rec_path, 'rb') as fin:
                record = pickle.load(fin)

        return record

    def add(self, record):
        rec_path = os.path.join(self.cache_dir, record.rec_ID)
        
        with open(rec_path, 'wb') as fout:
            pickle.dump(record, fout)


def esearchQuery(query, retstart=0):
    """
    Runs an esearch query with semi-intelligent error handling.
    """
    wait_time = 0.5
    success = False

    while not(success) and wait_time <= MAX_RETRY_TIME:
        try:
            handle = Entrez.esearch(
                db='sra', term=query, retstart=retstart, retmax=2000
            )
            recs = Entrez.read(handle)
            success = True
        except (urllib.error.HTTPError, urllib.error.URLError):
            print(
                'HTTP error encountered.  Waiting {0} seconds before '
                'retrying...'.format(wait_time)
            )
            time.sleep(wait_time)
            wait_time *= 2
    
    if not(success):
        raise Exception('Entrez esearch query failed: {0}'.format(query))
    
    return recs

def getSRARecordIDs(query):
    """
    Retrieves a complete set of record IDs for an SRA query.
    """
    ID_list = []
    recs = esearchQuery(query)
    rec_cnt = int(recs['Count'])
    ID_list += recs['IdList']
    while len(ID_list) < rec_cnt:
        # Wait 0.5 seconds to avoid hitting NCBI request limits.
        time.sleep(0.5)

        recs = esearchQuery(query, retstart=len(ID_list))
        ID_list += recs['IdList']

    #print(rec_cnt, len(ID_list))
    if rec_cnt != len(ID_list):
        raise Exception('Mismatched record count.')

    return ID_list

def esummaryQuery(rec_ID):
    """
    Runs an esummary query with semi-intelligent error handling.
    """
    wait_time = 0.5
    success = False

    while not(success) and wait_time <= MAX_RETRY_TIME:
        try:
            handle = Entrez.esummary(db='sra', id=rec_ID, retmode='xml')
            rec = Entrez.read(handle)
            success = True
        except (urllib.error.HTTPError, urllib.error.URLError):
            print(
                'HTTP error encountered.  Waiting {0} seconds before '
                'retrying...'.format(wait_time)
            )
            time.sleep(wait_time)
            wait_time *= 2
    
    if not(success):
        raise Exception(
            'Entrez esummary query failed for ID {0}'.format(rec_ID)
        )
    
    return rec

def getSRARecordSample(
    ID_list, read_len, platform, samp_size, min_reads, output_dir, log_csv,
    cache
):
    sci_names = set()
    submitters = set()
    studies = set()
    rec_IDs = set()
    samp_cnt = 0
    log_row = {}

    d_script = open(os.path.join(output_dir, 'download_reads.sh'), 'w')

    # Get a shuffled list of record IDs.
    rec_IDs = random.sample(ID_list, k=len(ID_list))

    while samp_cnt < samp_size and len(rec_IDs) > 0:
        # Get a previously unused record ID.
        rec_ID = rec_IDs[-1]
        rec_IDs.pop()

        print('Checking {0}...'.format(rec_ID))

        rec = cache.lookup(rec_ID)
        if rec is None:
            rec_xml = esummaryQuery(rec_ID)
            #print(rec_xml)

            rec = SRARecord(
                rec_ID,
                '<DocSum>' + rec_xml[0]['ExpXml'] + '</DocSum>',
                '<RunInfo>' + rec_xml[0]['Runs'] + '</RunInfo>'
            )
            cache.add(rec)

            # Wait 0.5 seconds to avoid hitting NCBI request limits.
            time.sleep(0.5)

        if (
            rec.sci_name not in sci_names and rec.sci_name is not None and
            rec.submitter_id not in submitters and rec.submitter_id is not None and
            rec.study_id not in studies and rec.study_id is not None
        ):
            for run in rec.run_infos:
                if run['read_cnt'] >= min_reads:
                    sci_names.add(rec.sci_name)
                    studies.add(rec.study_id)
                    submitters.add(rec.submitter_id)
                    samp_cnt += 1
                    print('FOUND ({0}/{1}): {2}, {3}'.format(
                        samp_cnt, samp_size, rec.exp_id, rec.sci_name
                    ))
                    log_row['read_length'] = read_len
                    log_row['platform'] = platform
                    log_row['experiment_id'] = rec.exp_id
                    log_row['study_id'] = rec.study_id
                    log_row['submitter_id'] = rec.submitter_id
                    log_row['scientific_name'] = rec.sci_name
                    log_row['run_id'] = run['id']
                    log_csv.writerow(log_row)
                    d_script.write('fastq-dump --split-files ' + run['id'] + '\n')

                    break

    d_script.close()

    if samp_cnt < samp_size:
        print(
            'WARNING: Not enough runs available at read length {0} to get a '
            'sample of the required size.'.format(read_len)
        )


argp = ArgumentParser(
    description='Retrieves SRA sequence data for generating sequencer error '
    'profiles.'
)
argp.add_argument(
    '-e', '--email', type=str, required=True,
    help='The email address to register with the NCBI E-utilities.'
)
argp.add_argument(
    '-y', '--start_year', type=str, required=False, default='2017',
    help='The starting year for data downloads.'
)
argp.add_argument(
    '-s', '--samp_size', type=int, required=False, default=8,
    help='The number of runs to sample for each read length/instrument '
    'combination (default: 8).'
)
argp.add_argument(
    '-m', '--min_reads', type=int, required=False, default=4000000,
    help='The minimum number of reads required per run (default: 4,000,000).'
)
argp.add_argument(
    '-o', '--output_dir', type=str, required=False, default='sra_data',
    help='The output directory (default: sra_data).'
)
argp.add_argument(
    'input_csv', type=str,
    help='An input CSV file with per-platform SRA count data.'
)

args = argp.parse_args()

Entrez.email = args.email

# Determine which read lengths we want to generate profiles for.  Only include
# read lengths with at least 1,000 SRA records.
target_lens = []
with open(args.input_csv) as fin:
    reader = csv.DictReader(fin)
    cur_len = ''
    read_cnt = 0
    for row in reader:
        if row['read_length'] != cur_len:
            if read_cnt >= 1000:
                target_lens.append(cur_len)
            cur_len = row['read_length']
            read_cnt = 0

        read_cnt += int(row['count'])

    if read_cnt >= 1000:
        target_lens.append(cur_len)

# The template query.
q_template = ('({0}[ReadLength]) AND '
'("' + args.start_year + '/01/01"[Publication Date] : '
'"3000"[Publication Date]) AND '
'("library layout paired"[Properties]) AND '
'("instrument {1}"[Properties]) AND '
'("WGS"[Strategy])')

log_csv_path = os.path.join(args.output_dir, 'sra_runs.csv')
main_script_path = os.path.join(args.output_dir, 'download_all.sh')
cache_path = os.path.join(args.output_dir, 'cache')

cache = SRARecordCache(cache_path)

# Get the sequence data samples.  For each target read length, get sequence
# data for the smallest set of sequencing instruments that collectively
# includes at least 75% of the SRA records for the target read length.
with open(args.input_csv) as fin, open(log_csv_path, 'w') as l_out, open(main_script_path, 'w') as s_out:
    reader = csv.DictReader(fin)
    writer = csv.DictWriter(
        l_out, [
            'read_length', 'platform', 'experiment_id', 'study_id',
            'submitter_id', 'run_id', 'scientific_name'
        ]
    )
    writer.writeheader()

    cur_len = ''
    count_prop = 0.0
    for row in reader:
        if row['read_length'] != cur_len:
            cur_len = row['read_length']
            count_prop = 0.0

        platform = row['platform']

        if count_prop < 0.75:
            print('Getting {0}-bp data for {1}...'.format(cur_len, platform))
            query = q_template.format(cur_len, platform)
            rec_IDs = getSRARecordIDs(query)

            samp_dir = cur_len + '-' + platform.replace(' ', '_')
            samp_dir_path = os.path.join(args.output_dir, samp_dir)
            if not(os.path.exists(samp_dir_path)):
                os.mkdir(samp_dir_path)

            s_out.write('cd {0}\n'.format(samp_dir))

            getSRARecordSample(
                rec_IDs, cur_len, platform, args.samp_size, args.min_reads,
                samp_dir_path, writer, cache
            )

            s_out.write('bash download_reads.sh\ncd ..\n\n')

        count_prop += float(row['count_prop'])

