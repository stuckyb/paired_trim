#!/usr/bin/python3

# Retrieves SRA record counts for various read lengths, sequencing instruments,
# and sequencing strategies.


from Bio import Entrez
import csv
import time
from argparse import ArgumentParser


def getSRARecordCount(query):
    handle = Entrez.egquery(term=query)
    rec = Entrez.read(handle)
    handle.close()

    for row in rec['eGQueryResult']:
        if row['DbName'] == 'sra':
            cnt = int(row['Count'])

    return cnt


READ_LENS = (50, 75, 100, 150, 250, 300)
PLATFORMS = (
    'illumina hiseq 2000', 'illumina hiseq 2500', 'illumina hiseq 3000',
    'illumina hiseq 4000', 'illumina miseq', 'illumina nextseq 500',
    'illumina nextseq 550', 'illumina novaseq 6000'
)
YEARS = (2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019)
STRATEGIES = ('', ' AND ("WGS"[Strategy])')

q_template_instrument = ('({0}[ReadLength]) AND '
'("{1}/01/01"[Publication Date] : "{1}/12/31"[Publication Date]) AND '
'("library layout paired"[Properties]) AND '
'("instrument {2}"[Properties]){3}')
q_template_overall = ('({0}[ReadLength]) AND '
'("{1}/01/01"[Publication Date] : "{1}/12/31"[Publication Date]) AND '
'"library layout paired"[Properties] AND '
'("Illumina"[Platform]){2}')


argp = ArgumentParser(
    description='Retrieves SRA record counts for various read lengths, '
    'sequencing instruments, and sequencing strategies.'
)
argp.add_argument(
    '-e', '--email', type=str, required=True,
    help='The email address to register with the NCBI E-utilities.'
)
argp.add_argument(
    '-o', '--output', type=str, required=False, default='sra_counts.csv',
    help='The output CSV file (default: sra_counts.csv).'
)

args = argp.parse_args()

Entrez.email = args.email

with open(args.output, 'w') as fout:
    writer = csv.DictWriter(
        fout, ('year', 'read_length', 'platform', 'count', 'strategy')
    )
    writer.writeheader()
    row = {}

    for strategy in STRATEGIES:
        for year in YEARS:
            for read_len in READ_LENS:
                for platform in PLATFORMS:
                    query = q_template_instrument.format(
                        read_len, year, platform, strategy
                    )
                    print(
                        'Query: {0} bp reads, {1}, '
                        '{2}'.format(read_len, year, platform)
                    )
                    cnt = getSRARecordCount(query)
                    print('  record count:', cnt)

                    row['year'] = year
                    row['read_length'] = read_len
                    row['platform'] = platform
                    row['count'] = cnt
                    if strategy == '':
                        row['strategy'] = 'ALL'
                    else:
                        row['strategy'] = 'WGS'
                    writer.writerow(row)

                    # Wait 0.5 seconds to avoid hitting NCBI request limits.
                    time.sleep(0.5)

                query = q_template_overall.format(read_len, year, strategy)
                print('Query: {0} bp reads, {1}'.format(read_len, year))
                cnt = getSRARecordCount(query)
                print('  record count:', cnt)

                row['year'] = year
                row['read_length'] = read_len
                row['platform'] = 'ILLUMINA'
                row['count'] = cnt
                if strategy == '':
                    row['strategy'] = 'ALL'
                else:
                    row['strategy'] = 'WGS'
                writer.writerow(row)

