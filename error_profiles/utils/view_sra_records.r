library('ggplot2')
library('dplyr')

raw_data = read.csv('sra_counts.csv')

# Examine raw record counts by read length per year.
ggplot(
    data=filter(raw_data, platform == 'ILLUMINA', year < 2019),
    aes(x=year, y=count, color=as.factor(read_length))
) + geom_line(size=1.6) +
    facet_grid(rows=vars(strategy))

# Examine proportional record counts by read length per year.
prop_data = filter(raw_data, platform == 'ILLUMINA') %>%
    group_by(year) %>% mutate(count_prop = count / sum(count))
ggplot(
    data=prop_data,
    aes(x=year, y=count_prop, color=as.factor(read_length))
) + geom_line(size=1.6) +
    facet_grid(rows=vars(strategy))

# Examine which instruments are most popular at each read length.
ggplot(
    data=filter(raw_data, platform != 'ILLUMINA', year >= 2017),
    aes(x=platform, y=count)
) + geom_bar(stat='identity') +
    facet_grid(rows=vars(strategy), cols=vars(read_length), scales='free_y') +
    theme(axis.text.x=element_text(angle=70, hjust=1))

# Generate a report to guide downloading SRA data.
report_data = filter(
    raw_data, platform != 'ILLUMINA', year >= 2017, strategy == strat_val
) %>%
    select(-year) %>%
    group_by(read_length, platform) %>%
    summarize(count = sum(count)) %>%
    mutate(count_prop = count / sum(count)) %>%
    arrange(read_length, desc(count_prop))
write.csv(report_data, 'platforms_report.csv')
