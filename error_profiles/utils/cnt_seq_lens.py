#!/usr/bin/python3

# Generates a report of sequence lengths in one or more FASTQ files.

import sys


if len(sys.argv) < 2:
    exit('\nERROR: Please provide a FASTQ input file.\n')

for fname in sys.argv[1:]:
    seq_lens = {}

    with open(fname) as fin:
        lcnt = 0
        for line in fin:
            if (lcnt - 1) % 4 == 0:
                seq_len = len(line.strip())
                if seq_len not in seq_lens:
                    seq_lens[seq_len] = 0

                seq_lens[seq_len] += 1

            lcnt += 1

    print('{0}:'.format(fname))
    for seq_len in sorted(seq_lens):
        print('  {0} b: {1:,} sequences'.format(seq_len, seq_lens[seq_len]))

