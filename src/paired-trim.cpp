/*
 * Copyright (C) 2019 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <string>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <filesystem>
using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::exception;
using std::ifstream;

#include "cxxopts.hpp"
#include "run_config.hpp"
#include "nuc_sequence.hpp"
#include "sequences_processor.hpp"


using ns_size_t = NucleotideSequence::size_type;
using cxxopts::OptionParseException;


cxxopts::Options getOptParser() {
    cxxopts::Options opts(
        "paired-trim",
        "Fast, accurate, and minimally destructive trimming of sequencing "
        "reads."
    );

    opts.add_options()
        ("h,help", "Print usage information.")
        (
            "1,R1_reads", "A FASTQ file of sequencing reads [REQUIRED].  If "
            "--R2_reads is also used, the file specified with --R1_reads "
            "should contain the forward sequencing reads.",
            cxxopts::value<string>(), "FASTQ_PATH"
        )
        (
            "2,R2_reads", "A FASTQ file of reverse sequencing reads that are "
            "paired with the reads in the file specified with --R1_reads.",
            cxxopts::value<string>(), "FASTQ_PATH"
        )
        (
            "s,suffix",
            "A suffix to append to the input file names to generate the "
            "output file names.",
            cxxopts::value<string>()->default_value("-paired_trim"), "SUFFIX"
        )
        (
            "o,output_dir",
            "A path to a folder in which to place the output files.",
            cxxopts::value<string>()->default_value("."), "DIR_PATH"
        )
    ;

    opts.add_options("Quality trimming")
        (
            "w,window_size",
            "The sliding window size to use for quality trimming.",
            cxxopts::value<ns_size_t>()->default_value("1"), "SIZE"
        )
        (
            "m,minimum_quality",
            "The minimum quality score to use for quality trimming.",
            cxxopts::value<int>()->default_value("0"), "MIN_QUAL"
        )
        (
            "c,minimum_highqual_count",
            "The minimum acceptable number of high-quality bases in the "
            "sliding window size to use for quality trimming.",
            cxxopts::value<ns_size_t>()->default_value("0"), "COUNT"
        )
    ;

    opts.add_options("Explicit end trimming")
        (
            "R1_trim_5p",
            "The number of bases to trim from the 5' end of all R1 (forward) "
            "sequencing reads.",
            cxxopts::value<ns_size_t>()->default_value("0"), "LENGTH"
        )
        (
            "R1_trim_3p",
            "The number of bases to trim from the 3' end of all R1 (forward) "
            "sequencing reads.",
            cxxopts::value<ns_size_t>()->default_value("0"), "LENGTH"
        )
        (
            "R2_trim_5p",
            "The number of bases to trim from the 5' end of all R2 (reverse) "
            "sequencing reads.",
            cxxopts::value<ns_size_t>()->default_value("0"), "LENGTH"
        )
        (
            "R2_trim_3p",
            "The number of bases to trim from the 5' end of all R2 (reverse) "
            "sequencing reads.",
            cxxopts::value<ns_size_t>()->default_value("0"), "LENGTH"
        )
    ;

    opts.add_options("Alignment-guided trimming")
        (
            "a,align_trim", "Enables alignment-guided trimming.",
            cxxopts::value<bool>()
        )
    ;

    opts.add_options("Explicit adapter trimming")
        (
            "R1_adapter",
            "The forward read adapter sequence.",
            cxxopts::value<string>(), "SEQUENCE"
        )
        (
            "R2_adapter",
            "The reverse read adapter sequence.",
            cxxopts::value<string>(), "SEQUENCE"
        )
    ;

    opts.add_options("Length filtering")
        (
            "l,minimum_length",
            "The minimum length required to include a sequence in the output "
            "files.  Length is calculated after all trimming operations are "
            "completed.",
            cxxopts::value<ns_size_t>()->default_value("1"), "LENGTH"
        )
    ;

    opts.add_options("Other")
        (
            "q,quality_offset",
            "The integer offset to use when interpreting encoded quality "
            "scores in FASTQ files.",
            cxxopts::value<int>()->default_value("33"), "OFFSET"
        )
        (
            "g,logging",
            "Log sequence processing details to the console in tab-delimited "
            "format.",
            cxxopts::value<bool>()
        )
        (
            "log_adapt_align",
            "Log adapter sequence alignment information for all reads, even "
            "if the adapter alignments are not to be used for trimming.  Has "
            "no effect if logging is not enabled.",
            cxxopts::value<bool>()
        )
        (
            "no_id_match",
            "Disables rigorous read ID matching when processing paired-end "
            "reads.",
            cxxopts::value<bool>()
        )
    ;

    return opts;
}


int main(int argc, char *argv[]) {
    RunConfig config;

    auto opt_p = getOptParser();

    try {
        auto args = opt_p.parse(argc, argv);

        if (args.count("help")) {
            cout << opt_p.help({
                "", "Quality trimming", "Explicit end trimming",
                "Alignment-guided trimming", "Explicit adapter trimming",
                "Length filtering", "Other"
            }) << endl;
            return 0;
        }

        config.fromCxxOpts(args);
    } catch(const OptionParseException &err) {
        cerr << "\nERROR: " << err.what() << ".\n" << endl;
        return 1;
    } catch(const RunConfigError &err) {
        cerr << "\nERROR: " << err.what() << endl << endl;
        return 1;
    } catch (...) {
        cerr << "\nERROR: Unknown exception encountered when processing "
            "command-line options..\n" << endl;
        return 1;
    }

    try {
        SequenceProcessor seq_processor(config);
        seq_processor.run();
    } catch (const exception &err) {
        cerr << "\nERROR: " << err.what() << endl << endl;
        return 1;
    } catch (...) {
        cerr << "\nERROR: Unknown exception encountered when processing "
            "sequencing reads.\n" << endl;
        return 1;
    }

    return 0;
}

