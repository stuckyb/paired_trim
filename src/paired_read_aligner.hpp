/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PAIRED_READ_ALIGNER_HPP
#define PAIRED_READ_ALIGNER_HPP

#include <istream>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <cctype>
#include <iterator>

#include "nuc_sequence.hpp"


/**
 * A simple exception class to indicate that input sequences are too long for
 * the requested operation.
 */
class InputSequencesTooLong: public std::runtime_error {
public:
    explicit InputSequencesTooLong(const std::string &message):
        std::runtime_error(message)
    {}
};


/*
 * @brief Implements semi-global pairwise sequence alignment.
 *
 * Provides the ability to calculate an optimal, semi-global, pairwise
 * alignment of two DNA sequences, with alignment parameters and supporting
 * functionality geared toward paired sequencing reads.  Alignment is
 * accomplished with a customized implementation of the
 * Needleman-Wunsch-Sankoff pairwise alignment algorithm.
 *
 * The default algorithm parameters/assumptions used here are designed for
 * aligning forward and reverse sequencing reads.  There are three important
 * things to note.  First, this implements semi-global alignment, so no
 * penalties are assessed for end gaps.  Second, so-called "affine gap
 * penalties" are not used.  All gaps are penalized the same, regardless of
 * whether they are initial gap openings or extensions.  This is because in
 * interpreting paired sequencing reads, all gaps are mistakes and should be
 * weighted equally.  The exception, of course, are end gaps, which are "free".
 * Third, mismatched bases are penalized exactly the same as gaps.  The
 * reasoning is the same: base mismatches in an alignment of paired sequencing
 * reads, like gaps, do not represent anything biologically real and are simply
 * sequencing mistakes.  Therefore, they should be weighted the same.
 */
class PairedReadAligner {
public:
    using ns_size_t = NucleotideSequence::size_type;

    /*
     * The default gap penalty is defined in _score_pr_table.cpp.
     */
    PairedReadAligner();

    /*
     * @brief Returns a reference to the scoring matrix.
     */
    static const int (&getScoreMatrix())[25][25] {
        return score_mat;
    }

    /*
     * @brief Returns the offset for the scoring matrix.
     */
    static const char getScoreMatrixOffset() {
        return sm_offset;
    }

    /*
     * @brief Returns the gap penalty.
     */
    int getGapPenalty() const {
        return gap_p;
    }

    /*
     * @brief Sets the gap penalty.
     */
    void setGapPenalty(int new_gap_penalty) {
        gap_p = new_gap_penalty;
    }

    /*
     * @brief Semi-global alignment of two sequences.
     *
     * Aligns two sequences using the Needleman-Wunsch-Sankoff algorithm, with
     * alignment scoring adapted to the problem of aligning paired sequencing
     * reads.  Specifically, end gaps are never penalized and affine gap
     * penalties for interior gaps are not used.  All interior gap positions
     * are penalized the same because with paired reads, every gap represents a
     * sequencing error.  Note that the reverse sequencing read is NOT
     * automatically reverse-complemented; if necessary, that should be done
     * prior to calling align().
     *
     * @return The optimal pairwise alignment score.
     */
    int align(NucleotideSequence &seq_fwd, NucleotideSequence &seq_rev);

    /*
     * @brief Prints the results of the last alignment to an output stream.
     */
    void printAlignment(std::ostream &out) const;

    /*
     * @brief Gets the score of the most recent alignment.
     */
    int getAlignmentScore() const {
        return align_score;
    }

    /*
     * @brief Gets the overlap length of the most recent alignment.
     */
    ns_size_t getOverlapLength() const {
        return overlap_len;
    }

    /*
     * Gets the index of the start of the region of sequence overlap in the
     * most recent alignment.
     */
    ns_size_t getOverlapStart() const {
        return overlap_start;
    }

    /*
     * Gets the index of the end of the region of sequence overlap in the most
     * recent alignment.  This index will be one past the last pair of
     * overlapping bases.
     */
    ns_size_t getOverlapEnd() const {
        return overlap_end;
    }

    /*
     * @brief Gets the average sequence identity of the most recent alignment.
     *
     * Gets the average pairwise sequence identity across the overlapping
     * region of the most recent alignment (i.e., end gaps are not included in
     * the calculation).  Mismatched non-ambiguous nucleotide codes (e.g.,
     * 'A','T') have identity 0.0, matched non-ambiguous nucleotide codes
     * (e.g., 'A','A') have identity 1.0, and pairs that include ambiguous
     * nucleotide codes are assigned an identity according to the probability
     * that the underlying actual bases are an exact match.  The average
     * identity is calculated in a way that is very fast, but as a result, the
     * individual pairwise identity values for some ambiguous nucleotide code
     * pairings will be subject to minor rounding error.
     */
    double getAverageIdentity() const;

    /*
     * @brief Returns a reference to the first aligned sequence.
     *
     * Returns a reference to the first aligned sequence.  This sequence
     * corresponds with the first sequence passed into align().
     */
    const std::string &getAlignedFwdSeq() const {
        return al_fwd_seq;
    }

    /*
     * @brief Returns a reference to the second aligned sequence.
     *
     * Returns a reference to the second aligned sequence.  This sequence
     * corresponds with the second sequence passed into align().
     */
    const std::string &getAlignedRevSeq() const {
        return al_rev_seq;
    }

    /*
     * For the most recent alignment, returns the length of the forward
     * sequence that overhangs the region of sequence overlap.  Only overhang
     * on the 3' end (that is, the end of the sequence) of the forward sequence
     * is counted.  This method will always return 0 when the reverse sequence
     * is empty.
     */
    const ns_size_t getForwardOverhangCnt() const {
        return fwd_overhang_cnt;
    }

    /*
     * For the most recent alignment, returns the length of the reverse
     * sequence that overhangs the region of sequence overlap.  Only overhang
     * on the 5' end (that is, the start of the sequence) of the reverse
     * sequence is counted.  Note that the 5' end of the aligned reverse
     * sequence corresponds with the 3' end of the reverse-complemented reverse
     * sequence (that is, the 3' end of the original read data).  This method
     * will always return 0 when the forward sequence is empty.
     */
    const ns_size_t getReverseOverhangCnt() const {
        return rev_overhang_cnt;
    }

    /*
     * Tests the null hypothesis that the most recent alignment is an alignment
     * of random sequences, at alpha = 0.001.  Returns true if the most recent
     * alignment is significantly different from a an alignment of random
     * sequences (that is, if the null hypothesis is rejected.  Uses a
     * pre-computed table of probabilities based on simulated alignment score
     * distributions generated by sampling optimal semi-global alignments of
     * 20,000 pairs of random sequences at each sequence length.  The
     * probability table includes sequence lengths of up to 1,000 nucleotides;
     * an attempt to call this method on an alignment with input sequences >
     * 1,000 nucleotides will throw an InputSequencesTooLong exception.
     *
     * The hypothesis test implemented by this method is conservative in two
     * key ways:
     * 1. Probability table values for sequence lengths > 100 are only
     * calculated for lengths that are multiples of 10.  Input sequence lengths
     * > 100 will be rounded up to the nearest multiple of 10 to look up the
     * alignment score cutoff.  This has the effect of making the test more
     * conservative (i.e., making alpha < 0.001) because score cutoff values
     * tend to increase as sequence lengths increase.
     *
     * 2. The probability table values were calculated using distributions of
     * alignment scores for alignments of sequences that are the same length.
     * If the input sequences in the alignment are not the same length, the
     * length of the longest input sequence will be used to look up the
     * alignment score cutoff.  This also makes the test more conservative,
     * because alignment scores tend to be lower if one sequence is shorter
     * than the other than they would be if both sequences were the length of
     * the longer sequence (this makes sense, of course, because the maximum
     * possible alignment score is less when one sequence is shorter than the
     * other).
     */
    bool testSignif() const;

    /*
     * For aligments with overhang on the 3' end of the forward read and the 5'
     * end of the reverse read (i.e., alignments that could be trimmed), this
     * method checks if an alternative alignment with overhang on the 5' end of
     * the forward read and the 3' end of the reverse read is plausible.  To
     * save computing time, this does not do a full alignment of the two
     * sequences.  Instead, it checks if the first match_size number of bases
     * of the reverse read (24 by default) are a perfect match (allowing for a
     * number of mismatches, defined by tolerance) for any position on the
     * forward read.  Under a random sequences scenario, with match_size == 24
     * and tolerance == 0, the probability of a match for any given position in
     * the forward sequence is 1 / 4^24 (about 1 in 2.8 * 10^14, or 1 in 2.8
     * septillion).  If tolerance == 1, the probability increases to 73 / 4^24
     * (about 1 in 3.9 trillion).  Returns true if a match was found.  Note
     * that, as with align(), this method does not automatically reverse
     * complement the reverse read.  Also, this method only does simple base
     * equality testing and does not do anything special with ambiguous
     * nucleotide codes.
     */
    bool checkAltAlignment (
        const NucleotideSequence &fwd_seq, const NucleotideSequence &rev_seq,
        int match_size=24, int tolerance=0
    ) const;

private:
    // Alignment scoring matrix.
    static const int score_mat[25][25];
    static const char sm_offset;

    // The gap penalty.  The default gap penalty is defined in
    // _score_pr_table.cpp.
    int gap_p;

    // Traceback direction constants.
    enum TraceDirection : unsigned char {UP, LEFT, DIAG};

    // Pointers to memory chunks for the score and traceback matrices.
    std::unique_ptr<int[]> scores = nullptr;
    std::unique_ptr<TraceDirection[]> tracebk = nullptr;

    // Space reserved for matrices, recorded as a 1-dimensional array length.
    ns_size_t mat_reserved = 0;

    // Input sequence lengths of the most recent alignment.
    ns_size_t fwd_seq_len = 0;
    ns_size_t rev_seq_len = 0;

    // Strings to hold the alignment.
    std::string al_fwd_seq;
    std::string al_rev_seq;

    // Score of the most recent alignment.
    int align_score = 0;

    // Alignment overlap length.
    ns_size_t overlap_len = 0;

    // Alignment overlap indices.
    ns_size_t overlap_start = 0;
    ns_size_t overlap_end = 0;

    // Lengths of 3' overhangs outside the alignment overlap.
    ns_size_t fwd_overhang_cnt = 0;
    ns_size_t rev_overhang_cnt = 0;

    // Probability table for testing alignment scores.
    static const int score_pr_table[191];
    static const ns_size_t MAX_PR_LEN = 1000;
};

#endif /* PAIRED_READ_ALIGNER_HPP */
