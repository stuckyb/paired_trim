/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FASTQ_EXCEPTIONS_HPP
#define FASTQ_EXCEPTIONS_HPP

#include <stdexcept>
#include <string>


/**
 * @brief Exception class to indicate no output.
 * 
 * A simple exception class used to indicate no output source was provided.
 */
class FASTQNoOutput: public std::runtime_error {
public:
    explicit FASTQNoOutput(const std::string &message):
        std::runtime_error(message)
    {}
};


/**
 * @brief Exception class to indicate no input.
 * 
 * A simple exception class used to indicate no input source was provided.
 */
class FASTQNoInput: public std::runtime_error {
public:
    explicit FASTQNoInput(const std::string &message):
        std::runtime_error(message)
    {}
};


/**
 * @brief Exception class to indicate end of input when parsing a FASTQ stream.
 * 
 * A simple exception class used to indicate that an input source has been
 * exhausted when parsing a FASTQ stream.
 */
class FASTQNoMoreInput: public std::exception { };


/**
 * @brief Exception class for FASTQ parse errors.
 * 
 * A simple exception class for reporting errors encountered when reading
 * FASTQ-format inputs.
 */
class FASTQParseError: public std::exception {
public:
    /**
     * @brief Constructs a FASTQParseError with a simple error message.
     */
    explicit FASTQParseError(const std::string &message):
      msg(message), line(0), context() {
    }
    
    /**
     * @brief Constructs a FASTQParseError that reports error location.
     * 
     * Constructs a FASTQParseError with a detailed error message that includes
     * the line number at which the error was detected and a custom error
     * message.  If applicable, a string containing the context of the error in
     * the input source can also be included.
     * 
     * @param message A custom message describing the error.
     * @param linenum Input line number at which the error was detected.
     * @param context The context of the error in the input source.
     */
    explicit FASTQParseError(
        const std::string &message, unsigned linenum,
        const std::string &context=""
    );
    
    /**
     * @brief Returns the error message.
     * 
     * Returns the error message associated with the exception.  If the line
     * number and column were provided, they will be included in the message.
     * 
     * @return The error message.
     */
    virtual const char *what() const noexcept;

protected:
    std::string msg;
    unsigned line;
    std::string context;
};

#endif /* FASTQ_EXCEPTIONS_HPP */
