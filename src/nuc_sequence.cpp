/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "nuc_sequence.hpp"
#include <stdexcept>
#include <cctype>
#include <cstddef>
#include <iostream>

using std::string;
using std::ostringstream;
using std::vector;
using std::runtime_error;
using std::toupper;
using std::size_t;


/******
 * Members of NucleotideSequence.
 ******/

// All valid IUPAC nucleotide codes.
const string NucleotideSequence::_valid_codes("ATGCBDHKMRSVWYN");

// Lookup table for base complements, implemented as an array of chars.  Given
// some char base value, bval, this data structure works by defining the value
// at index (bval - _clookup_offset) to be the complementary base of bval.  For
// example, if bval == 'A', then the index of the complementary base is at
// ('A' - _clookup_offset) == 0.
const char NucleotideSequence::_compl_lookup[] = {
    'T', 'V', 'G', 'H', 0, 0, 'C', 'D', 0, 0, 'M', 0, 'K', 'N', 0, 0, 0,
    'Y', 'S', 'A', 0, 'B', 'W', 0, 'R'
};
const char NucleotideSequence::_clookup_offset = 'A';

NucleotideSequence::NucleotideSequence(
    const string &bases_str, const std::string &id_str1,
    const std::string &id_str2, int read_num
): NucleotideSequence(
       bases_str, vector<int>(bases_str.size(), -1), id_str1, id_str2, read_num
) {
}

NucleotideSequence::NucleotideSequence(
    const string &bases_str, const vector<int> &qual_scores_vec,
    const std::string &id_str1, const std::string &id_str2,
    int read_num
): _bases(bases_str), _qual_scores(qual_scores_vec), _id_str1(id_str1),
   _id_str2(id_str2), _read_num(read_num)
{
    if (_bases.size() != _qual_scores.size()) {
        throw runtime_error(
            "The number of bases and number of quality scores must match."
        );
    }

    // Verify that all base characters are valid.
    for (auto &base : _bases) {
        base = toupper(base);

        if (_valid_codes.find(base) == string::npos) {
            throw runtime_error(
                string("The character '") + base +
                "' is not a valid nucleotide code."
            );
        }
    }
}

string NucleotideSequence::getTitle() const {
    std::ostringstream title;
    
    if (_read_num != -1) {
        title << _id_str1 << ' ' << _read_num << ':' << _id_str2;
    } else {
        title << _id_str1 << _id_str2;
    }

    return title.str();
}

bool NucleotideSequence::operator==(const NucleotideSequence &rhs) const {
    return _id_str1 == rhs._id_str1 &&
        _id_str2 == rhs._id_str2 &&
        _read_num == rhs._read_num &&
        _is_revcomp == rhs._is_revcomp &&
        _bases == rhs._bases &&
        _qual_scores == rhs._qual_scores;
}

bool NucleotideSequence::operator!=(const NucleotideSequence &rhs) const {
    return !(operator==(rhs));
}

void NucleotideSequence::reverseComplement() {
    if (length() == 0) {
        _is_revcomp = !(_is_revcomp);
        return;
    }

    int t_int;
    char t_char;
    size_type i1, i2;

    for (i1 = 0, i2 = length() - 1; i1 < i2; ++i1, --i2) {
        t_char = _bases[i1];
        _bases[i1] = _compl_lookup[_bases[i2] - _clookup_offset];
        _bases[i2] = _compl_lookup[t_char - _clookup_offset];
        
        t_int = _qual_scores[i1];
        _qual_scores[i1] = _qual_scores[i2];
        _qual_scores[i2] = t_int;    
    }

    // Sequences with odd numbers of bases will need to have the middle base
    // complemented after running the previous loop, because the middle base
    // will not have been involved in a swap operation.
    if (i1 == i2) {
        _bases[i1] = _compl_lookup[_bases[i1] - _clookup_offset];
    }

    _is_revcomp = !(_is_revcomp);
}

void NucleotideSequence::trimStart(size_type base_cnt) {
    if (length() < 1) {
        return;
    }

    if (base_cnt > length()) {
        base_cnt = length();
    }

    _bases.erase(0, base_cnt);
    _qual_scores.erase(_qual_scores.begin(), _qual_scores.begin() + base_cnt);
}

void NucleotideSequence::trimEnd(size_type base_cnt) {
    if (length() < 1 || base_cnt < 1) {
        return;
    }

    if (base_cnt > length()) {
        base_cnt = length();
    }

    auto start_len = length();

    _bases.erase(start_len - base_cnt, base_cnt);
    _qual_scores.erase(
        _qual_scores.begin() + (start_len - base_cnt), _qual_scores.end()
    );
}

void NucleotideSequence::checkQualityTrimParams(
    size_type win_size, size_type min_qual, size_type min_highqual_cnt
) {
    if (min_highqual_cnt > win_size) {
        throw runtime_error(
            "The minimum high-quality base count cannot exceed the window size "
            "for quality trimming."
        );
    }

    if (win_size < 1) {
        throw runtime_error(
            "Invalid window size for quality trimming."
        );
    }
}

void NucleotideSequence::qualityTrimStart(
    size_type win_size, int min_qual, size_type min_highqual_cnt
) {
    checkQualityTrimParams(win_size, min_qual, min_highqual_cnt);

    if (win_size > length()) {
        trimEnd(length());
        return;
    }

    // i1 is the index of the start of the window, highqual_cnt is the number
    // of high-quality bases inside the current window.
    size_type i1 = 0;
    size_type highqual_cnt = 0;

    // Initialize the sliding window.
    for (; i1 < win_size; ++i1) {
        if (_qual_scores[i1] >= min_qual) {
            ++highqual_cnt;
        }
    }

    // Slide the window along the sequence, stopping either at the end of the
    // sequence or when the count of high-quality bases meets the minimum.
    i1 = 0;
    while (
        highqual_cnt < min_highqual_cnt &&
        (i1 + win_size) < length()
    ) {
        if (_qual_scores[i1 + win_size] >= min_qual) {
            ++highqual_cnt;
        }
        if (_qual_scores[i1] >= min_qual) {
            --highqual_cnt;
        }

        ++i1;
    }

    size_type trim_len;
    if (highqual_cnt < min_highqual_cnt) {
        trim_len = length();
    } else {
        trim_len = i1;
    }

    trimStart(trim_len);
}

void NucleotideSequence::qualityTrimEnd(
    size_type win_size, int min_qual, size_type min_highqual_cnt
) {
    checkQualityTrimParams(win_size, min_qual, min_highqual_cnt);

    if (win_size > length()) {
        trimEnd(length());
        return;
    }

    // i1 is the index of the start of the window, originating from the end of
    // the sequence, and highqual_cnt is the number of high-quality bases
    // inside the current window.
    size_type i1 = length() - 1;
    size_type highqual_cnt = 0;

    // Initialize the sliding window.
    for (size_type i2 = 0; i2 < win_size; ++i2) {
        if (_qual_scores[i1 - i2] >= min_qual) {
            ++highqual_cnt;
        }
    }

    // Slide the window along the sequence, stopping either at the beginning of
    // the sequence or when the count of high-quality bases meets the minimum.
    while (
        highqual_cnt < min_highqual_cnt &&
        (i1 + 1 - win_size) > 0
    ) {
        if (_qual_scores[i1 - win_size] >= min_qual) {
            ++highqual_cnt;
        }
        if (_qual_scores[i1] >= min_qual) {
            --highqual_cnt;
        }

        --i1;
    }

    size_type trim_len;
    if (highqual_cnt < min_highqual_cnt) {
        trim_len = length();
    } else {
        trim_len = length() - (i1 + 1);
    }

    trimEnd(trim_len);
}

