/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "paired_read_aligner.hpp"
#include <stdexcept>
#include <cctype>
#include <cstddef>
#include <iostream>

using std::string;
using std::vector;
using std::runtime_error;
using std::toupper;
using std::size_t;
using std::cout;
using std::endl;


/******
 * Members of PairedReadAligner.
 ******/

// Define the substitution score matrix.  This matrix includes all IUPAC DNA
// nucleotide codes.  The scores were generated as follows.  Perfect matches
// get 6 points and complete mismatches get -6 points.  Ambiguous nucleotides
// are scored as the average score of all possible matches/mismatches.  Another
// way to think of this is that the score for an ambiguous nucleotide will fall
// between 6 and -6 according to the probability of an exact match.
// Mathematically, the scores can be expressed as score = P(exact match) * 12 -
// 6. Multiplying by 12 simply provides a scaling factor so that most of the
// scores work out to integer values.
 
// The matrix is implemented as a 2-D array of ints that is indexed directly by
// nucleotide code values, minus an offset.  This allows for very fast score
// lookups at the cost of some wasted space due to the sparseness of the
// matrix.  For example, to find the score of an alignment position with the
// bases 'A' and 'T', look up
// score_mat['A' - sm_offset]['T' - sm_offset],
// which is -6.  It would be possible to make the matrix compact by
// implementing an additional step that maps nucleotide codes to the range
// [0:15], but the relatively minor space savings don't seem worth the
// additional instructions that would be required for each lookup.
const int PairedReadAligner::score_mat[25][25] = {
//     A  B  C  D        G  H        K     M  N           R  S  T     V  W     Y
/*A*/{ 6,-6,-6,-2, 0, 0,-6,-2, 0, 0,-6, 0, 0,-3, 0, 0, 0, 0,-6,-6, 0,-2, 0, 0,-6},
/*B*/{-6,-2,-2,-3, 0, 0,-2,-3, 0, 0,-2, 0,-4,-3, 0, 0, 0,-4,-2,-2, 0,-3,-4, 0,-2},
/*C*/{-6,-2, 6,-6, 0, 0,-6,-2, 0, 0,-6, 0, 0,-3, 0, 0, 0,-6, 0,-6, 0,-2,-6, 0, 0},
/*D*/{-2,-3,-6,-2, 0, 0,-2,-3, 0, 0,-2, 0,-4,-3, 0, 0, 0,-2,-4,-2, 0,-3,-2, 0,-4},
     { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
/*G*/{-6,-2,-6,-2, 0, 0, 6,-6, 0, 0, 0, 0,-6,-3, 0, 0, 0, 0, 0,-6, 0,-2,-6, 0,-6},
/*H*/{-2,-3,-2,-3, 0, 0,-6,-2, 0, 0,-4, 0,-2,-3, 0, 0, 0,-4,-4,-2, 0,-3,-2, 0,-2},
     { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
/*K*/{-6,-2,-6,-2, 0, 0, 0,-4, 0, 0, 0, 0,-6,-3, 0, 0, 0,-3,-3, 0, 0,-4,-3, 0,-3},
     { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
/*M*/{ 0,-4, 0,-4, 0, 0,-6,-2, 0, 0,-6, 0, 0,-3, 0, 0, 0,-3,-3,-6, 0,-2,-3, 0,-3},
/*N*/{-3,-3,-3,-3, 0, 0,-3,-3, 0, 0,-3, 0,-3,-3, 0, 0, 0,-3,-3,-3, 0,-3,-3, 0,-3},
     { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
/*R*/{ 0,-4,-6,-2, 0, 0, 0,-4, 0, 0,-3, 0,-3,-3, 0, 0, 0, 0,-3,-6, 0,-2,-3, 0,-6},
/*S*/{-6,-2, 0,-4, 0, 0, 0,-4, 0, 0,-3, 0,-3,-3, 0, 0, 0,-3, 0,-6, 0,-2,-6, 0,-3},
/*T*/{-6,-2,-6,-2, 0, 0,-6,-2, 0, 0, 0, 0,-6,-3, 0, 0, 0,-6,-6, 6, 0,-6, 0, 0, 0},
     { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
/*V*/{-2,-3,-2,-3, 0, 0,-2,-3, 0, 0,-4, 0,-2,-3, 0, 0, 0,-2,-2,-6, 0,-2,-4, 0,-4},
/*W*/{ 0,-4,-6,-2, 0, 0,-6,-2, 0, 0,-3, 0,-3,-3, 0, 0, 0,-3,-6, 0, 0,-4, 0, 0,-3},
     { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
/*Y*/{-6,-2, 0,-4, 0, 0,-6,-2, 0, 0,-3, 0,-3,-3, 0, 0, 0,-6,-3, 0, 0,-4,-3, 0, 0}
};

const char PairedReadAligner::sm_offset = 'A';

// Include the code that defines the default gap penalty and the lookup table
// for testing alignment significance.  The gap penalty and significance cutoff
// (i.e., alpha) are hyperparameters that guide sequence trimming, and defining
// the gap penalty and lookup table in an external file makes it much easier to
// automate exploration of hyperparameter space.  Making the gap penalty a
// command-line option would be a bad idea because if the gap penalty is not
// compatible with the significance lookup table, then alignment significance
// tests will be incorrect.
#include "_score_pr_table.cpp_"


PairedReadAligner::PairedReadAligner(): gap_p(DEFAULT_GAP_PENALTY) {}


// Define a macro to simplify addressing matrix arrays allocated as
// 1-dimensional contiguous arrays.
#define I(i, j) (i) * mat_w + j

int PairedReadAligner::align(
    NucleotideSequence &fwd_seq, NucleotideSequence &rev_seq
) {
    fwd_seq_len = fwd_seq.length();
    rev_seq_len = rev_seq.length();

    // The required matrix dimensions.
    auto mat_w = rev_seq_len + 1;
    auto mat_h = fwd_seq_len + 1;

    // Make sure that the memory chunks for the score and traceback matrices
    // are large enough to accommodate the input sequences, and reallocate if
    // needed.
    if (mat_reserved < mat_w * mat_h) {
        mat_reserved = mat_w * mat_h;
        scores.reset(new int[mat_reserved]);
        tracebk.reset(new TraceDirection[mat_reserved]);
    }

    // Initialize the score and traceback matrices.
    for (ns_size_t i = 0; i < fwd_seq_len + 1; ++i) {
        scores[I(i, 0)] = 0;
        tracebk[I(i, 0)] = UP;
    }
    for (ns_size_t j = 0; j < rev_seq_len + 1; ++j) {
        scores[I(0, j)] = 0;
        tracebk[I(0, j)] = LEFT;
    }

    // Hold intermediate score calculations.
    int s_diag, s_up, s_left;
    // Hold scoring matrix indices.
    size_t sm_i, sm_j;

    // Calculate the scores for the alignment matrix and the directional markers
    // for the traceback matrix.  To avoid the need to reverse the aligned
    // sequences at the end of the algorithm, we inspect the bases of the input
    // sequences in reverse order here.  That is; i=1, j=1 in the matrices
    // corresponds with the ends of the input sequences, and i=fwd_seq_len,
    // j=rev_seq_len in the matrices corresponds with index 0 in the input
    // sequences.  This is more efficient than reversing the alignment at the
    // end, because it eliminates the sequence reversals without introducing
    // any additional computations elsewhere in the algorithm.  In the standard
    // implementation of the Needleman-Wunsch-Sankoff algorithm, i=1, j=1 in
    // the matrix corresponds with index 0 in the input sequences, which means
    // that sequence indexing still requires arithmetic on the matrix indices.
    for (ns_size_t i = 1; i < fwd_seq_len + 1; ++i) {
        for (ns_size_t j = 1; j < rev_seq_len + 1; ++j) {
            // Calculate the subscores for this position.
            sm_i = fwd_seq.bases()[fwd_seq_len - i] - sm_offset;
            sm_j = rev_seq.bases()[rev_seq_len - j] - sm_offset;
            s_diag = scores[I(i-1, j-1)] + score_mat[sm_i][sm_j];

            s_up = scores[I(i-1, j)] + gap_p;
            s_left = scores[I(i, j-1)] + gap_p;

            // Do not assess a penalty for end gaps.
            if (i == fwd_seq_len) { s_left -= gap_p; }
            if (j == rev_seq_len) { s_up -= gap_p; }

            // Record the maximum subscore and direction.
            if (s_diag >= s_up && s_diag >= s_left) {
                scores[I(i, j)] = s_diag;
                tracebk[I(i, j)] = DIAG;
            } else if (s_up >= s_diag && s_up >= s_left) {
                scores[I(i, j)] = s_up;
                tracebk[I(i, j)] = UP;
            } else {
                scores[I(i, j)] = s_left;
                tracebk[I(i, j)] = LEFT;
            }
        }
    }

    // Use the traceback matrix to generate an optimal alignment.  Again, note
    // that sequence indexing is such that the bottom-right corner of the
    // matrix (i=fwd_seq_len, j=rev_seq_len) corresponds with index 0 in the input
    // sequences.  We can therefore build the alignment from index 0 with no
    // need to reverse the sequences afterwards.  While building the alignment,
    // we also count the length of the overlapping parts of the two sequences
    // and the lengths of sequence overhangs.
    al_fwd_seq.clear();
    al_rev_seq.clear();
    overlap_start = overlap_end = 0;
    ns_size_t i = fwd_seq_len;
    ns_size_t j = rev_seq_len;
    bool in_end_gap = true;
    while (i > 0 || j > 0) {
        if (tracebk[I(i, j)] == DIAG) {
            al_fwd_seq.push_back(fwd_seq.bases()[fwd_seq_len - i]);
            al_rev_seq.push_back(rev_seq.bases()[rev_seq_len - j]);
            if (in_end_gap) {
                in_end_gap = false;
                rev_overhang_cnt = rev_seq_len - j;
                overlap_start = al_fwd_seq.size() - 1;
            }
            i -= 1;
            j -= 1;
        } else if (tracebk[I(i, j)] == UP) {
            al_fwd_seq.push_back(fwd_seq.bases()[fwd_seq_len - i]);
            al_rev_seq.push_back('-');
            i -= 1;
        } else {
            al_fwd_seq.push_back('-');
            al_rev_seq.push_back(rev_seq.bases()[rev_seq_len - j]);
            j -= 1;
        }

        if (!in_end_gap && (i == 0 || j == 0)) {
            in_end_gap = true;
            fwd_overhang_cnt = i;
            overlap_end = al_fwd_seq.size();
        }
    }

    align_score = scores[I(fwd_seq_len, rev_seq_len)];
    overlap_len = overlap_end - overlap_start;

    return align_score;
}

double PairedReadAligner::getAverageIdentity() const {
    double avg_identity;
    int adj_score = align_score;

    if (gap_p != -6) {
        // Calculate an adjusted alignment score that is equivalent to what the
        // score would have been if the the gap penalty were -6.
        int adj_val = -6 - gap_p;
        for (ns_size_t i = getOverlapStart(); i < getOverlapEnd(); ++i) {
            if (al_fwd_seq[i] == '-' || al_rev_seq[i] == '-') {
                adj_score += adj_val;
            }
        }
    }

    // Because the entries in the alignment score matrix are based on the exact
    // match probabilities of all possible nucleotide code pairs and the gap
    // penalty is equivalent to P(match) = 0 (or we have adjusted the alignment
    // score as if the gap penalty were equivalent to P(match) = 0), we can use
    // the alignment score and the overlap length to calculate the average
    // identity of the alignment in a single step.  The calculated average
    // identity will assign partial identity values to pairs that include
    // ambiguous nucleotide codes.  Note that in the cases of ambiguous codes,
    // the partial identity is subject to rounding error due to the integral
    // values used in the score matrix, and so the resulting alignment identity
    // calculation should be treated as a good approximation.
    if (overlap_len > 0) {
        avg_identity = double(adj_score + overlap_len * 6) /
            (12 * overlap_len);
    } else {
        avg_identity = 0.0;
    }

    return avg_identity;
}

bool PairedReadAligner::testSignif() const {
    // Use the length of the longest input sequence.
    auto seq_len = (fwd_seq_len > rev_seq_len) ? fwd_seq_len : rev_seq_len;

    if (seq_len > MAX_PR_LEN) {
        throw(InputSequencesTooLong(
            "The input sequence length exceeds the maximum sequence length for "
            "testing alignment score probabilities."
        ));
    }
    
    // Calculate the index in the lookup table.  Probability cutoffs are
    // included for all sequence lengths from 1 to 100 (inclusive), and then
    // for every multiple of 10 greater than 100, up to 1,000 (inclusive).
    size_t lookup_i;
    if (seq_len > 100) {
        // Calculate the index, rounding up to the nearest multiple of 10.  The
        // formula is simplified from the more intuitive
        // lookup_i = (seq_len + 9 - 100) / 10 + 100;
        lookup_i = (seq_len + 9) / 10 + 90;
    } else {
        lookup_i = seq_len;
    }

    if (align_score > score_pr_table[lookup_i]) {
        // Reject the null hypothesis (p < 0.001).
        return true;
    } else {
        // Do not reject the null hypothesis (p > 0.001).
        return false;
    }
}

void PairedReadAligner::printAlignment(std::ostream &out) const {
    size_t line_len = 77;
    size_t i = 0;

    if (al_fwd_seq.size() == 0) {
        cout << "1:" << endl << "2:" << endl;
    } else {
        while (i < al_fwd_seq.size()) {
            cout << "1: " << al_fwd_seq.substr(i, line_len) << endl;
            cout << "2: " << al_rev_seq.substr(i, line_len) << endl;
            i += line_len;
        }
    }

    cout << "   score: " << align_score << "; overlap: " <<
        overlap_len << "bp; identity: " << getAverageIdentity() << endl;
}

bool PairedReadAligner::checkAltAlignment (
    const NucleotideSequence &fwd_seq, const NucleotideSequence &rev_seq,
    int match_size, int tolerance
) const {
    int match_cnt, mismatch_cnt;

    if (fwd_seq.length() < match_size || rev_seq.length() < match_size) {
        return false;
    }

    for (int i = 0; i <= fwd_seq.length() - match_size; ++i) {
        match_cnt = mismatch_cnt = 0;

        for (int j = 0; j < match_size; ++j) {
            if (fwd_seq.bases()[i + j] != rev_seq.bases()[j]) {
                if (mismatch_cnt == tolerance) {
                    break;
                } else {
                    ++mismatch_cnt;
                }
            } else {
                ++match_cnt;
            }
        }

        if (match_cnt + mismatch_cnt == match_size) {
            return true;
        }
    }

    return false;
}

