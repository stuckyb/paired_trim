/*
 * Copyright (C) 2019 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "run_config.hpp"

using std::string;
using std::ifstream;
using std::ofstream;
using std::cout;
using std::endl;
using cxxopts::OptionParseException;
using std::filesystem::path;
using std::cout;
using std::endl;


void RunConfig::fromCxxOpts(const cxxopts::ParseResult &args) {
    string r1_path, r2_path;

    if (args.count("R1_reads") < 1) {
        throw RunConfigError("No FASTQ file was specified.");
    }
    r1_path = args["R1_reads"].as<string>();

    // We process the R2 and sequencing adapter default values this way rather
    // than providing default values explicitly to cxxopts because an empty
    // string default results in a confusing help message.
    if (args.count("R2_reads") < 1) {
        r2_path = "";
    } else {
        r2_path = args["R2_reads"].as<string>();
    }

    qual_offset = args["quality_offset"].as<int>();

    // Quality trimming options.
    min_qual = args["minimum_quality"].as<int>();
    win_size = args["window_size"].as<ns_size_t>();
    min_highqual_cnt = args["minimum_highqual_count"]
        .as<ns_size_t>();

    if (win_size < 1) {
        throw RunConfigError(
            "The minimum window size for quality trimming cannot be less "
            "than 1."
        );
    }
    if (min_highqual_cnt > win_size) {
        throw RunConfigError(
            "The minimum count of high-quality bases for quality trimming "
            "cannot exceed the window size."
        );
    }

    // Explicit end trimming options.
    r1_trim_5p = args["R1_trim_5p"].as<ns_size_t>();
    r1_trim_3p = args["R1_trim_3p"].as<ns_size_t>();
    r2_trim_5p = args["R2_trim_5p"].as<ns_size_t>();
    r2_trim_3p = args["R2_trim_3p"].as<ns_size_t>();

    // Alignment-guided trimming options.
    align_trim = args["align_trim"].as<bool>();

    // Sequencing adapters.
    if (args.count("R1_adapter") < 1) {
        // If an R2 adapter was provided, use it for the R1 adapter, too.
        if (args.count("R2_adapter") > 0) {
            r1_adapter = NucleotideSequence(args["R2_adapter"].as<string>());
        } else {
            r1_adapter = NucleotideSequence("");
        }
    } else {
        r1_adapter = NucleotideSequence(args["R1_adapter"].as<string>());
    }

    if (args.count("R2_adapter") < 1) {
        // If an R1 adapter was provided, use it for the R2 adapter, too.
        if (args.count("R1_adapter") > 0) {
            r2_adapter = NucleotideSequence(args["R1_adapter"].as<string>());
        } else {
            r2_adapter = NucleotideSequence("");
        }
    } else {
        r2_adapter = NucleotideSequence(args["R2_adapter"].as<string>());
    }

    // Length-filtering options.
    min_length = args["minimum_length"].as<ns_size_t>();

    // Verbose output mode.
    logging = args["logging"].as<bool>();

    // Whether to log adapter alignment information for all reads.
    log_adapt_align = args["log_adapt_align"].as<bool>();

    // Rigorous ID matching for paired-end reads.
    match_paired_ids = !(args["no_id_match"].as<bool>());

    // Output file parameters.
    string suffix = args["suffix"].as<string>();
    string out_dirpath = args["output_dir"].as<string>();

    // Set up the file streams.
    openFASTQFileIn(r1_path, r1_in);
    r1_reader.setQualityOffset(qual_offset);
    r1_reader.setInput(r1_in);
    openFASTQFileOut(r1_path, out_dirpath, suffix, r1_out);
    r1_writer.reset(r1_out);

    if (r2_path != "") {
        openFASTQFileIn(r2_path, r2_in);
        r2_reader.setQualityOffset(qual_offset);
        r2_reader.setInput(r2_in);
        openFASTQFileOut(r2_path, out_dirpath, suffix, r2_out);
        r2_writer.reset(r2_out);

        // Open the output files for singleton sequences.
        openFASTQFileOut(
            r1_path, out_dirpath, suffix + "-singletons", r1_singltns_out
        );
        r1_singltns_writer.reset(r1_singltns_out);
        openFASTQFileOut(
            r2_path, out_dirpath, suffix + "-singletons", r2_singltns_out
        );
        r2_singltns_writer.reset(r2_singltns_out);

        is_paired = true;
    } else {
        is_paired = false;
    }
}

void RunConfig::openFASTQFileIn(const string &fin_path_str, ifstream &fin) {
    fin.open(fin_path_str, std::ios_base::in);
    if (fin.fail()) {
        throw(RunConfigError(
            "The input FASTQ file \"" + fin_path_str +
            "\" could not be opened."
        ));
    }
}

/**
 * @brief Initializes an output file stream.
 *
 * @param fin_path_str The path of an input file from which to derive the
 *     output file name.
 * @param dirpath The path of the output directory.
 * @param suffix The suffix to use for generating output file names.
 * @param fout The output file stream.
 */
void RunConfig::openFASTQFileOut(
    const string &fin_path_str, const string &dirpath, const string &suffix,
    ofstream &fout
) {
    path ifpath(fin_path_str);
    auto ofpath = path(dirpath) / (
        ifpath.stem().string() + suffix + ifpath.extension().string()
    );

    if (!ofpath.has_filename()) {
        throw(RunConfigError(
            "The output FASTQ file path, \"" + ofpath.string() +
            "\", is invalid."
        ));
    }

    if (std::filesystem::exists(ofpath)) {
        throw(RunConfigError(
            "The output FASTQ file \"" + ofpath.string() + "\" already exists."
        ));
    }

    fout.open(ofpath);
    if (fout.fail()) {
        throw(RunConfigError(
            "The output FASTQ file \"" + ofpath.string() +
            "\" could not be opened for writing."
        ));
    }
}

