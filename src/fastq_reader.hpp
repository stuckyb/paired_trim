/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FASTQ_READER_HPP
#define FASTQ_READER_HPP

#include <istream>
#include <string>
#include <sstream>
#include <iomanip>
#include <cctype>
#include <iterator>

#include "fastq_exceptions.hpp"
#include "nuc_sequence.hpp"


class FASTQReader {
public:
    /**
     * @brief Constructs a reader with no input source.
     * 
     * Constructs a reader that does not have any input text to analyze.
     * Calling getNextSequence() will throw a FASTQEndOfFile exception.
     */
    FASTQReader(int quality_offset=33);
    
    /**
     * @brief Constructs a reader that parses a FASTQ string.
     * 
     * Constructs a reader that parses a FASTQ-format text string.  The
     * contents of the string will be copied by the reader, so the string
     * provided as an argument to the constructor does not need to persist
     * beyond the constructor call.
     * 
     * @param input A text string in FASTQ format.
     */
    explicit FASTQReader(const std::string &input, int quality_offset=33);
    
    /**
     * @brief Constructs a reader that parses an input text stream.
     * 
     * Constructs a reader that parses an input stream of FASTQ-format text.
     * 
     * @param input An input stream of FASTQ-format text.
     */
    explicit FASTQReader(std::istream &input, int quality_offset=33);
    
    /**
     * @brief Resets the reader with a new input string.
     * 
     * Sets a new FASTQ-format text string to use as the source for parsing.
     * After the call to setInput(), parsing will start at the beginning of the
     * new input string.  The contents of the input string will be copied by
     * the reader, so the string provided by the caller does not need to
     * persist beyond the call.
     * 
     * @param input A text string in FASTQ format.
     * @return void
     */
    void setInput(const std::string &input);
    
    /**
     * @brief Resets the reader with a new input stream.
     * 
     * Sets a new input stream as the source for parsing.  After the call to
     * setInput(), parsing will start at the beginning of the new input stream.
     * 
     * @param input An input stream of FASTQ-format text.
     * @return void
     */
    void setInput(std::istream &input);
    
    /**
     * @brief Get the current input line number.
     * 
     * Returns the line of the current position in the input text stream.  This
     * method and getColumnNumber() are implicitly inline.
     */
    unsigned getLineNumber() const {
        return linenum;
    }
    
    /**
     * @brief Tests if the input source has more input to process.
     *
     * Returns true if the end of the input source has not yet been detected.
     * If not input source has been provided, throws a FASTQNoInput exception.
     */
    bool hasMoreInput() const;

    /**
     * @brief Returns the current quality score encoding offset.
     */
    int getQualityOffset() {
        return qual_offset;
    }

    /**
     * @brief Sets the quality score encoding offset.
     */
    void setQualityOffset(int quality_offset) {
        qual_offset = quality_offset;
    }

    /**
     * @brief Gets the next sequence from the input source.
     * 
     * Returns the next nucleotide sequence read from the input source.  If the
     * parser is at the end of the input, a FASTQNoMoreInput exception will be
     * thrown.  If a parse error is encountered, a FASTQParseError exception
     * will be thrown.  When interpreting sequence ID lines (lines starting
     * with a '@'), getNextSequence() will attempt to parse them as if they
     * follow recent Illumina conventions, which include a read number
     * following a space character.  If parsing a sequence ID line following
     * the Illumina convention fails, then the entire contents of the line
     * (excluding the leading '@') will be treated as the sequence identifier.
     * 
     * @return The next sequence in the input source.
     */
    NucleotideSequence getNextSequence();
    
    // The copy constructor and copy assignment operator are defined as deleted
    // (which means the move constructor and move assignment operator are not
    // defined) because I'm not certain we can always preserve state during a
    // copy/move.  Specifically, if the input source is a string, I'm not sure
    // that the state of the istringstream iterator will be preserved across
    // move/copy operations.  For example, any iterators pointing to an
    // istringstream are unlikely to survive move/copy operations on the
    // underlying istringstream, so it seems best to just avoid this problem.
    FASTQReader(const FASTQReader&) = delete;
    FASTQReader &operator=(FASTQReader) = delete;

private:
    void initInputStream(std::istream &input);
    void advance();
    void syntaxError(const std::string &msg, const std::string &context="");
    int strToInt(std::string &int_str);
    void readTitle(std::string &id1, std::string &id2, int &readnum);
    std::string readBases();
    std::vector<int> readQualityScores(NucleotideSequence::size_type exp_size);

    std::istream* is = nullptr;
    std::istringstream iss;
    std::string curline;
    bool syntax_error = false;
    bool end_of_input = false;
    unsigned linenum = 0;
    int qual_offset;
};

#endif /* FASTQ_READER_HPP */
