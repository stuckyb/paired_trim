/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef NUC_SEQUENCE_HPP
#define NUC_SEQUENCE_HPP

#include <istream>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <cctype>
#include <iterator>


class NucleotideSequence {
public:
    using size_type = std::string::size_type;

    /**
     * @brief Constructs a nucleotide sequence with the given string of bases
     * and name.
     * 
     * Constructs a nucleotide sequence with the given string of bases and name
     * and with no quality score information.  The bases must be standard IUPAC
     * codes and can be provided in either upper or lower case.
     * 
     * @param bases_str A string of IUPAC base codes.
     * @param id_str1 The primary sequence identifier.
     * @param id_str2 The secondary sequence identifier.
     * @param read_num The sequence read number.
     */
    NucleotideSequence(
        const std::string &bases_str="", const std::string &id_str1="",
        const std::string &id_str2="", int read_num=-1
    );
    
    /**
     * @brief Constructs a nucleotide sequence with the given string of bases,
     * quality scores, and name.
     * 
     * Constructs a nucleotide sequence with the given string of bases, vector
     * of quality scores, and name.  The bases must be standard IUPAC codes and
     * can be provided in either upper or lower case.
     * 
     * @param bases_str A string of IUPAC base codes.
     * @param qual_scores A vector of quality scores.
     * @param id_str1 The primary sequence identifier.
     * @param id_str2 The secondary sequence identifier.
     * @param read_num The sequence read number.
     */
    NucleotideSequence(
        const std::string &bases_str, const std::vector<int> &qual_scores_vec,
        const std::string &id_str1="", const std::string &id_str2="",
        int read_num=-1
    );

    // The synthesized copy/move constructors and copy/move assignment
    // operators are sufficient because all class members define these
    // operations.

    /**
     * @brief Gets the sequence length.
     * 
     * @return The length of the nucleotide sequence.
     */
    size_type length() const {
        return _bases.size();
    }
    
    /**
     * @brief Gets a reference to the primary identifier string.
     * 
     * @return A reference to the primary identifier string.
     */
    const std::string &idString1() const {
        return _id_str1;
    }

    /**
     * @brief Gets a reference to the secondary identifier string.
     * 
     * @return A reference to the secondary identifier string.
     */
    const std::string &idString2() const {
        return _id_str2;
    }

    /**
     * @brief Gets the complete title string for the sequence.
     *
     * @return The complete title string.
     */
    std::string getTitle() const;

    /**
     * @brief Gets the read number of this sequence.
     *
     * @return The sequence read number.
     */
    int readNumber() const {
        return _read_num;
    }

    /**
     * @brief Gets a reference to the bases.
     * 
     * @return A reference to the sequence of bases.
     */
    const std::string &bases() const {
        return _bases;
    }

    /**
     * @brief Gets a reference to the quality scores.
     * 
     * @return A reference to the sequence of quality scores.
     */
    const std::vector<int> &qualityScores() const {
        return _qual_scores;
    }

    /**
     * @brief Equality test operator.
     *
     * Tests if two NucleotideSequence objects are equivalent.  The ID strings,
     * read number, bases, quality scores, and reverse complement state are all
     * included in the comparison.
     *
     * @return True if the sequences are the same.
     */
    bool operator==(const NucleotideSequence &rhs) const;

    /**
     * @brief Inequality test operator.
     *
     * Tests if two NucleotideSequence objects are not equivalent.  The ID
     * strings, read number, bases, quality scores, and reverse complement
     * state are all included in the comparison.
     *
     * @return True if the sequences are not the same.
     */
    bool operator!=(const NucleotideSequence &rhs) const;

    /**
     * @brief Tests if the sequence is reverse complemented.
     *
     * @return True if the sequence is reverse complements, false otherwise.
     */
    bool isReverseComplement() const {
        return _is_revcomp;
    }

    /**
     * @brief Converts the sequence to its reverse complement.
     *
     * Converts the sequence to its reverse complement.  Modifies the sequence
     * in place.
     */
    void reverseComplement();

    /**
     * @brief Trims bases from the start of the sequence.
     *
     * Trims the specified number of bases from the start of the sequence
     * (i.e., beginning at index 0).  If the number of bases to trim equals or
     * exceeds the length of the sequence, the result will be an empty
     * sequence.  Modifies the sequence in place.
     *
     * @param base_cnt The number of bases to trim.
     */
    void trimStart(size_type base_cnt);

    /**
     * @brief Trims bases from the end of the sequence.
     *
     * Trims the specified number of bases from the end of the sequence (i.e.,
     * beginning at index length() - 1).  If the number of bases to trim equals
     * or exceeds the length of the sequence, the result will be an empty
     * sequence.  Modifies the sequence in place.
     *
     * @param base_cnt The number of bases to trim.
     */
    void trimEnd(size_type base_cnt);

    /**
     * @brief Quality-trims the start of the sequence.
     * 
     * Trims the start of the sequence (i.e., beginning at index 0) until a
     * specified quality threshold is reached.  Uses a sliding window algorithm
     * that tests for the number of bases within the window that meet the
     * quality threshold.  If the number of bases in the sequence is less than
     * the size of the window, or if no window position in the sequence meets
     * the quality threshold, the result will be an empty sequence.  Modifies
     * the sequence in place.
     *
     * @param win_size The number of bases in the sliding window.
     * @param min_qual The minimum acceptable quality score.
     * @param min_highqual_cnt The minimum acceptable number of high-quality
     *     bases in the window.
     */
    void qualityTrimStart(
        size_type win_size, int min_qual, size_type min_highqual_cnt
    );

    /**
     * @brief Quality-trims the end of the sequence.
     *
     * Trims the end of the sequence (i.e., beginning at index length() - 1)
     * until a specified quality threshold is reached.  Uses a sliding window
     * algorithm that tests for the number of bases within the window that meet
     * the quality threshold.  If the number of bases in the sequence is less
     * than the size of the window, or if no window position in the sequence
     * meets the quality threshold, the result will be an empty sequence.
     * Modifies the sequence in place.
     *
     * @param win_size The number of bases in the sliding window.
     * @param min_qual The minimum acceptable quality score.
     * @param min_highqual_cnt The minimum acceptable number of high-quality
     *     bases in the window.
     */
    void qualityTrimEnd(
        size_type win_size, int min_qual, size_type min_highqual_cnt
    );

private:
    // Valid IUPAC nucleotide codes.
    static const std::string _valid_codes;

    // Lookup table for base complements.
    static const char _compl_lookup[];
    static const char _clookup_offset;

    // Data members.  We use std::string and std::vector for the base calls and
    // quality scores because they are memory-efficient and support very fast
    // random element access.  Although these data structures have O(N) time
    // complexity for operations that modify the start of the sequence, such
    // operations are expected to be infrequent, and because sequence lengths
    // are typically no more than a few hundred bases, the time cost should be
    // negligible.
    std::string _id_str1;
    std::string _id_str2;
    int _read_num;
    std::string _bases;
    std::vector<int> _qual_scores;
    bool _is_revcomp = false;

    void checkQualityTrimParams(
        size_type win_size, size_type min_qual, size_type min_highqual_cnt
    );
};

#endif /* NUC_SEQUENCE_HPP */
