/*
 * Copyright (C) 2019 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RUN_CONFIG_HPP
#define RUN_CONFIG_HPP

#include <string>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <filesystem>

#include "cxxopts.hpp"
#include "nuc_sequence.hpp"
#include "fastq_reader.hpp"
#include "fastq_writer.hpp"


using ns_size_t = NucleotideSequence::size_type;


/**
 * @brief Exception class to indicate configuration errors.
 * 
 * A simple exception class used to indicate a configuration error.
 */
class RunConfigError: public std::runtime_error {
public:
    explicit RunConfigError(const std::string &message):
        std::runtime_error(message)
    {}
};


struct RunConfig {
public:
    void fromCxxOpts(const cxxopts::ParseResult &args);

    // Whether paired reads are available.
    bool is_paired;

    // Input FASTQ readers.  If paired reads are not available, r2_reader
    // should not have an associated input source.
    FASTQReader r1_reader, r2_reader;

    // FASTQ writers for single or paired reads.  If paired reads are not
    // available, only r1_reader should have an associated output stream.
    FASTQWriter r1_writer, r2_writer;
    // FASTQ writers for singletons left after their matching pairs were
    // eliminated.  If paired reads are not available, these writers should not
    // have associated output streams.
    FASTQWriter r1_singltns_writer, r2_singltns_writer;

    // The offset for ASCII-encoded quality scores.
    int qual_offset;
    // The minimum quality score for sliding-window quality trimming.
    int min_qual;
    // The window size for sliding-window quality trimming.
    ns_size_t win_size;
    // The minimum number of high-quality bases for sliding-window quality
    // trimming.  If 0, quality trimming is disabled.
    ns_size_t min_highqual_cnt;

    // Explicit end trimming lengths.
    ns_size_t r1_trim_5p, r1_trim_3p;
    ns_size_t r2_trim_5p, r2_trim_3p;

    // Whether to do alignment-guided trimming.
    bool align_trim;

    // Adapter sequences.
    NucleotideSequence r1_adapter;
    NucleotideSequence r2_adapter;

    // The minimum acceptable final sequence length.
    ns_size_t min_length;

    // Whether to log sequence processing details to the console.
    bool logging;

    // Whether to log adapter alignment information for all reads.
    bool log_adapt_align;

    // Whether to do rigorous ID matching when processing paired-end reads.
    bool match_paired_ids;

private:
    void openFASTQFileIn(const std::string &fin_path_str, std::ifstream &fin);
    void openFASTQFileOut(
        const std::string &fin_path_str, const std::string &dirpath,
        const std::string &suffix, std::ofstream &fout
    );

    std::ifstream r1_in, r2_in;
    std::ofstream r1_out, r2_out;
    std::ofstream r1_singltns_out, r2_singltns_out;
};

#endif /* RUN_CONFIG_HPP */

