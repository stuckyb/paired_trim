/*
 * Copyright (C) 2019 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SEQUENCE_PROCESSOR_HPP
#define SEQUENCE_PROCESSOR_HPP

#include <string>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <filesystem>

#include "nuc_sequence.hpp"
#include "fastq_reader.hpp"
#include "run_config.hpp"
#include "paired_read_aligner.hpp"
#include "logger.hpp"


using ns_size_t = NucleotideSequence::size_type;


/**
 * @brief Exception class to indicate sequence processing errors.
 * 
 * A simple exception class used to indicate a sequence processing error.
 */
class SequenceProcessingError: public std::runtime_error {
public:
    explicit SequenceProcessingError(const std::string &message):
        std::runtime_error(message)
    {}
};


/**
 * @brief Implements high-level sequence processing.
 *
 * For now, all methods in this class are static.  They are still grouped
 * within a class, though, because I think it provides useful semantic
 * packaging.
 */
class SequenceProcessor {
public:
    SequenceProcessor(RunConfig &run_config): config(run_config) {}
    void run();

private:
    void runSingleReads();
    void runPairedReads();
    void checkPairedReadIDsMatch(
        NucleotideSequence &fwd_seq, NucleotideSequence &rev_seq
    );
    void doAdapterTrimming(
        NucleotideSequence &fwd_seq, NucleotideSequence &rev_seq,
        PairedReadAligner &aligner
    );
    void logAdapterAlignments(
        NucleotideSequence &fwd_seq, NucleotideSequence &rev_seq,
        PairedReadAligner &aligner
    );

    RunConfig &config;
    ReadProcessingLogger logger;
};

#endif /* SEQUENCE_PROCESSOR_HPP */


