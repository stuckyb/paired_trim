/*
 * Copyright (C) 2019 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * Use simulation to estimate the null-model probability distributions of
 * semi-global, pairwise sequence alignments with sequences of various lengths.
 */

#include <string>
#include <vector>
#include <iostream>
#include <stdexcept>
using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::exception;
using std::vector;

#include "cxxopts.hpp"
#include "nuc_sequence.hpp"
#include "paired_read_aligner.hpp"
#include "rand_seqs.hpp"


using ns_size_t = NucleotideSequence::size_type;
using cxxopts::OptionParseException;


cxxopts::Options getOptParser() {
    cxxopts::Options opts(
        "sim_align_scores",
        "Estimates null-model probability distributions of semi-global, "
        "pairwise sequence alignment scores."
    );

    opts.add_options()
        ("h,help", "Print usage information.")
        (
            "p,percentile", "Estimate the alignment score at the given "
            "percentile (expressed as a value from 0.0 to 1.0) for the range "
            "of sequence lengths.",
            cxxopts::value<double>()->default_value("0.999"), "PERCENTILE"
        )
        (
            "s,one_sample", "Output a full probability distribution sample "
            "for the given sequence length.",
            cxxopts::value<ns_size_t>()->default_value("0"), "SEQ_LEN"
        )
        (
            "z,sample_size", "The sample size to use.",
            cxxopts::value<unsigned>()->default_value("1000"), "SAMP_SIZE"
        )
        (
            "m,max_seqlen", "The maximum sequence length to use for "
            "generating probability tables.",
            cxxopts::value<ns_size_t>()->default_value("200"), "MAX_LEN"
        )
        (
            "g,gap_penalty", "The gap penalty to use.",
            cxxopts::value<int>()->default_value("-6"), "GAP_PENALTY"
        )
    ;

    return opts;
}


int main(int argc, char *argv[]) {
    double percentile;
    ns_size_t single_samp_seqlen, max_seqlen;
    unsigned sample_size;
    int gap_penalty;

    auto opt_p = getOptParser();

    try {
        auto args = opt_p.parse(argc, argv);

        if (args.count("help")) {
            cout << opt_p.help() << endl;
            return 0;
        }

        percentile = args["percentile"].as<double>();
        single_samp_seqlen = args["one_sample"].as<ns_size_t>();
        sample_size = args["sample_size"].as<unsigned>();
        max_seqlen = args["max_seqlen"].as<ns_size_t>();
        gap_penalty = args["gap_penalty"].as<int>();
    } catch(const OptionParseException &err) {
        cerr << "\nERROR: " << err.what() << ".\n" << endl;
        return 1;
    } catch (...) {
        cerr << "\nERROR: Unknown exception encountered when processing "
            "command-line options..\n" << endl;
        return 1;
    }

    if (percentile < 0 || percentile > 1) {
        cerr << "\nERROR: Invalid percentile value: " << percentile << ".\n"
            << endl;
        return 1;
    }
    if (max_seqlen < 1) {
        cerr << "\nERROR: Invalid maximum sequence length: " << max_seqlen <<
            ".\n" << endl;
        return 1;
    }

    PairedReadAligner aligner;
    aligner.setGapPenalty(gap_penalty);
    vector<int> samp;

    if (single_samp_seqlen > 0) {
        // Generate and print a single sample.
        samp = getAlignmentSample(sample_size, single_samp_seqlen, aligner);
        cout << "score" << endl;
        for (auto &score: samp) {
            cout << score << endl;
        }
    } else {
        // Generate a table of percentile cutoff values.
        cout << "seq_len,cutoff" << endl;

        ns_size_t cur_seqlen = 1;
        int pcntl_val;
        while (cur_seqlen <= max_seqlen) {
            samp = getAlignmentSample(sample_size, cur_seqlen, aligner);
            pcntl_val = getPercentileScore(samp, percentile);

            cout << cur_seqlen << "," << pcntl_val << endl;

            if (cur_seqlen < 100) {
                ++cur_seqlen;
            } else {
                cur_seqlen += 10;
            }
        }
    }

    return 0;
}

