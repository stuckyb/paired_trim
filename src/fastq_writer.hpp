/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FASTQ_WRITER_HPP
#define FASTQ_WRITER_HPP

#include <istream>
#include <string>
#include <sstream>
#include <iomanip>
#include <cctype>
#include <iterator>

#include "fastq_exceptions.hpp"
#include "nuc_sequence.hpp"


/**
 * Writes FASTQ files from NucleotideSequence objects.  FASTQWriter objects
 * hold a pointer to an output stream instance, but they do not manage the
 * output stream.  Thus, it is important for client code to ensure that the
 * output stream pointer is not invalidated while the FASTQWriter is in use.
 */
class FASTQWriter {
public:
    /**
     * @brief Construct a new FASTQWriter.
     *
     * Construct a new FASTQWriter that is not associated with an output
     * stream.
     *
     * @param os An output stream.
     * @param quality_offset The quality score encoding offset.
     */
    FASTQWriter(int quality_offset=33);

    /**
     * @brief Construct a new FASTQWriter.
     *
     * @param os An output stream.
     * @param quality_offset The quality score encoding offset.
     */
    explicit FASTQWriter(std::ostream &os, int quality_offset=33);

    /**
     * @brief Resets the writer with a different output stream.
     */
    void reset(std::ostream &os);

    /**
     * @brief Writes a sequence to the given output stream.
     *
     * Writes a sequence to the given output stream in FASTQ format.
     */
    void write(const NucleotideSequence &seq);

private:
    std::ostream* _os;
    int _qual_offset;
    int _seq_cnt = 0;
    std::string quals_str;
};

#endif /* FASTQ_WRITER_HPP */
