#!/usr/bin/python3

# Copyright (C) 2019 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# This script generates C++ code that implements efficient C++ lookup tables
# for sequence manipulations.  These tables are built upon standard C++ arrays
# and use nucleotide characters as array indices (minus some offset).

# Reverse complement lookup.
rclookup = {
    'A': 'T', 
    'T': 'A', 
    'G': 'C', 
    'C': 'G',
    'W': 'W', 
    'S': 'S', 
    'M': 'K', 
    'K': 'M', 
    'R': 'Y', 
    'Y': 'R',
    'B': 'V', 
    'D': 'H', 
    'H': 'D', 
    'V': 'B', 
    'N': 'N'
}

bases = sorted(rclookup.keys())
arr_vals = []

for intval in range(ord(bases[0]), ord(bases[-1]) + 1):
    charval = chr(intval)
    if charval in rclookup:
        arr_vals.append(rclookup[charval])
    else:
        arr_vals.append('0')

arr_literal = "{'" + "', '".join(arr_vals) + "'}"
arr_literal = arr_literal.replace("'0'", "0")
print(arr_literal)


# Alignment scoring matrix.
sm_vals = {
    'A': {
        'A':  6, 'T': -6, 'G': -6, 'C': -6, 'W':  0, 'S': -6, 'M':  0, 'K': -6,
        'R':  0, 'Y': -6, 'B': -6, 'D': -2, 'H': -2, 'V': -2, 'N': -3
    },
    'T': {
        'A': -6, 'T':  6, 'G': -6, 'C': -6, 'W':  0, 'S': -6, 'M': -6, 'K':  0,
        'R': -6, 'Y':  0, 'B': -2, 'D': -2, 'H': -2, 'V': -6, 'N': -3
    },
    'G': {
        'A': -6, 'T': -6, 'G':  6, 'C': -6, 'W': -6, 'S':  0, 'M': -6, 'K':  0,
        'R':  0, 'Y': -6, 'B': -2, 'D': -2, 'H': -6, 'V': -2, 'N': -3
    },
    'C': {
        'A': -6, 'T': -6, 'G': -6, 'C':  6, 'W': -6, 'S':  0, 'M':  0, 'K': -6,
        'R': -6, 'Y':  0, 'B': -2, 'D': -6, 'H': -2, 'V': -2, 'N': -3
    },
    'W': {
        'A':  0, 'T':  0, 'G': -6, 'C': -6, 'W':  0, 'S': -6, 'M': -3, 'K': -3,
        'R': -3, 'Y': -3, 'B': -4, 'D': -2, 'H': -2, 'V': -4, 'N': -3
    },
    'S': {
        'A': -6, 'T': -6, 'G':  0, 'C':  0, 'W': -6, 'S':  0, 'M': -3, 'K': -3,
        'R': -3, 'Y': -3, 'B': -2, 'D': -4, 'H': -4, 'V': -2, 'N': -3
    },
    'M': {
        'A':  0, 'T': -6, 'G': -6, 'C':  0, 'W': -3, 'S': -3, 'M':  0, 'K': -6,
        'R': -3, 'Y': -3, 'B': -4, 'D': -4, 'H': -2, 'V': -2, 'N': -3
    },
    'K': {
        'A': -6, 'T':  0, 'G':  0, 'C': -6, 'W': -3, 'S': -3, 'M': -6, 'K':  0,
        'R': -3, 'Y': -3, 'B': -2, 'D': -2, 'H': -4, 'V': -4, 'N': -3
    },
    'R': {
        'A':  0, 'T': -6, 'G':  0, 'C': -6, 'W': -3, 'S': -3, 'M': -3, 'K': -3,
        'R':  0, 'Y': -6, 'B': -4, 'D': -2, 'H': -4, 'V': -2, 'N': -3
    },
    'Y': {
        'A': -6, 'T':  0, 'G': -6, 'C':  0, 'W': -3, 'S': -3, 'M': -3, 'K': -3,
        'R': -6, 'Y':  0, 'B': -2, 'D': -4, 'H': -2, 'V': -4, 'N': -3
    },
    'B': {
        'A': -6, 'T': -2, 'G': -2, 'C': -2, 'W': -4, 'S': -2, 'M': -4, 'K': -2,
        'R': -4, 'Y': -2, 'B': -2, 'D': -3, 'H': -3, 'V': -3, 'N': -3
    },
    'D': {
        'A': -2, 'T': -2, 'G': -2, 'C': -6, 'W': -2, 'S': -4, 'M': -4, 'K': -2,
        'R': -2, 'Y': -4, 'B': -3, 'D': -2, 'H': -3, 'V': -3, 'N': -3
    },
    'H': {
        'A': -2, 'T': -2, 'G': -6, 'C': -2, 'W': -2, 'S': -4, 'M': -2, 'K': -4,
        'R': -4, 'Y': -2, 'B': -3, 'D': -3, 'H': -2, 'V': -3, 'N': -3
    },
    'V': {
        'A': -2, 'T': -6, 'G': -2, 'C': -2, 'W': -4, 'S': -2, 'M': -2, 'K': -4,
        'R': -2, 'Y': -4, 'B': -3, 'D': -3, 'H': -3, 'V': -2, 'N': -3
    },
    'N': {
        'A': -3, 'T': -3, 'G': -3, 'C': -3, 'W': -3, 'S': -3, 'M': -3, 'K': -3,
        'R': -3, 'Y': -3, 'B': -3, 'D': -3, 'H': -3, 'V': -3, 'N': -3
    }
}

offset = ord(bases[0])
mat_dim = ord(bases[-1]) - offset + 1

print('int score_mat[{0}][{0}] = {{'.format(mat_dim))

# Generate column labels.
helpstr = '//   '
for intval1 in range(offset, ord(bases[-1]) + 1):
    base1 = chr(intval1)
    if base1 in sm_vals:
        helpstr += '  ' + base1
    else:
        helpstr += '   '
print(helpstr)

# Fill rows that don't correspond to an actual nucleotide code with 0s.
fill_row = '     {' + ','.join([' 0'] * mat_dim) + '},'

for intval1 in range(offset, ord(bases[-1]) + 1):
    base1 = chr(intval1)

    if base1 not in sm_vals:
        print(fill_row)
    else:
        scores = []
        for intval2 in range(offset, ord(bases[-1]) + 1):
            base2 = chr(intval2)
            if base2 in sm_vals[base1]:
                score_str = str(sm_vals[base1][base2])
                if len(score_str) == 1:
                    score_str = ' ' + score_str
                scores.append(score_str)
            else:
                scores.append(' 0')

        print('/*{0}*/{{'.format(base1) + ','.join(scores) + '},')

print('};')

