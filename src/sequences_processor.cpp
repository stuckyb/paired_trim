/*
 * Copyright (C) 2019 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sequences_processor.hpp"
using std::cout;
using std::endl;
using std::ostringstream;

using ns_size_t = NucleotideSequence::size_type;


void SequenceProcessor::run() {
    if (config.is_paired) {
        runPairedReads();
    } else {
        runSingleReads();
    }
}

void SequenceProcessor::runSingleReads() {
    NucleotideSequence seq;

    while (config.r1_reader.hasMoreInput()) {
        seq = config.r1_reader.getNextSequence();
        if (config.logging) { logger.startUnpairedReadEntry(seq); }

        
        // Explicit end trimming.
        if (config.r1_trim_5p > 0) {
            seq.trimStart(config.r1_trim_5p);
        }
        if (config.r1_trim_3p > 0) {
            seq.trimEnd(config.r1_trim_3p);
        }

        // Quality trimming.
        if (config.min_highqual_cnt > 0) {
            seq.qualityTrimEnd(
                config.win_size, config.min_qual, config.min_highqual_cnt
            );
        }

        // Length filtering.
        if (seq.length() >= config.min_length) {
            config.r1_writer.write(seq);
        }

        if (config.logging) { logger.endUnpairedReadEntry(seq); }
    }
}

void SequenceProcessor::runPairedReads() {
    NucleotideSequence fwd_seq, rev_seq;
    PairedReadAligner aligner;

    while (config.r1_reader.hasMoreInput()) {
        fwd_seq = config.r1_reader.getNextSequence();

        try {
            rev_seq = config.r2_reader.getNextSequence();
        } catch (FASTQNoMoreInput &err) {
            throw(SequenceProcessingError(
                "There were more forward sequencing reads than there were "
                "reverse sequencing reads."
            ));
        }

        if (config.logging) {
            logger.startPairedReadEntry(fwd_seq, rev_seq);
        }

        if (config.match_paired_ids) {
            // Verify that the paired sequence IDs actually match.  If they
            // don't, this will throw a SequenceProcessingError, which we
            // simply let pass up to the caller.
            checkPairedReadIDsMatch(fwd_seq, rev_seq);
        }
        
        // Explicit end trimming.
        if (config.r1_trim_5p > 0) {
            fwd_seq.trimStart(config.r1_trim_5p);
        }
        if (config.r1_trim_3p > 0) {
            fwd_seq.trimEnd(config.r1_trim_3p);
        }
        if (config.r2_trim_5p > 0) {
            rev_seq.trimStart(config.r2_trim_5p);
        }
        if (config.r2_trim_3p > 0) {
            rev_seq.trimEnd(config.r2_trim_3p);
        }

        // Quality trimming.
        if (config.min_highqual_cnt > 0) {
            fwd_seq.qualityTrimEnd(
                config.win_size, config.min_qual, config.min_highqual_cnt
            );
            rev_seq.qualityTrimEnd(
                config.win_size, config.min_qual, config.min_highqual_cnt
            );
        }

        // Alignment-guided trimming.
        if (config.align_trim) {
            if (config.logging && config.log_adapt_align) {
                logAdapterAlignments(fwd_seq, rev_seq, aligner);
            }

            rev_seq.reverseComplement();
            aligner.align(fwd_seq, rev_seq);
            //aligner.printAlignment(cout);
            if (config.logging) {
                logger.addPairAlignment(aligner, fwd_seq, rev_seq);
            }
            rev_seq.reverseComplement();

            if (aligner.testSignif()) {
                /*if (aligner.getForwardOverhangCnt() > 0 || aligner.getReverseOverhangCnt() > 0) {
                    cout << "TRIMMING FROM ALIGNMENT!!" << endl;
                    cout << fwd_seq.getTitle() << endl << rev_seq.getTitle() << endl;
                    cout << fwd_seq.bases() << endl;
                    cout << rev_seq.bases() << endl;
                    aligner.printAlignment(cout);
                    cout << "***** END READ ALIGNMENT *****" << endl;
                }*/
                fwd_seq.trimEnd(aligner.getForwardOverhangCnt());
                rev_seq.trimEnd(aligner.getReverseOverhangCnt());
            } else if (
                config.r1_adapter.length() > 0 &&
                config.r2_adapter.length() > 0
            ) {
                // Only do explicit adapter trimming if the alignment-guided
                // trimming failed.
                doAdapterTrimming(fwd_seq, rev_seq, aligner);
            }
        }

        // Length filtering.
        if (
            fwd_seq.length() >= config.min_length &&
            rev_seq.length() >= config.min_length
        ) {
            config.r1_writer.write(fwd_seq);
            config.r2_writer.write(rev_seq);
        } else {
            if (fwd_seq.length() >= config.min_length) {
                config.r1_singltns_writer.write(fwd_seq);
            }
            if (rev_seq.length() >= config.min_length) {
                config.r2_singltns_writer.write(rev_seq);
            }
        }

        if (config.logging) {
            logger.endPairedReadEntry(fwd_seq, rev_seq);
        }
    }

    // Also check for the case where there is an excess of reverse reads.
    if (config.r2_reader.hasMoreInput()) {
        throw(SequenceProcessingError(
            "There were more reverse sequencing reads than there were forward "
            "sequencing reads."
        ));
    }
}

void SequenceProcessor::checkPairedReadIDsMatch(
    NucleotideSequence &fwd_seq, NucleotideSequence &rev_seq
) {
    if (!(
        fwd_seq.idString1() == rev_seq.idString1() &&
        fwd_seq.idString2() == rev_seq.idString2()
    )) {
        std::ostringstream os;
        os << "The sequencing reads immediately before line " <<
            config.r1_reader.getLineNumber() <<
            " in the forward reads and line " <<
            config.r2_reader.getLineNumber() <<
            " in the reverse reads do not appear to be a matched pair.";
        throw(SequenceProcessingError(os.str()));
    }
}

void SequenceProcessor::doAdapterTrimming(
    NucleotideSequence &fwd_seq, NucleotideSequence &rev_seq,
    PairedReadAligner &aligner
) {
    if (fwd_seq.length() >= config.r1_adapter.length()) {
        aligner.align(fwd_seq, config.r1_adapter);
        if (config.logging && !config.log_adapt_align) {
            logger.addFwdAdapterAlignment(aligner);
        }
        //cout << "TRIMMING FROM ADAPTER...\n***** ADAPTER ALIGNMENT *****" << endl;
        //aligner.printAlignment(cout);
        //cout << "***** END ADAPTER ALIGNMENT *****" << endl;

        if (
            static_cast<double>(aligner.getAlignmentScore()) /
            config.r1_adapter.length() > 3.15
        ) {
            /*if (aligner.getOverlapStart() == 0) {
                aligner.printAlignment(cout);
            }*/
            fwd_seq.trimEnd(fwd_seq.length() - aligner.getOverlapStart());
        }
    }

    if (rev_seq.length() >= config.r2_adapter.length()) {
        aligner.align(rev_seq, config.r2_adapter);
        if (config.logging && !config.log_adapt_align) {
            logger.addRevAdapterAlignment(aligner);
        }

        if (
            static_cast<double>(aligner.getAlignmentScore()) /
            config.r2_adapter.length() > 3.15
        ) {
            rev_seq.trimEnd(rev_seq.length() - aligner.getOverlapStart());
        }
    }
}

/*
 * Logs adapter alignments for a read pair without actually doing anything with
 * the adapter alignments.
 */
void SequenceProcessor::logAdapterAlignments(
    NucleotideSequence &fwd_seq, NucleotideSequence &rev_seq,
    PairedReadAligner &aligner
) {
    if (!config.logging) {
        return;
    }

    if (
        config.r1_adapter.length() > 0 &&
        fwd_seq.length() >= config.r1_adapter.length()
    ) {
        aligner.align(fwd_seq, config.r1_adapter);
        logger.addFwdAdapterAlignment(aligner);
    }

    if (
        config.r2_adapter.length() > 0 &&
        rev_seq.length() >= config.r2_adapter.length()
    ) {
        aligner.align(rev_seq, config.r2_adapter);
        logger.addRevAdapterAlignment(aligner);
    }
}

