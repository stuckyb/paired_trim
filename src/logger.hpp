/*
 * Copyright (C) 2019 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <string>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <filesystem>

#include "nuc_sequence.hpp"
#include "fastq_reader.hpp"
#include "run_config.hpp"
#include "paired_read_aligner.hpp"


using ns_size_t = NucleotideSequence::size_type;


/**
 * Provides detailed logging of sequence processing.  All logging output is
 * written to stdout in tab-separated values (TSV) format.
 */
class ReadProcessingLogger {
public:
    /*
     * Starts a new unpaired read log entry.  The TSV header will be printed if
     * it has not already been printed.
     */
    void startUnpairedReadEntry(const NucleotideSequence &seq);

    /*
     * Signals the end of an unpaired read log entry and prints the entry to
     * stdout.
     */
    void endUnpairedReadEntry(const NucleotideSequence &seq);

    /*
     * Starts a new paired read log entry.  The TSV header will be printed if
     * it has not already been printed.
     */
    void startPairedReadEntry(
        const NucleotideSequence &fwd_seq, const NucleotideSequence &rev_seq
    );

    /*
     * @brief Adds paired read alignment information to a log entry.
     */
    void addPairAlignment(
        const PairedReadAligner &aligner, const NucleotideSequence &fwd_seq,
        const NucleotideSequence & rev_seq
    );

    /*
     * @brief Adds forward adapter alignment information to a log entry.
     */
    void addFwdAdapterAlignment(const PairedReadAligner &aligner);

    /*
     * @brief Adds reverse adapter alignment information to a log entry.
     */
    void addRevAdapterAlignment(const PairedReadAligner &aligner);

    /*
     * Signals the end of a paired read log entry and prints the entry to
     * stdout.
     */
    void endPairedReadEntry(
        const NucleotideSequence &fwd_seq, const NucleotideSequence &rev_seq
    );

    /*
     * @brief Resets the state of the logger so that a TSV header will be
     * printed before the next log entry.
     */
    void reset();

private:
    void initVars();

    // Whether the header has been output.
    bool header_printed = false;

    // Number of empty cells in the current row (only used for paired read
    // entries).
    unsigned fwd_empty_cells, rev_empty_cells;

    // Starting sequence lengths.
    ns_size_t fwd_start_len, rev_start_len;

    // Used for collecting processing details (only used for paired read
    // entries).  Separate accumulators are used for adapter alignment details
    // so that adapter alignment information and read alignment information can
    // be logged independently of each other.
    std::ostringstream fwd_output_accum, rev_output_accum;
    std::ostringstream fwd_adapt_output_accum, rev_adapt_output_accum;
};

#endif /* LOGGER_HPP */


