/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <sstream>

#include "fastq_exceptions.hpp"


FASTQParseError::FASTQParseError(
    const std::string &message, unsigned linenum, const std::string &context
): msg(), line(linenum), context(context)
{
    std::ostringstream os;
    
    os << "FASTQ parse error at line " << linenum;
    if (context != "") {
        os << ", near \"" << context << "\"";
    }
    os << ": " << message;
    
    msg = os.str();
}

const char *FASTQParseError::what() const noexcept {
    return msg.c_str();
}

