/*
 * Copyright (C) 2019 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <string>
#include <stdexcept>
#include <math.h>
using std::ostream;
using std::endl;
using std::string;

#include "fastq_writer.hpp"


FASTQWriter::FASTQWriter(int quality_offset):
    _os(nullptr), _qual_offset(quality_offset)
{}

FASTQWriter::FASTQWriter(ostream &os, int quality_offset):
    _os(&os), _qual_offset(quality_offset)
{}

void FASTQWriter::reset(ostream &os) {
    _os = &os;
    _seq_cnt = 0;
}

void FASTQWriter::write(const NucleotideSequence &seq) {
    if (_os == nullptr) {
        throw FASTQNoOutput("No output source was provided.");
    }

    if (_seq_cnt > 0) {
        *_os << endl;
    }

    // Write the title line.  We don't use NucleotideSequence's getTitle()
    // method for efficiency reasons: by building the title directly as output
    // we avoid setting up an ostringstream on the stack and the memory
    // manipulation associated with string move operations.
    if (seq.readNumber() != -1) {
        *_os << '@' << seq.idString1() << ' ' << seq.readNumber() << ':';
        *_os  << seq.idString2() << endl;
    } else {
        *_os << '@' << seq.idString1() << endl;
    }

    // Write the bases and the '+' separater line.
    *_os << seq.bases() << endl << '+' << endl;

    // Generate and write the quality scores line.
    quals_str.clear();

    for (auto &qualval : seq.qualityScores()) {
        quals_str.push_back(qualval + _qual_offset);
    }
    *_os << quals_str;

    ++_seq_cnt;
}

