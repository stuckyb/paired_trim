/*
 * Copyright (C) 2019 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cstddef>

#include "rand_seqs.hpp"

using std::vector;
using std::string;
using std::size_t;
using std::cout;
using std::endl;

const string valid_codes = "ATGC";


NucleotideSequence getRandomSequence(ns_size_t length) {
    static std::default_random_engine re(
        std::chrono::system_clock::now().time_since_epoch().count()
    );
    static std::uniform_int_distribution<unsigned> ui_dist(
        0, valid_codes.length() - 1
    );

    string basevals = "";
    basevals.reserve(length);
    for (decltype(basevals.size()) i = 0; i < length; ++i) {
        basevals.push_back(valid_codes[ui_dist(re)]);
    }

    return NucleotideSequence(basevals);
}

vector<int> getAlignmentSample(
    unsigned samp_size, ns_size_t seq_len, PairedReadAligner &aligner
) {
    vector<int> samp;
    samp.reserve(samp_size);

    NucleotideSequence seq1, seq2;

    for (unsigned i = 0; i < samp_size; ++i) {
        seq1 = getRandomSequence(seq_len);
        seq2 = getRandomSequence(seq_len);
        samp.push_back(aligner.align(seq1, seq2));
        //aligner.printAlignment(cout);
    }

    return samp;
}

int getPercentileScore(std::vector<int> &sample, double percentile) {
    std::sort(sample.begin(), sample.end());

    size_t i = size_t(ceil(sample.size() * percentile)) - 1;

    return sample[i];
}

