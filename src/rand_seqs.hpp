/*
 * Copyright (C) 2019 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * Defines functions for generating and analyzing random sequences and
 * alignments.
 */

#ifndef RAND_SEQS_HPP
#define RAND_SEQS_HPP

#include <vector>

#include "nuc_sequence.hpp"
#include "paired_read_aligner.hpp"


using ns_size_t = NucleotideSequence::size_type;

/*
 * Generates a random nucleotide sequence of the specified length.  The
 * sequence will include the codes 'A', 'T', 'G', and 'C', each of which will
 * have the same probability of occuring at any position.
 */
NucleotideSequence getRandomSequence(ns_size_t length);

/*
 * Generates a random sample of semi-global pairwise alignment scores of
 * sequences of the specified length, using the provided aligner.
 */
std::vector<int> getAlignmentSample(
    unsigned samp_size, ns_size_t seq_len, PairedReadAligner &aligner
);

/*
 * Given a random sample of alignment scores, estimates the score at a
 * particular percentile using the nearest-rank method.  Note that this
 * function will sort the sample in place.
 *
 * @param sample A random sample of alignment scores.
 * @param percentile The percentile at which to estimate the score.
 */
int getPercentileScore(std::vector<int> &sample, double percentile);

#endif /* RAND_SEQS_HPP */

