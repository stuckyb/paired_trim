/*
 * Copyright (C) 2019 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "logger.hpp"
using std::cout;
using std::endl;


void ReadProcessingLogger::startUnpairedReadEntry(
    const NucleotideSequence &seq
) {
    if (!header_printed) {
        cout << "sequence\tlength_in\tlength_out" << endl;
        header_printed = true;
    }

    fwd_start_len = seq.length();
}

void ReadProcessingLogger::endUnpairedReadEntry(
    const NucleotideSequence &seq
) {
    cout << seq.getTitle() << '\t' << fwd_start_len << '\t' << seq.length()
        << endl;
}

void ReadProcessingLogger::startPairedReadEntry(
    const NucleotideSequence &fwd_seq, const NucleotideSequence &rev_seq
) {
    if (!header_printed) {
        cout << "sequence\tdir\tlength_in\tlength_out\t"
            "align_len\talign_score\talign_ident\talign_signif\t"
            //"alt_align_possible\talign_trimmable\talign_trim_mismatch\t"
            "16_0\t16_1\t16_2\t18_0\t18_1\t18_2\t20_0\t20_1\t20_2\t"
            "22_0\t22_1\t22_2\t24_0\t24_1\t24_2\t"
            "align_trimmable\talign_trim_mismatch\t"
            "adapt_align_score\tadapt_align_len\tadapt_align_ident" << endl;
        header_printed = true;
    }

    fwd_start_len = fwd_seq.length();
    rev_start_len = rev_seq.length();
    fwd_empty_cells = 13;
    rev_empty_cells = 13;
    fwd_output_accum.str("");
    rev_output_accum.str("");
    fwd_adapt_output_accum.str("");
    rev_adapt_output_accum.str("");
}

void ReadProcessingLogger::addPairAlignment(
    const PairedReadAligner &aligner, const NucleotideSequence &fwd_seq,
    const NucleotideSequence &rev_seq
) {
    std::ostringstream output_accum;

    bool align_trim_mismatch = (
        aligner.getForwardOverhangCnt() != aligner.getReverseOverhangCnt()
    );

    output_accum << '\t' << aligner.getOverlapLength()
        << '\t' << aligner.getAlignmentScore()
        << '\t' << aligner.getAverageIdentity()
        << '\t' << (aligner.testSignif() ? 'Y' : 'N');

    for (int i = 16; i <= 24; i += 2) {
        for (int j = 0; j <= 2; ++j) {
            output_accum << '\t' << (aligner.checkAltAlignment(fwd_seq, rev_seq, i, j) ? 'Y' : 'N');
        }
    }

    fwd_output_accum << output_accum.str()
        << '\t' << (aligner.getForwardOverhangCnt() > 0 ? 'Y' : 'N')
        << '\t' << (align_trim_mismatch ? 'Y' : 'N');
    rev_output_accum << output_accum.str()
        << '\t' << (aligner.getReverseOverhangCnt() > 0 ? 'Y' : 'N')
        << '\t' << (align_trim_mismatch ? 'Y' : 'N');

    fwd_empty_cells -= 10;
    rev_empty_cells -= 10;
}

void ReadProcessingLogger::addFwdAdapterAlignment(
    const PairedReadAligner &aligner
) {
    fwd_adapt_output_accum << '\t' << aligner.getAlignmentScore()
        << '\t' << aligner.getOverlapLength()
        << '\t' << aligner.getAverageIdentity();

    fwd_empty_cells -= 3;
}

void ReadProcessingLogger::addRevAdapterAlignment(
    const PairedReadAligner &aligner
) {
    rev_adapt_output_accum << '\t' << aligner.getAlignmentScore()
        << '\t' << aligner.getOverlapLength()
        << '\t' << aligner.getAverageIdentity();

    rev_empty_cells -= 3;
}

void ReadProcessingLogger::endPairedReadEntry(
    const NucleotideSequence &fwd_seq, const NucleotideSequence &rev_seq
) {
    cout << fwd_seq.getTitle() << "\tF\t" << fwd_start_len << '\t'
        << fwd_seq.length() << fwd_output_accum.str()
        << fwd_adapt_output_accum.str();
    for (unsigned i = 0; i < fwd_empty_cells; ++i) {
        cout << '\t';
    }
    cout << endl;

    cout << rev_seq.getTitle() << "\tR\t" << rev_start_len << '\t'
        << rev_seq.length() << rev_output_accum.str()
        << rev_adapt_output_accum.str();
    for (unsigned i = 0; i < rev_empty_cells; ++i) {
        cout << '\t';
    }
    cout << endl;
}

