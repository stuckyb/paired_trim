/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "fastq_reader.hpp"
#include <iostream>
#include <stdexcept>

using std::string;
using std::vector;
using std::getline;
using std::stoi;
using std::istream;
using std::istringstream;
using std::ostringstream;
using std::invalid_argument;
using std::out_of_range;


/******
 * Members of FASTQReader.
 ******/

FASTQReader::FASTQReader(int quality_offset): qual_offset(quality_offset) {}

FASTQReader::FASTQReader(const string &input, int quality_offset):
    qual_offset(quality_offset)
{
    setInput(input);
}

FASTQReader::FASTQReader(istream &input, int quality_offset):
    qual_offset(quality_offset)
{
    setInput(input);
}

void FASTQReader::setInput(const string &input) {
    // Note that accepting a reference to the source string is acceptable, even
    // for temporary arguments (i.e., rvalues), because the istringstream
    // constructor copies the source string.
    iss.str(input);
    iss.clear();
    initInputStream(iss);
}

void FASTQReader::setInput(istream &input) {
    iss.str("");
    initInputStream(input);
}

bool FASTQReader::hasMoreInput() const {
    if (is == nullptr) {
        throw FASTQNoInput("No input source was provided.");
    } else {
        return !(end_of_input);
    }
}

/**
 * @brief Prepares the parser to read an input source.
 * 
 * Given an input stream, initializes the stream pointer and the line counter.
 * 
 * @param input The input stream.
 * @return void
 */
void FASTQReader::initInputStream(istream &input) {
    is = &input;
    syntax_error = false;
    end_of_input = false;
    linenum = 0;

    // Check for the case where we are given an empty input source.  If the
    // input is empty, peek() will detect EOF.
    is->peek();

    advance();
}

/**
 * @brief Reads the next line from the input source.
 */
void FASTQReader::advance() {
    if (!(is->eof())) {
        getline(*is, curline);

        // Handle Windows/DOS-style line endings.
        if (curline.size() > 0 && curline[curline.size() - 1] == '\r') {
            curline.pop_back();
        }

        ++linenum;
    } else if (!end_of_input) {
        // This code will only run after an attempt to advance beyond EOF.
        // Resetting curline is necessary because if the input stream has
        // reached EOF, the last value of curline will remain after subsequent
        // calls to getline().
        curline = "";
        end_of_input = true;
    }
}

/**
 * Throws a FASTQParseError with the provided message and context information
 * and sets the syntax_error flag.
 */
void FASTQReader::syntaxError(
    const std::string &msg, const std::string &context
){
    // Set a flag to indicate that a syntax error was encountered.
    syntax_error = true;
    throw FASTQParseError(msg, linenum, context);
}

/**
 * Converts string representations of integers to int values, and reports a
 * syntax error if the conversion fails.
 */
int FASTQReader::strToInt(string &int_str) {
    int intval;
    size_t pos;

    try {
        intval = stoi(int_str, &pos);
    } catch (invalid_argument err) {
        syntaxError(
            "The read number, \"" + int_str +
            "\", could not be converted to an integer value."
        );
    } catch (out_of_range err) {
        syntaxError(
            "The read number, \"" + int_str +
            "\", could not be converted to an integer value."
        );
    }
    if (pos != int_str.size()) {
        syntaxError(
            "The read number, \"" + int_str +
            "\", could not be converted to an integer value."
        );
    }

    return intval;
}

/**
 * Parse the sequence title line.  This method will attempt to parse the line
 * using the conventions of recent Illumina software; if that fails, everything
 * after the '@' character will be returned in id1, id2 will be an empty
 * string, and readnum will be -1.  For details of the Illumina title string
 * format, see
 * https://help.basespace.illumina.com/articles/descriptive/fastq-files/.
 */
void FASTQReader::readTitle(string &id1, string &id2, int &readnum) {
    if (curline.size() < 1) {
        syntaxError("Expected a sequence title but found an empty line.");
    } else if (curline[0] != '@') {
        syntaxError(
            "Missing \"@\" at the beginning of a sequence title line."
        );
    }

    string::size_type split_pos;

    split_pos = curline.find(' ');
    if (split_pos != string::npos) {
        id1 = curline.substr(1, split_pos - 1);
        id2 = curline.substr(split_pos + 1);

        // Try to extract the read number.
        split_pos = id2.find(':');
        if (split_pos != string::npos) {
            string int_str = id2.substr(0, split_pos);
            readnum = strToInt(int_str);
            id2 = id2.substr(split_pos + 1);
        } else {
            id1 += ' ' + id2;
            id2 = "";
            readnum = -1;
        }
    } else {
        id1 = curline.substr(1, curline.size() - 1);
        id2 = "";
        readnum = -1;
    }

    advance();
}

string FASTQReader::readBases() {
    string bases_str = "";

    while (
        !(curline.size() > 0 && curline[0] == '+') &&
        !end_of_input
    ) {
        bases_str += curline;
        advance();
    }

    // At the end of the while() loop, either curline will start with a '+' or
    // we are out of input.  Either way, we can attempt to advance the stream
    // reader, which will eat the end-of-sequence line, if it exists, and then
    // we can test for end_of_input.
    advance();

    if (end_of_input) {
        syntaxError("Incomplete FASTQ sequence data definition.");
    }

    return bases_str;
}

vector<int> FASTQReader::readQualityScores(
    NucleotideSequence::size_type exp_size
) {
    vector<int> qual_scores;
    qual_scores.reserve(exp_size);

    while (qual_scores.size() < exp_size && !(end_of_input)) {
        for (char &qual_char : curline) {
            qual_scores.push_back(qual_char - qual_offset);
        }
        advance();
    }

    // Check the number of quality scores that were read.  In the case of an
    // empty sequence (and empty quality scores string), the while() loop body
    // will never execute, so we instead check that curline is "".
    if (
        qual_scores.size() != exp_size ||
        (exp_size == 0 && curline != "")
    ) {
        syntaxError(
            "The number of quality scores does not match the number of base "
            "calls."
        );
    }

    // In the case of an empty sequence (and empty quality scores string),
    // curline will still be the quality scores line, so we need to explicitly
    // advance the stream reader.  Also, if there are empty lines at the end of
    // the quality score section, they will need to be explicitly consumed.
    //std::cout << "EXP_SIZE:" << exp_size << std::endl;
    while (curline == "" && !(end_of_input)) {
        //std::cout << "ADVANCING EMPTY!!" << std::endl;
        advance();
    }

    return qual_scores;
}

NucleotideSequence FASTQReader::getNextSequence() {
    if (is == nullptr) {
        throw FASTQNoInput("No input source was provided.");
    } else if (end_of_input) {
        throw FASTQNoMoreInput();
    } else if (!(*is)) {
        throw FASTQParseError("Error state detected in input stream.");
    } else if (syntax_error) {
        throw FASTQParseError(
            "Parsing cannot continue because a syntax error was previously "
            "encountered."
        );
    }

    string id1, id2;
    int readnum;
    readTitle(id1, id2, readnum);
    //std::cout << id1 << " " << id2 << " " << readnum << std::endl;

    string bases = readBases();
    //std::cout << bases << std::endl;

    vector<int> qual_scores = readQualityScores(bases.size());
    //std::cout << qual_scores.size() << std::endl;

    return NucleotideSequence(bases, qual_scores, id1, id2, readnum);
}

