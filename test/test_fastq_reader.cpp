/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <sstream>
#include <fstream>
#include <stdexcept>
using std::string;
using std::istringstream;
using std::ifstream;
using std::runtime_error;

#include "gtest/gtest.h"
#include "fastq_reader.hpp"


/*
 * Define the test fixture class.
 */
class FASTQReaderTest: public testing::Test {
protected:
    FASTQReader reader;
};

/*
 * Define the tests.
 */

/*
 * Test that an "empty" reader behaves as expected.
 */
TEST_F(FASTQReaderTest, emptyInputs) {
    ASSERT_EQ(0, reader.getLineNumber());
    ASSERT_THROW(reader.getNextSequence(), FASTQNoInput);
    ASSERT_THROW(reader.hasMoreInput(), FASTQNoInput);
    
    reader.setInput("");
    ASSERT_EQ(0, reader.getLineNumber());
    ASSERT_EQ(false, reader.hasMoreInput());
    ASSERT_THROW(reader.getNextSequence(), FASTQNoMoreInput);
    
    string input = "";
    istringstream iss(input);
    reader.setInput(iss);
    ASSERT_EQ(0, reader.getLineNumber());
    ASSERT_EQ(false, reader.hasMoreInput());
    ASSERT_THROW(reader.getNextSequence(), FASTQNoMoreInput);
}

/*
 * Test sequence parsing.
 */
TEST_F(FASTQReaderTest, getNextSequence) {
    NucleotideSequence seq, exp;

    // Simplest legal, non-empty FASTQ file: a single block with an empty title
    // and an empty sequence.
    reader.setInput(R"(@

+
)");
    exp = NucleotideSequence("", {}, "", "", -1);
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_FALSE(reader.hasMoreInput());

    // Multiple blocks with empty titles and empty sequences.
    reader.setInput(R"(@

+

@

+
)");
    exp = NucleotideSequence("", {}, "", "", -1);
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_TRUE(reader.hasMoreInput());
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_FALSE(reader.hasMoreInput());

    // Multiple blocks with empty titles and empty sequences, including
    // multi-line sequences and quality score strings.
    reader.setInput(R"(@


+



@



+

)");
    exp = NucleotideSequence("", {}, "", "", -1);
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_TRUE(reader.hasMoreInput());
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_FALSE(reader.hasMoreInput());

    // A single block with a 1-base sequence.
    reader.setInput(R"(@seq1
A
+
?)");
    exp = NucleotideSequence("A", {30}, "seq1", "", -1);
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_FALSE(reader.hasMoreInput());

    // Multiple blocks with 1-base sequences, including multi-line sequences
    // and quality score strings.
    reader.setInput(R"(@seq1

A

+

?

@seq2
T

+

=)");
    exp = NucleotideSequence("A", {30}, "seq1", "", -1);
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_TRUE(reader.hasMoreInput());
    exp = NucleotideSequence("T", {28}, "seq2", "", -1);
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_FALSE(reader.hasMoreInput());

    // A single block with a multi-base sequence.
    reader.setInput(R"(@seq1
GCAT
+
=>?@)");
    exp = NucleotideSequence("GCAT", {28,29,30,31}, "seq1", "", -1);
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_FALSE(reader.hasMoreInput());

    // Multiple blocks with multi-base sequences, including multi-line
    // sequences and quality score strings.  Also test a quality score line
    // that begins with a '@'.
    reader.setInput(R"(@seq1

GC

AT

+

=>
?

@

@seq2
SYK

+
12
3
)");
    exp = NucleotideSequence("GCAT", {28,29,30,31}, "seq1", "", -1);
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_TRUE(reader.hasMoreInput());
    exp = NucleotideSequence("SYK", {16,17,18}, "seq2", "", -1);
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_FALSE(reader.hasMoreInput());

    // Multiple blocks with multi-base sequences and Illumina-format sequence
    // titles.  Tests another quality score line that begins with a '@'.
    reader.setInput(R"(@SIM:1:FCX:1:15:6329:1045 1:N:0:2
GCATSYK
+
@=>?123
@SIM:1:FCX:1:15:6329:1045 2:N:0:2
VMY
+
456)");
    exp = NucleotideSequence(
        "GCATSYK", {31,28,29,30,16,17,18}, "SIM:1:FCX:1:15:6329:1045",
        "N:0:2", 1
    );
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_TRUE(reader.hasMoreInput());
    exp = NucleotideSequence(
        "VMY", {19,20,21}, "SIM:1:FCX:1:15:6329:1045",
        "N:0:2", 2
    );
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_FALSE(reader.hasMoreInput());

    // A single block with a multi-base sequence and Windows-style line
    // endings (i.e., lines that end with "\r\n").
    reader.setInput("@seq1\r\nGCAT\r\n+\r\n=>?@");
    exp = NucleotideSequence("GCAT", {28,29,30,31}, "seq1", "", -1);
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_FALSE(reader.hasMoreInput());
}

/*
 * Test that the reader works with an input stream (ifstream, in this case).
 */
TEST_F(FASTQReaderTest, readsInputStream) {
    NucleotideSequence seq, exp;

    ifstream fin(PAIREDTRIM_TEST_SOURCE_DIR "/data/parser_test.fastq");
    ASSERT_TRUE(fin);
    
    reader.setInput(fin);
    
    exp = NucleotideSequence(
        "GCATSYK", {31,28,29,30,16,17,18}, "SIM:1:FCX:1:15:6329:1045",
        "N:0:2", 1
    );
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_TRUE(reader.hasMoreInput());
    exp = NucleotideSequence(
        "VMY", {19,20,21}, "SIM:1:FCX:1:15:6329:1045",
        "N:0:2", 2
    );
    seq = reader.getNextSequence();
    ASSERT_TRUE(exp == seq);
    ASSERT_FALSE(reader.hasMoreInput());
}

/*
 * Test various error conditions.
 */
TEST_F(FASTQReaderTest, errors) {
    // Missing title marker.
    reader.setInput(R"(seq1
GCAT
+
=>?@)");
    ASSERT_THROW(reader.getNextSequence(), FASTQParseError);

    // Missing '+' line.
    reader.setInput(R"(@seq1
GCAT
=>?@)");
    ASSERT_THROW(reader.getNextSequence(), FASTQParseError);

    // Mismatched base and quality score count.
    reader.setInput(R"(@seq1
GCAT
+
=?@)");
    ASSERT_THROW(reader.getNextSequence(), FASTQParseError);

    // Invalid nucleotide codes.
    reader.setInput(R"(@seq1
fCAT
+
=>?@)");
    ASSERT_THROW(reader.getNextSequence(), runtime_error);

}

