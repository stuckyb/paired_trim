/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <vector>
#include <stdexcept>
#include <iostream>
using std::string;
using std::vector;
using std::runtime_error;
using std::cout;
using std::endl;

#include "gtest/gtest.h"
#include "paired_read_aligner.hpp"
#include "nuc_sequence.hpp"


using ns_size_t = NucleotideSequence::size_type;

/*
 * Define the test fixture class.
 */
class PairedReadAlignerTest: public testing::Test {
};

/*
 * Define the tests.
 */


/*
 * Runs consistency checks on the alignment scoring matrix.
 *
 * The average probability of an exact match across all columns and rows of the
 * matrix is the same (0.25), so all rows and columns should have the same sum,
 * (0.25 * 12 - 6) * 15 = -45.  Checking the row/column sums will detect
 * virtually all accidental mistakes, such as swapping two numbers or using the
 * wrong value somewhere.  It is of course possible to construct wrong matrices
 * that this test will not catch, but it is extremely unlikely that these cases
 * would arise by accident!
 */
TEST_F(PairedReadAlignerTest, scoreMatrix) {
    auto score_mat = PairedReadAligner::getScoreMatrix();
    auto offset = PairedReadAligner::getScoreMatrixOffset();

    // All valid nucleotide codes.
    string all_codes = "ATGCBDHKMRSVWYN";

    // The row/column sums for these nucleotide codes should be -45.
    string check_codes = "ATGCWSMKRYN";
    int sum;
    for (auto &check_code : check_codes) {
        // Check the row sum.
        sum = 0;
        for (auto &code : all_codes) {
            sum += score_mat[check_code - offset][code - offset];
        }
        ASSERT_EQ(-45, sum);

        // Check the column sum.
        sum = 0;
        for (auto &code : all_codes) {
            sum += score_mat[code - offset][check_code - offset];
        }
        ASSERT_EQ(-45, sum);
    }

    // Because of rounding, the row/column sums for these nucleotide codes will
    // be -44 instead of -45.
    check_codes = "BDHV";
    for (auto &check_code : check_codes) {
        // Check the row sum.
        sum = 0;
        for (auto &code : all_codes) {
            sum += score_mat[check_code - offset][code - offset];
        }
        ASSERT_EQ(-44, sum);

        // Check the column sum.
        sum = 0;
        for (auto &code : all_codes) {
            sum += score_mat[code - offset][check_code - offset];
        }
        ASSERT_EQ(-44, sum);
    }
}


/*
 * A struct for defining alignment test data.
 */
struct AlignTestData {
    // The sequence data.
    NucleotideSequence fwd_seq, rev_seq;
    // The expected alignment.
    string al_fwd_seq, al_rev_seq;
    // The gap penalty to use.
    int gap_penalty;
    // Expected alignment score.
    int score;
    // The overlap length and overlap location.
    ns_size_t overlap_len, overlap_start, overlap_end;
    // The expected average pairwise identity.
    double avg_identity;
    // The 3' overhang lengths.
    ns_size_t fwd_overhang_cnt, rev_overhang_cnt;
    // Whether to also test the swapped the forward and reverse sequences.
    bool test_swap = true;
};


/*
 * Tests the semi-global, pairwise alignment implementation and related
 * calculations (mean pairwise identity, overlap length, overhang lengths).
 */
TEST_F(PairedReadAlignerTest, align) {
    PairedReadAligner aligner;

    // Define the test data.  The sequence orders are such that when the
    // sequences are aligned in the struct order, the forward and reverse
    // overhang values will be correct; when the sequence order is reversed,
    // the overhang values should both be 0.
    vector<AlignTestData> tdata_vec = {
        // Alignments with empty sequences.
        {
            string(""),
            string(""),
            "",
            "",
            -6,
            0,
            0, 0, 0,
            0.0,
            0, 0
        },
        {
            string("A"),
            string(""),
            "A",
            "-",
            -6,
            0,
            0, 0, 0,
            0.0,
            0, 0
        },
        {
            string("GCAT"),
            string(""),
            "GCAT",
            "----",
            -6,
            0,
            0, 0, 0,
            0.0,
            0, 0
        },
        // Two single-base sequences.
        {
            string("A"),
            string("A"),
            "A",
            "A",
            -6,
            6,
            1, 0, 1,
            1.0,
            0, 0
        },
        // Two 2-base sequences that do not align at all.
        {
            string("GC"),
            string("AT"),
            "GC--",
            "--AT",
            -6,
            0,
            0, 0, 0,
            0.0,
            0, 0,
            // Don't test the forward/reverse swap, because the alignment would
            // be different.
            false
        },
        // Two 4-base sequences, partial end match.
        {
            string("GAGC"),
            string("TAGA"),
            "--GAGC",
            "TAGA--",
            -6,
            12,
            2, 2, 4,
            1.0,
            2, 2
        },
        // Sequences of different lengths that align perfectly.
        {
            string("CATGCATTTATTATAAGGTT"),
            string("CATGCATTTATTATAAGG"),
            "CATGCATTTATTATAAGGTT",
            "CATGCATTTATTATAAGG--",
            -6,
            108,
            18, 0, 18,
            1.0,
            2, 0
        },
        {
            string("TGCATTTATTATAAGGTT"),
            string("CATGCATTTATTATAAGGTT"),
            "--TGCATTTATTATAAGGTT",
            "CATGCATTTATTATAAGGTT",
            -6,
            108,
            18, 2, 20,
            1.0,
            0, 2
        },
        // Sequences of different lengths with 2 mismatched bases and 6 fully
        // ambiguous bases.
        {
            string("CNTGCANCCATTATNAGGTT"),
            string("CNTGCANTTATTATANGG"),
            "CNTGCANCCATTATNAGGTT",
            "CNTGCANTTATTATANGG--",
            -6,
            48,
            18, 0, 18,
            (13.0 / 18),
            2, 0
        },
        {
            string("CNTGCANCCATTATNAGGTT"),
            string("CNTGCANTTATTATAGNT"),
            "CNTGCANCCATTATNAGGTT",
            "CNTGCANTTATTAT-AGNT-",
            -6,
            51,
            19, 0, 19,
            (13.75 / 19),
            1, 0
        },
        // Sequences of different lengths with 2 mismatched bases.
        {
            string("CATGCATCCATTATAAGGTT"),
            string("CATGCATTTATTATAAGG"),
            "CATGCATCCATTATAAGGTT",
            "CATGCATTTATTATAAGG--",
            -6,
            84,
            18, 0, 18,
            (16.0 / 18),
            2, 0
        },
        // Sequences of the same length with 2 mismatched bases and 2 missing
        // bases in the middle.
        {
            string("CATATCCATTATAAGGTT"),
            string("CATGCATTTATTATAAGG"),
            "CAT--ATCCATTATAAGGTT",
            "CATGCATTTATTATAAGG--",
            -6,
            60,
            18, 0, 18,
            (14.0 / 18),
            2, 0
        },
        // Sequences that align with two end gaps, one internal gap, and one
        // mismatch.
        {
            string("TGTCCATCATAAGGCA"),
            string("CATATCCATATAAG"),
            "--TGTCCATCATAAGGCA",
            "CATATCCAT-ATAAG---",
            -6,
            54,
            13, 2, 15,
            (11.0 / 13),
            3, 2
        },
        // Sequences with partially ambiguous bases.  Tests several
        // combinations of 1-2- and 3-base codes to verify that the bases are
        // placed correctly.
        {
            string("CAWGCAWTTBTATAAGGTT"),
            string("CATGSAMWTTATTATAMGT"),
            "CAWGCA-WTT-BTATAAGGTT",
            "CATGSAMWTTATTATAMG-T-",
            -6,
            52,
            20, 0, 20,
            ((12.0 + .5 + .5 + .5 + (1.0/3) + .5) / 20),
            1, 0
        },

        // Test some alignments with a different gap penalty.

        // Sequences of different lengths that align perfectly.
        {
            string("CATGCATTTATTATAAGGTT"),
            string("CATGCATTTATTATAAGG"),
            "CATGCATTTATTATAAGGTT",
            "CATGCATTTATTATAAGG--",
            -18,
            108,
            18, 0, 18,
            1.0,
            2, 0
        },
        // Sequences of different lengths with 2 mismatched bases and 6 fully
        // ambiguous bases.
        {
            string("CNTGCANCCATTATNAGGTT"),
            string("CNTGCANTTATTATAGNT"),
            "CNTGCANCCATTATNAGGTT",
            "CNTGCANTTATTAT-AGNT-",
            -18,
            39,
            19, 0, 19,
            (13.75 / 19),
            1, 0
        },
        // Sequences of different lengths with 2 mismatched bases.
        {
            string("CATGCATCCATTATAAGGTT"),
            string("CATGCATTTATTATAAGG"),
            "CATGCATCCATTATAAGGTT",
            "CATGCATTTATTATAAGG--",
            -18,
            84,
            18, 0, 18,
            (16.0 / 18),
            2, 0
        },
        // Sequences of the same length with 2 mismatched bases and 2 missing
        // bases in the middle.
        {
            string("CATATCCATTATAAGGTT"),
            string("CATGCATTTATTATAAGG"),
            "CAT--ATCCATTATAAGGTT",
            "CATGCATTTATTATAAGG--",
            -18,
            36,
            18, 0, 18,
            (14.0 / 18),
            2, 0
        },
        // Sequences that align with two end gaps, one internal gap, and one
        // mismatch.
        {
            string("TGTCCATCATAAGGCA"),
            string("CATATCCATATAAG"),
            "--TGTCCATCATAAGGCA",
            "CATATCCAT-ATAAG---",
            -18,
            42,
            13, 2, 15,
            (11.0 / 13),
            3, 2
        }
    };

    int score;
    for (auto &tdata : tdata_vec) {
        aligner.setGapPenalty(tdata.gap_penalty);

        score = aligner.align(tdata.fwd_seq, tdata.rev_seq);
        //aligner.printAlignment(cout);
        ASSERT_EQ(tdata.al_fwd_seq, aligner.getAlignedFwdSeq());
        ASSERT_EQ(tdata.al_rev_seq, aligner.getAlignedRevSeq());
        ASSERT_EQ(tdata.score, score);
        ASSERT_EQ(tdata.overlap_len, aligner.getOverlapLength());
        ASSERT_EQ(tdata.overlap_start, aligner.getOverlapStart());
        ASSERT_EQ(tdata.overlap_end, aligner.getOverlapEnd());
        ASSERT_EQ(tdata.avg_identity, aligner.getAverageIdentity());
        ASSERT_EQ(tdata.fwd_overhang_cnt, aligner.getForwardOverhangCnt());
        ASSERT_EQ(tdata.rev_overhang_cnt, aligner.getReverseOverhangCnt());

        // Swap the sequence order and test again.
        if (tdata.test_swap) {
            score = aligner.align(tdata.rev_seq, tdata.fwd_seq);
            //aligner.printAlignment(cout);
            ASSERT_EQ(tdata.al_rev_seq, aligner.getAlignedFwdSeq());
            ASSERT_EQ(tdata.al_fwd_seq, aligner.getAlignedRevSeq());
            ASSERT_EQ(tdata.score, score);
            ASSERT_EQ(tdata.overlap_len, aligner.getOverlapLength());
            ASSERT_EQ(tdata.avg_identity, aligner.getAverageIdentity());
            ASSERT_EQ(0, aligner.getForwardOverhangCnt());
            ASSERT_EQ(0, aligner.getReverseOverhangCnt());
        }
    }
}

TEST_F(PairedReadAlignerTest, testSignif) {
    NucleotideSequence seq1, seq2;
    PairedReadAligner aligner;

    seq1 = NucleotideSequence("");
    seq2 = NucleotideSequence("");
    aligner.align(seq1, seq2);
    ASSERT_FALSE(aligner.testSignif());

    seq1 = NucleotideSequence("A");
    seq2 = NucleotideSequence("");
    aligner.align(seq1, seq2);
    ASSERT_FALSE(aligner.testSignif());

    seq1 = NucleotideSequence("A");
    seq2 = NucleotideSequence("A");
    aligner.align(seq1, seq2);
    ASSERT_FALSE(aligner.testSignif());

    seq1 = NucleotideSequence("A");
    seq2 = NucleotideSequence("T");
    aligner.align(seq1, seq2);
    ASSERT_FALSE(aligner.testSignif());

    seq1 = NucleotideSequence("AA");
    seq2 = NucleotideSequence("AA");
    aligner.align(seq1, seq2);
    ASSERT_FALSE(aligner.testSignif());

    seq1 = NucleotideSequence("AAGCA");
    seq2 = NucleotideSequence("AAGCA");
    aligner.align(seq1, seq2);
    ASSERT_FALSE(aligner.testSignif());

    seq1 = NucleotideSequence("AAGCATG");
    seq2 = NucleotideSequence("AAGCATG");
    aligner.align(seq1, seq2);
    ASSERT_TRUE(aligner.testSignif());

    // Verify that we're using the longer of the two sequences for the test.
    // If not, the second test would also evaluate to false.
    seq1 = NucleotideSequence("AAGCATGCAT");
    seq2 = NucleotideSequence("AAGCATGCAT");
    aligner.align(seq1, seq2);
    ASSERT_TRUE(aligner.testSignif());

    seq1 = NucleotideSequence("AAGCATGCAT");
    seq2 = NucleotideSequence("AAGCAT");
    aligner.align(seq1, seq2);
    ASSERT_FALSE(aligner.testSignif());

}

TEST_F(PairedReadAlignerTest, checkAltAlignment) {
    NucleotideSequence seq1, seq2;
    PairedReadAligner aligner;

    seq1 = NucleotideSequence("");
    seq2 = NucleotideSequence("");
    aligner.align(seq1, seq2);
    ASSERT_FALSE(aligner.checkAltAlignment(seq1, seq2));

    seq1 = NucleotideSequence("A");
    seq2 = NucleotideSequence("");
    aligner.align(seq1, seq2);
    ASSERT_FALSE(aligner.checkAltAlignment(seq1, seq2));

    seq1 = NucleotideSequence("A");
    seq2 = NucleotideSequence("A");
    aligner.align(seq1, seq2);
    ASSERT_FALSE(aligner.checkAltAlignment(seq1, seq2));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 1));

    seq1 = NucleotideSequence("A");
    seq2 = NucleotideSequence("T");
    aligner.align(seq1, seq2);
    ASSERT_FALSE(aligner.checkAltAlignment(seq1, seq2));
    ASSERT_FALSE(aligner.checkAltAlignment(seq1, seq2, 1));

    seq1 = NucleotideSequence("ATGCGCATACT");
    seq2 = NucleotideSequence("ATGCACT");
    aligner.align(seq1, seq2);
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 1));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 2));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 3));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 4));
    ASSERT_FALSE(aligner.checkAltAlignment(seq1, seq2, 5));

    seq2 = NucleotideSequence("CGCATGACT");
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 1));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 2));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 3));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 4));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 5));
    ASSERT_FALSE(aligner.checkAltAlignment(seq1, seq2, 6));

    seq2 = NucleotideSequence("TACTGCAT");
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 1));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 2));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 3));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 4));
    ASSERT_FALSE(aligner.checkAltAlignment(seq1, seq2, 5));

    seq1 = NucleotideSequence("A");
    seq2 = NucleotideSequence("T");
    aligner.align(seq1, seq2);
    ASSERT_FALSE(aligner.checkAltAlignment(seq1, seq2));
    ASSERT_FALSE(aligner.checkAltAlignment(seq1, seq2, 1));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 1, 1));

    seq1 = NucleotideSequence("ACGCGCATACT");
    seq2 = NucleotideSequence("ATGCACT");
    aligner.align(seq1, seq2);
    ASSERT_FALSE(aligner.checkAltAlignment(seq1, seq2, 4, 0));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 4, 1));
    ASSERT_FALSE(aligner.checkAltAlignment(seq1, seq2, 5, 0));
    ASSERT_FALSE(aligner.checkAltAlignment(seq1, seq2, 5, 1));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 5, 2));

    seq2 = NucleotideSequence("TTCT");
    aligner.align(seq1, seq2);
    ASSERT_FALSE(aligner.checkAltAlignment(seq1, seq2, 4, 0));
    ASSERT_TRUE(aligner.checkAltAlignment(seq1, seq2, 4, 1));
}


