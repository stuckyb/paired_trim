/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <vector>
#include <stdexcept>
#include <iostream>
using std::string;
using std::vector;
using std::runtime_error;
using std::cout;

#include "gtest/gtest.h"

#include "rand_seqs.hpp"


/*
 * Define the test fixture class.
 */
class RandSeqsTest: public testing::Test {
};

/*
 * Define the tests.
 */
TEST_F(RandSeqsTest, getRandomSequence) {
    auto seq1 = getRandomSequence(200);
    auto seq2 = getRandomSequence(200);

    ASSERT_EQ(200, seq1.length());
    ASSERT_EQ(200, seq2.length());

    ASSERT_NE(string::npos, seq1.bases().find('A'));
    ASSERT_NE(string::npos, seq1.bases().find('T'));
    ASSERT_NE(string::npos, seq1.bases().find('G'));
    ASSERT_NE(string::npos, seq1.bases().find('C'));

    ASSERT_NE(seq1.bases(), seq2.bases());
}

TEST_F(RandSeqsTest, getPercentileScore) {
    vector<int> samp = {2,4,1,3};

    ASSERT_EQ(4, getPercentileScore(samp, 1.0));
    ASSERT_EQ(4, getPercentileScore(samp, 0.99));
    ASSERT_EQ(4, getPercentileScore(samp, 0.9));
    ASSERT_EQ(3, getPercentileScore(samp, 0.75));
    ASSERT_EQ(2, getPercentileScore(samp, 0.5));
    ASSERT_EQ(1, getPercentileScore(samp, 0.25));
    ASSERT_EQ(1, getPercentileScore(samp, 0.1));
}

