/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <vector>
#include <stdexcept>
#include <iostream>
using std::string;
using std::vector;
using std::runtime_error;
using std::cout;

#include "gtest/gtest.h"

#include "nuc_sequence.hpp"

using QualVec = vector<int>;


/*
 * Define the test fixture class.
 */
class NucleotideSequenceTest: public testing::Test {
};

/*
 * Define the tests.
 */

/*
 * Tests constructor behavior.
 */
TEST_F(NucleotideSequenceTest, init) {
    NucleotideSequence seq("");

    ASSERT_EQ("", seq.idString1());
    ASSERT_EQ("", seq.idString2());
    ASSERT_EQ(-1, seq.readNumber());
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());
    ASSERT_EQ(false, seq.isReverseComplement());

    seq = NucleotideSequence("ATGC", "id1", "id2", 2);
    ASSERT_EQ("id1", seq.idString1());
    ASSERT_EQ("id2", seq.idString2());
    ASSERT_EQ(2, seq.readNumber());
    ASSERT_EQ("ATGC", seq.bases());
    ASSERT_EQ(QualVec({-1,-1,-1,-1}), seq.qualityScores());
    ASSERT_EQ(4, seq.length());
    ASSERT_EQ(false, seq.isReverseComplement());

    seq = NucleotideSequence("ATGC", {1,2,3,4}, "id1", "id2");
    ASSERT_EQ("id1", seq.idString1());
    ASSERT_EQ("id2", seq.idString2());
    ASSERT_EQ(-1, seq.readNumber());
    ASSERT_EQ("ATGC", seq.bases());
    ASSERT_EQ(QualVec({1,2,3,4}), seq.qualityScores());
    ASSERT_EQ(4, seq.length());
    ASSERT_EQ(false, seq.isReverseComplement());

    seq = NucleotideSequence("ATGC", {1,2,3,4}, "id1", "id2", 2);
    ASSERT_EQ("id1", seq.idString1());
    ASSERT_EQ("id2", seq.idString2());
    ASSERT_EQ(2, seq.readNumber());
    ASSERT_EQ("ATGC", seq.bases());
    ASSERT_EQ(QualVec({1,2,3,4}), seq.qualityScores());
    ASSERT_EQ(4, seq.length());
    ASSERT_EQ(false, seq.isReverseComplement());

    seq = NucleotideSequence("atgc", {1,2,3,4}, "id1", "id2");
    ASSERT_EQ("ATGC", seq.bases());

    // Mismatched numbers of bases and quality scores.
    ASSERT_THROW(
        NucleotideSequence("ATGC", {1,2,3}, "id1", "id2"), runtime_error
    );

    // Invalid nucleotide code.
    ASSERT_THROW(NucleotideSequence("ATEC"), runtime_error);
}

TEST_F(NucleotideSequenceTest, getTitle) {
    NucleotideSequence seq("ATGC", {1,2,3,4}, "id1", "", -1);
    ASSERT_EQ("id1", seq.getTitle());

    seq = NucleotideSequence("ATGC", {1,2,3,4}, "id1", "_str", -1);
    ASSERT_EQ("id1_str", seq.getTitle());

    seq = NucleotideSequence("ATGC", {1,2,3,4}, "id1", "_str", 1);
    ASSERT_EQ("id1 1:_str", seq.getTitle());
}

TEST_F(NucleotideSequenceTest, equality) {
    NucleotideSequence seq1("ATGC", {1,2,3,4}, "id1", "id2", 2);
    NucleotideSequence seq2("ATGC", {1,2,3,4}, "id1", "id2", 2);
    ASSERT_TRUE(seq1 == seq2);

    seq2 = NucleotideSequence("ATKC", {1,2,3,4}, "id1", "id2", 2);
    ASSERT_TRUE(seq1 != seq2);

    seq2 = NucleotideSequence("ATGC", {1,2,6,4}, "id1", "id2", 2);
    ASSERT_TRUE(seq1 != seq2);

    seq2 = NucleotideSequence("ATGC", {1,2,3,4}, "id1", "notid2", 2);
    ASSERT_TRUE(seq1 != seq2);

    seq2 = NucleotideSequence("ATGC", {1,2,3,4}, "id1", "id2", 1);
    ASSERT_TRUE(seq1 != seq2);
}

TEST_F(NucleotideSequenceTest, copy) {
    NucleotideSequence seq1("");
    ASSERT_EQ("", seq1.idString1());
    ASSERT_EQ("", seq1.idString2());
    ASSERT_EQ(-1, seq1.readNumber());
    ASSERT_EQ("", seq1.bases());
    ASSERT_EQ(QualVec(), seq1.qualityScores());
    ASSERT_EQ(0, seq1.length());
    ASSERT_EQ(false, seq1.isReverseComplement());

    NucleotideSequence seq2("ATGC", {1,2,3,4}, "seq2id1", "seq2id2", 2);
    ASSERT_EQ("seq2id1", seq2.idString1());
    ASSERT_EQ("seq2id2", seq2.idString2());
    ASSERT_EQ(2, seq2.readNumber());
    ASSERT_EQ("ATGC", seq2.bases());
    ASSERT_EQ(QualVec({1,2,3,4}), seq2.qualityScores());
    ASSERT_EQ(4, seq2.length());
    ASSERT_EQ(false, seq2.isReverseComplement());

    seq1 = seq2;
    seq2 = NucleotideSequence("CAT", {5,6,7}, "newseq2id1", "newseq2id2", 3);

    ASSERT_EQ("newseq2id1", seq2.idString1());
    ASSERT_EQ("newseq2id2", seq2.idString2());
    ASSERT_EQ(3, seq2.readNumber());
    ASSERT_EQ("CAT", seq2.bases());
    ASSERT_EQ(QualVec({5,6,7}), seq2.qualityScores());
    ASSERT_EQ(3, seq2.length());
    ASSERT_EQ(false, seq2.isReverseComplement());

    ASSERT_EQ("seq2id1", seq1.idString1());
    ASSERT_EQ("seq2id2", seq1.idString2());
    ASSERT_EQ(2, seq1.readNumber());
    ASSERT_EQ("ATGC", seq1.bases());
    ASSERT_EQ(QualVec({1,2,3,4}), seq1.qualityScores());
    ASSERT_EQ(4, seq1.length());
    ASSERT_EQ(false, seq1.isReverseComplement());
}

TEST_F(NucleotideSequenceTest, reverseComplement) {
    NucleotideSequence seq("");
    seq.reverseComplement();
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());
    ASSERT_EQ(true, seq.isReverseComplement());

    seq = NucleotideSequence("A", {1}, "id1", "id2");
    seq.reverseComplement();
    ASSERT_EQ("T", seq.bases());
    ASSERT_EQ(QualVec({1}), seq.qualityScores());
    ASSERT_EQ(1, seq.length());
    ASSERT_EQ(true, seq.isReverseComplement());

    seq = NucleotideSequence("AG", {1,2}, "id1", "id2");
    seq.reverseComplement();
    ASSERT_EQ("CT", seq.bases());
    ASSERT_EQ(QualVec({2,1}), seq.qualityScores());
    ASSERT_EQ(2, seq.length());
    ASSERT_EQ(true, seq.isReverseComplement());

    seq = NucleotideSequence("ATG", {1,2,3}, "id1", "id2");
    seq.reverseComplement();
    ASSERT_EQ("CAT", seq.bases());
    ASSERT_EQ(QualVec({3,2,1}), seq.qualityScores());
    ASSERT_EQ(3, seq.length());
    ASSERT_EQ(true, seq.isReverseComplement());

    seq.reverseComplement();
    ASSERT_EQ("ATG", seq.bases());
    ASSERT_EQ(QualVec({1,2,3}), seq.qualityScores());
    ASSERT_EQ(3, seq.length());
    ASSERT_EQ(false, seq.isReverseComplement());

    // Test all possible nucleotide codes.
    seq = NucleotideSequence(
        "ATGCWSMKRYBDHVN", {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15}, "id1", "id2"
    );
    seq.reverseComplement();
    ASSERT_EQ("NBDHVRYMKSWGCAT", seq.bases());
    ASSERT_EQ(
        QualVec({15,14,13,12,11,10,9,8,7,6,5,4,3,2,1}), seq.qualityScores()
    );
    ASSERT_EQ(15, seq.length());
    ASSERT_EQ(true, seq.isReverseComplement());
    //cout << seq.bases() << std::endl;
}

TEST_F(NucleotideSequenceTest, trimStart) {
    NucleotideSequence seq("");
    seq.trimStart(2);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = NucleotideSequence("A", {1}, "id1", "id2");
    seq.trimStart(0);
    ASSERT_EQ("A", seq.bases());
    ASSERT_EQ(QualVec({1}), seq.qualityScores());
    ASSERT_EQ(1, seq.length());
    seq.trimStart(1);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = NucleotideSequence("A", {1}, "id1", "id2");
    seq.trimStart(2);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = NucleotideSequence("AT", {1,2}, "id1", "id2");
    seq.trimStart(1);
    ASSERT_EQ("T", seq.bases());
    ASSERT_EQ(QualVec({2}), seq.qualityScores());
    ASSERT_EQ(1, seq.length());
    seq.trimStart(1);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = NucleotideSequence("ATGCSYN", {1,2,3,4,5,6,7}, "id1", "id2");
    seq.trimStart(1);
    ASSERT_EQ("TGCSYN", seq.bases());
    ASSERT_EQ(QualVec({2,3,4,5,6,7}), seq.qualityScores());
    ASSERT_EQ(6, seq.length());
    seq.trimStart(3);
    ASSERT_EQ("SYN", seq.bases());
    ASSERT_EQ(QualVec({5,6,7}), seq.qualityScores());
    ASSERT_EQ(3, seq.length());
    seq.trimStart(3);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());
}

TEST_F(NucleotideSequenceTest, trimEnd) {
    NucleotideSequence seq("");
    seq.trimEnd(2);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = NucleotideSequence("A", {1}, "id1", "id2");
    seq.trimEnd(0);
    ASSERT_EQ("A", seq.bases());
    ASSERT_EQ(QualVec({1}), seq.qualityScores());
    ASSERT_EQ(1, seq.length());
    seq.trimEnd(1);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = NucleotideSequence("A", {1}, "id1", "id2");
    seq.trimEnd(2);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = NucleotideSequence("AT", {1,2}, "id1", "id2");
    seq.trimEnd(1);
    ASSERT_EQ("A", seq.bases());
    ASSERT_EQ(QualVec({1}), seq.qualityScores());
    ASSERT_EQ(1, seq.length());
    seq.trimEnd(1);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = NucleotideSequence("ATGCSYN", {1,2,3,4,5,6,7}, "id1", "id2");
    seq.trimEnd(1);
    ASSERT_EQ("ATGCSY", seq.bases());
    ASSERT_EQ(QualVec({1,2,3,4,5,6}), seq.qualityScores());
    ASSERT_EQ(6, seq.length());
    seq.trimEnd(3);
    ASSERT_EQ("ATG", seq.bases());
    ASSERT_EQ(QualVec({1,2,3}), seq.qualityScores());
    ASSERT_EQ(3, seq.length());
    seq.trimEnd(3);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());
}

TEST_F(NucleotideSequenceTest, qualityTrimStart) {
    // Empty sequence.
    NucleotideSequence seq("");
    seq.qualityTrimStart(1, 28, 1);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    ASSERT_THROW(seq.qualityTrimStart(1, 28, 2), runtime_error);
    ASSERT_THROW(seq.qualityTrimStart(0, 28, 0), runtime_error);

    // Sequences of length 1.
    seq = NucleotideSequence("A", {30}, "id1", "id2");
    seq.qualityTrimStart(1, 28, 1);
    ASSERT_EQ("A", seq.bases());
    ASSERT_EQ(QualVec({30}), seq.qualityScores());
    ASSERT_EQ(1, seq.length());

    seq.qualityTrimStart(1, 30, 1);
    ASSERT_EQ("A", seq.bases());
    ASSERT_EQ(QualVec({30}), seq.qualityScores());
    ASSERT_EQ(1, seq.length());

    seq.qualityTrimStart(1, 31, 0);
    ASSERT_EQ("A", seq.bases());
    ASSERT_EQ(QualVec({30}), seq.qualityScores());
    ASSERT_EQ(1, seq.length());

    seq.qualityTrimStart(1, 31, 1);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = NucleotideSequence("A", {30}, "id1", "id2");
    seq.qualityTrimStart(2, 28, 1);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    // Multi-nucleotide sequences.
    NucleotideSequence mnseq(
        "ATGCWSMKRYBDHV", {10,30,10,30,30,30,10,10,30,30,30,30,30,30},
        "id1", "id2"
    );

    seq = mnseq;
    seq.qualityTrimStart(1, 10, 1);
    ASSERT_EQ("ATGCWSMKRYBDHV", seq.bases());
    ASSERT_EQ(
        QualVec({10,30,10,30,30,30,10,10,30,30,30,30,30,30}), seq.qualityScores()
    );
    ASSERT_EQ(14, seq.length());

    seq.qualityTrimStart(1, 28, 1);
    ASSERT_EQ("TGCWSMKRYBDHV", seq.bases());
    ASSERT_EQ(
        QualVec({30,10,30,30,30,10,10,30,30,30,30,30,30}), seq.qualityScores()
    );
    ASSERT_EQ(13, seq.length());

    seq.qualityTrimStart(1, 31, 1);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = mnseq;
    seq.qualityTrimStart(4, 10, 2);
    ASSERT_EQ("ATGCWSMKRYBDHV", seq.bases());
    ASSERT_EQ(
        QualVec({10,30,10,30,30,30,10,10,30,30,30,30,30,30}), seq.qualityScores()
    );
    ASSERT_EQ(14, seq.length());

    seq.qualityTrimStart(4, 28, 2);
    ASSERT_EQ("ATGCWSMKRYBDHV", seq.bases());
    ASSERT_EQ(
        QualVec({10,30,10,30,30,30,10,10,30,30,30,30,30,30}), seq.qualityScores()
    );
    ASSERT_EQ(14, seq.length());

    seq.qualityTrimStart(4, 28, 3);
    ASSERT_EQ("TGCWSMKRYBDHV", seq.bases());
    ASSERT_EQ(
        QualVec({30,10,30,30,30,10,10,30,30,30,30,30,30}), seq.qualityScores()
    );
    ASSERT_EQ(13, seq.length());

    seq.qualityTrimStart(4, 28, 4);
    ASSERT_EQ("RYBDHV", seq.bases());
    ASSERT_EQ(
        QualVec({30,30,30,30,30,30}), seq.qualityScores()
    );
    ASSERT_EQ(6, seq.length());

    seq.qualityTrimStart(4, 31, 1);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = mnseq;
    seq.qualityTrimStart(7, 30, 2);
    ASSERT_EQ("ATGCWSMKRYBDHV", seq.bases());
    ASSERT_EQ(
        QualVec({10,30,10,30,30,30,10,10,30,30,30,30,30,30}), seq.qualityScores()
    );
    ASSERT_EQ(14, seq.length());

    seq.qualityTrimStart(7, 30, 4);
    ASSERT_EQ("ATGCWSMKRYBDHV", seq.bases());
    ASSERT_EQ(
        QualVec({10,30,10,30,30,30,10,10,30,30,30,30,30,30}), seq.qualityScores()
    );
    ASSERT_EQ(14, seq.length());

    seq.qualityTrimStart(7, 30, 5);
    ASSERT_EQ("CWSMKRYBDHV", seq.bases());
    ASSERT_EQ(
        QualVec({30,30,30,10,10,30,30,30,30,30,30}), seq.qualityScores()
    );
    ASSERT_EQ(11, seq.length());

    seq = mnseq;
    seq.qualityTrimStart(7, 30, 6);
    ASSERT_EQ("KRYBDHV", seq.bases());
    ASSERT_EQ(
        QualVec({10,30,30,30,30,30,30}), seq.qualityScores()
    );
    ASSERT_EQ(7, seq.length());

    seq = mnseq;
    seq.qualityTrimStart(7, 30, 7);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = mnseq;
    seq.qualityTrimStart(14, 28, 6);
    ASSERT_EQ("ATGCWSMKRYBDHV", seq.bases());
    ASSERT_EQ(
        QualVec({10,30,10,30,30,30,10,10,30,30,30,30,30,30}), seq.qualityScores()
    );
    ASSERT_EQ(14, seq.length());

    seq.qualityTrimStart(14, 28, 10);
    ASSERT_EQ("ATGCWSMKRYBDHV", seq.bases());
    ASSERT_EQ(
        QualVec({10,30,10,30,30,30,10,10,30,30,30,30,30,30}), seq.qualityScores()
    );
    ASSERT_EQ(14, seq.length());

    seq.qualityTrimStart(14, 28, 11);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());
}

TEST_F(NucleotideSequenceTest, qualityTrimEnd) {
    // Empty sequence.
    NucleotideSequence seq("");
    seq.qualityTrimEnd(1, 28, 1);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    ASSERT_THROW(seq.qualityTrimEnd(1, 28, 2), runtime_error);
    ASSERT_THROW(seq.qualityTrimEnd(0, 28, 0), runtime_error);

    // Sequences of length 1.
    seq = NucleotideSequence("A", {30}, "id1", "id2");
    seq.qualityTrimEnd(1, 28, 1);
    ASSERT_EQ("A", seq.bases());
    ASSERT_EQ(QualVec({30}), seq.qualityScores());
    ASSERT_EQ(1, seq.length());

    seq.qualityTrimEnd(1, 30, 1);
    ASSERT_EQ("A", seq.bases());
    ASSERT_EQ(QualVec({30}), seq.qualityScores());
    ASSERT_EQ(1, seq.length());

    seq.qualityTrimEnd(1, 31, 0);
    ASSERT_EQ("A", seq.bases());
    ASSERT_EQ(QualVec({30}), seq.qualityScores());
    ASSERT_EQ(1, seq.length());

    seq.qualityTrimEnd(1, 31, 1);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = NucleotideSequence("A", {30}, "id1", "id2");
    seq.qualityTrimEnd(2, 28, 1);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    // Multi-nucleotide sequences.
    NucleotideSequence mnseq(
        "VHDBYRKMSWCGTA", {30,30,30,30,30,30,10,10,30,30,30,10,30,10},
        "id1", "id2"
    );

    seq = mnseq;
    seq.qualityTrimEnd(1, 10, 1);
    ASSERT_EQ("VHDBYRKMSWCGTA", seq.bases());
    ASSERT_EQ(
        QualVec({30,30,30,30,30,30,10,10,30,30,30,10,30,10}), seq.qualityScores()
    );
    ASSERT_EQ(14, seq.length());

    seq.qualityTrimEnd(1, 28, 1);
    ASSERT_EQ("VHDBYRKMSWCGT", seq.bases());
    ASSERT_EQ(
        QualVec({30,30,30,30,30,30,10,10,30,30,30,10,30}), seq.qualityScores()
    );
    ASSERT_EQ(13, seq.length());

    seq.qualityTrimEnd(1, 31, 1);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = mnseq;
    seq.qualityTrimEnd(4, 10, 2);
    ASSERT_EQ("VHDBYRKMSWCGTA", seq.bases());
    ASSERT_EQ(
        QualVec({30,30,30,30,30,30,10,10,30,30,30,10,30,10}), seq.qualityScores()
    );
    ASSERT_EQ(14, seq.length());

    seq.qualityTrimEnd(4, 28, 2);
    ASSERT_EQ("VHDBYRKMSWCGTA", seq.bases());
    ASSERT_EQ(
        QualVec({30,30,30,30,30,30,10,10,30,30,30,10,30,10}), seq.qualityScores()
    );
    ASSERT_EQ(14, seq.length());

    seq.qualityTrimEnd(4, 28, 3);
    ASSERT_EQ("VHDBYRKMSWCGT", seq.bases());
    ASSERT_EQ(
        QualVec({30,30,30,30,30,30,10,10,30,30,30,10,30}), seq.qualityScores()
    );
    ASSERT_EQ(13, seq.length());

    seq.qualityTrimEnd(4, 28, 4);
    ASSERT_EQ("VHDBYR", seq.bases());
    ASSERT_EQ(
        QualVec({30,30,30,30,30,30}), seq.qualityScores()
    );
    ASSERT_EQ(6, seq.length());

    seq.qualityTrimEnd(4, 31, 1);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = mnseq;
    seq.qualityTrimEnd(7, 30, 2);
    ASSERT_EQ("VHDBYRKMSWCGTA", seq.bases());
    ASSERT_EQ(
        QualVec({30,30,30,30,30,30,10,10,30,30,30,10,30,10}), seq.qualityScores()
    );
    ASSERT_EQ(14, seq.length());

    seq.qualityTrimEnd(7, 30, 4);
    ASSERT_EQ("VHDBYRKMSWCGTA", seq.bases());
    ASSERT_EQ(
        QualVec({30,30,30,30,30,30,10,10,30,30,30,10,30,10}), seq.qualityScores()
    );
    ASSERT_EQ(14, seq.length());

    seq.qualityTrimEnd(7, 30, 5);
    ASSERT_EQ("VHDBYRKMSWC", seq.bases());
    ASSERT_EQ(
        QualVec({30,30,30,30,30,30,10,10,30,30,30}), seq.qualityScores()
    );
    ASSERT_EQ(11, seq.length());

    seq = mnseq;
    seq.qualityTrimEnd(7, 30, 6);
    ASSERT_EQ("VHDBYRK", seq.bases());
    ASSERT_EQ(
        QualVec({30,30,30,30,30,30,10}), seq.qualityScores()
    );
    ASSERT_EQ(7, seq.length());

    seq = mnseq;
    seq.qualityTrimEnd(7, 30, 7);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());

    seq = mnseq;
    seq.qualityTrimEnd(14, 28, 6);
    ASSERT_EQ("VHDBYRKMSWCGTA", seq.bases());
    ASSERT_EQ(
        QualVec({30,30,30,30,30,30,10,10,30,30,30,10,30,10}), seq.qualityScores()
    );
    ASSERT_EQ(14, seq.length());

    seq.qualityTrimEnd(14, 28, 10);
    ASSERT_EQ("VHDBYRKMSWCGTA", seq.bases());
    ASSERT_EQ(
        QualVec({30,30,30,30,30,30,10,10,30,30,30,10,30,10}), seq.qualityScores()
    );
    ASSERT_EQ(14, seq.length());

    seq.qualityTrimEnd(14, 28, 11);
    ASSERT_EQ("", seq.bases());
    ASSERT_EQ(QualVec(), seq.qualityScores());
    ASSERT_EQ(0, seq.length());
}

