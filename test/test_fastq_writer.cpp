/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <sstream>
#include <stdexcept>
using std::string;
using std::ostringstream;
using std::range_error;

#include "gtest/gtest.h"
#include "fastq_writer.hpp"


/*
 * Define the test fixture class.
 */
class FASTQWriterTest: public testing::Test {
};


/*
 * Define the tests.
 */

/*
 * Tests FASTQ output.
 */
TEST_F(FASTQWriterTest, testWrite) {
    // Test an "empty" FASTQWriter.
    FASTQWriter empty(33);
    ASSERT_THROW(empty.write(NucleotideSequence()), FASTQNoOutput);

    ostringstream str_out;
    FASTQWriter writer(str_out, 33);
    
    // A sequence with a simple title.
    NucleotideSequence seq("GCAT", {28,29,30,31}, "seq1", "", -1);
    string exp_output = R"(@seq1
GCAT
+
=>?@)";
    writer.write(seq);
    EXPECT_EQ(exp_output, str_out.str());

    // A sequence with an Illumina-format title.
    writer.reset(str_out);
    str_out.str("");
    seq = NucleotideSequence("GCAT", {28,29,30,31}, "IDstr1", "IDstr2", 1);
    exp_output = R"(@IDstr1 1:IDstr2
GCAT
+
=>?@)";
    writer.write(seq);
    EXPECT_EQ(exp_output, str_out.str());

    // Multiple sequences
    writer.reset(str_out);
    str_out.str("");
    seq = NucleotideSequence("GCAT", {28,29,30,31}, "seq1", "", -1);
    writer.write(seq);
    seq = NucleotideSequence("SYK", {16,17,18}, "seq2", "", -1);
    writer.write(seq);
    exp_output = R"(@seq1
GCAT
+
=>?@
@seq2
SYK
+
123)";
    EXPECT_EQ(exp_output, str_out.str());
}

