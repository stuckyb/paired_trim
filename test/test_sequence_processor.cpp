/*
 * Copyright (C) 2019 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <sstream>
#include <stdexcept>
using std::string;
using std::ostringstream;
using std::range_error;

#include "gtest/gtest.h"
#include "sequences_processor.hpp"
#include "run_config.hpp"


/*
 * Define the test fixture class.
 */
class SequenceProcessorTest: public testing::Test {
protected:
    SequenceProcessorTest(): seq_processor(conf) {}

    void SetUp() override {
        // Set up a basic run configuration with no input or output configured
        // and with all sequence trimming and filtering disabled.
        conf.qual_offset = 33;
        conf.min_qual = 0;
        conf.win_size = 1;
        conf.min_highqual_cnt = 0;
        conf.r1_trim_5p = 0;
        conf.r1_trim_3p = 0;
        conf.r2_trim_5p = 0;
        conf.r2_trim_3p = 0;
        conf.align_trim = false;
        conf.min_length = 1;
        conf.r1_adapter = NucleotideSequence("");
        conf.r2_adapter = NucleotideSequence("");
    }

    /*
     * Resets the ostringstream output streams and FASTQWriters for paired
     * sequence read processing.
     */
    void clearPairedWriteBuffers(
        ostringstream &r1_out, ostringstream &r2_out,
        ostringstream &r1_singltn_out, ostringstream &r2_singltn_out
    ) {
        r1_out.str("");
        r2_out.str("");
        r1_singltn_out.str("");
        r2_singltn_out.str("");
        conf.r1_writer.reset(r1_out);
        conf.r2_writer.reset(r2_out);
        conf.r1_singltns_writer.reset(r1_singltn_out);
        conf.r2_singltns_writer.reset(r2_singltn_out);
    }

    RunConfig conf;
    SequenceProcessor seq_processor;
};


/*
 * Define the tests.
 */

/*
 * Tests basic single-read processing functionality.
 */
TEST_F(SequenceProcessorTest, testRunSingleReads_Basic) {
    ostringstream str_out;

    conf.is_paired = false;

    // Simplest case of a single input sequence and no trimming or filtering.
    // The quality values of the input sequence are {28,29,30,31}.
    conf.r1_reader.setInput(R"(@seq1
GCAT
+
=>?@)");
    str_out.str("");
    conf.r1_writer.reset(str_out);

    string exp_output = R"(@seq1
GCAT
+
=>?@)";
    seq_processor.run();
    ASSERT_EQ(exp_output, str_out.str());

    // Multiple input sequences and no trimming or filtering.  The quality
    // values of the input sequences are {28,29,30,31} and {26,24,18,12,10}.
    conf.r1_reader.setInput(R"(@seq1
GCAT
+
=>?@
@seq2
STACK
+
;93-+)");
    str_out.str("");
    conf.r1_writer.reset(str_out);

    exp_output = R"(@seq1
GCAT
+
=>?@
@seq2
STACK
+
;93-+)";
    seq_processor.run();
    ASSERT_EQ(exp_output, str_out.str());
}


/*
 * Tests single-read processing functionality with quality trimming and length
 * filtering.
 */
TEST_F(SequenceProcessorTest, testRunSingleReads_LengthAndQual) {
    ostringstream str_out;

    conf.is_paired = false;

    // Same two input sequences with length filtering.
    conf.min_length = 4;
    conf.r1_reader.setInput(R"(@seq1
GCAT
+
=>?@
@seq2
STACK
+
;93-+)");
    str_out.str("");
    conf.r1_writer.reset(str_out);

    string exp_output = R"(@seq1
GCAT
+
=>?@
@seq2
STACK
+
;93-+)";
    seq_processor.run();
    ASSERT_EQ(exp_output, str_out.str());

    conf.min_length = 5;
    conf.r1_reader.setInput(R"(@seq1
GCAT
+
=>?@
@seq2
STACK
+
;93-+)");
    str_out.str("");
    conf.r1_writer.reset(str_out);

    exp_output = R"(@seq2
STACK
+
;93-+)";
    seq_processor.run();
    ASSERT_EQ(exp_output, str_out.str());

    conf.min_length = 6;
    conf.r1_reader.setInput(R"(@seq1
GCAT
+
=>?@
@seq2
STACK
+
;93-+)");
    str_out.str("");
    conf.r1_writer.reset(str_out);

    exp_output = "";
    seq_processor.run();
    ASSERT_EQ(exp_output, str_out.str());

    // Same two input sequences with both quality trimming and length
    // filtering.  Quality scores: {28,29,30,31} and {26,24,18,12,10}.
    conf.min_length = 4;
    conf.min_qual = 18;
    conf.win_size = 3;
    conf.min_highqual_cnt = 2;

    conf.r1_reader.setInput(R"(@seq1
GCAT
+
=>?@
@seq2
STACK
+
;93-+)");
    str_out.str("");
    conf.r1_writer.reset(str_out);

    exp_output = R"(@seq1
GCAT
+
=>?@
@seq2
STAC
+
;93-)";
    seq_processor.run();
    ASSERT_EQ(exp_output, str_out.str());

    conf.min_length = 4;
    conf.min_qual = 20;
    conf.win_size = 3;
    conf.min_highqual_cnt = 2;

    conf.r1_reader.setInput(R"(@seq1
GCAT
+
=>?@
@seq2
STACK
+
;93-+)");
    str_out.str("");
    conf.r1_writer.reset(str_out);

    exp_output = R"(@seq1
GCAT
+
=>?@)";
    seq_processor.run();
    ASSERT_EQ(exp_output, str_out.str());
}


/*
 * Tests single-read processing functionality with explicit end trimming.
 */
TEST_F(SequenceProcessorTest, testRunSingleReads_EndTrimming) {
    ostringstream str_out;

    // Same two input sequences with explicit end trimming.  Quality scores:
    // {28,29,30,31} and {26,24,18,12,10}.
    conf.min_qual = 0;
    conf.win_size = 1;
    conf.min_highqual_cnt = 0;
    conf.r1_trim_5p = 1;
    conf.r1_trim_3p = 2;
    conf.min_length = 1;

    conf.r1_reader.setInput(R"(@seq1
GCAT
+
=>?@
@seq2
STACK
+
;93-+)");
    str_out.str("");
    conf.r1_writer.reset(str_out);

    string exp_output = R"(@seq1
C
+
>
@seq2
TA
+
93)";
    seq_processor.run();
    ASSERT_EQ(exp_output, str_out.str());

    // Same two input sequences with explicit end trimming that completely
    // eliminates one sequence.  Quality scores:
    // {28,29,30,31} and {26,24,18,12,10}.
    conf.min_qual = 0;
    conf.win_size = 1;
    conf.min_highqual_cnt = 0;
    conf.r1_trim_5p = 1;
    conf.r1_trim_3p = 3;
    conf.min_length = 1;

    conf.r1_reader.setInput(R"(@seq1
GCAT
+
=>?@
@seq2
STACK
+
;93-+)");
    str_out.str("");
    conf.r1_writer.reset(str_out);

    exp_output = R"(@seq2
T
+
9)";
    seq_processor.run();
    ASSERT_EQ(exp_output, str_out.str());
}


/*
 * Tests basic paired-read processing functionality.
 */
TEST_F(SequenceProcessorTest, testRunPairedReads_Basic) {
    ostringstream r1_out, r2_out;
    ostringstream r1_singltn_out, r2_singltn_out;

    conf.is_paired = true;

    // A case where R1 has more reads than R2.
    conf.r1_reader.setInput(R"(@seq1
GCAT
+
=>?@)");
    conf.r2_reader.setInput("");
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    ASSERT_THROW(seq_processor.run(), SequenceProcessingError);

    // A case where R2 has more reads than R1.
    conf.r1_reader.setInput("");
    conf.r2_reader.setInput(R"(@seq1
GCAT
+
=>?@)");
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    ASSERT_THROW(seq_processor.run(), SequenceProcessingError);

    // Paired sequences, no filtering or trimming.
    conf.r1_reader.setInput(R"(@seq1
GCAT
+
=>?@)");
    conf.r2_reader.setInput(R"(@seq1
STACK
+
;93-+)");
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ(R"(@seq1
GCAT
+
=>?@)", r1_out.str());
    ASSERT_EQ(R"(@seq1
STACK
+
;93-+)", r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());

    // Paired sequences, mismatched IDs.
    conf.r1_reader.setInput(R"(@seq1
GCAT
+
=>?@)");
    conf.r2_reader.setInput(R"(@seq_mismatch
STACK
+
;93-+)");
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    ASSERT_THROW(seq_processor.run(), SequenceProcessingError);
}


/*
 * Tests paired-read processing with length filtering and quality trimming.
 */
TEST_F(SequenceProcessorTest, testRunPairedReads_LengthAndQual) {
    ostringstream r1_out, r2_out;
    ostringstream r1_singltn_out, r2_singltn_out;

    conf.is_paired = true;

    // Paired sequences, length filtering and quality trimming.
    // Quality scores: {28,29,30,31} and {26,24,18,12,10}.
    conf.min_length = 4;
    conf.min_qual = 18;
    conf.win_size = 3;
    conf.min_highqual_cnt = 2;

    conf.r1_reader.setInput(R"(@seq1
GCAT
+
=>?@)");
    conf.r2_reader.setInput(R"(@seq1
STACK
+
;93-+)");
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ(R"(@seq1
GCAT
+
=>?@)", r1_out.str());
    ASSERT_EQ(R"(@seq1
STAC
+
;93-)", r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());

    // Paired sequences, length filtering and quality trimming.
    // Quality scores: {28,29,30,31} and {26,24,18,12,10}.
    conf.min_length = 4;
    conf.min_qual = 20;
    conf.win_size = 3;
    conf.min_highqual_cnt = 2;

    conf.r1_reader.setInput(R"(@seq1
GCAT
+
=>?@)");
    conf.r2_reader.setInput(R"(@seq1
STACK
+
;93-+)");
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ("", r1_out.str());
    ASSERT_EQ("", r2_out.str());
    ASSERT_EQ(R"(@seq1
GCAT
+
=>?@)", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());

    // Paired sequences, length filtering and quality trimming, but with the
    // previous R1 and R2 switched.
    // Quality scores: {26,24,18,12,10} and {28,29,30,31}.
    conf.min_length = 4;
    conf.min_qual = 18;
    conf.win_size = 3;
    conf.min_highqual_cnt = 2;

    conf.r1_reader.setInput(R"(@seq1
STACK
+
;93-+)");
    conf.r2_reader.setInput(R"(@seq1
GCAT
+
=>?@)");
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ(R"(@seq1
STAC
+
;93-)", r1_out.str());
    ASSERT_EQ(R"(@seq1
GCAT
+
=>?@)", r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());

    // Paired sequences, length filtering and quality trimming, but with the
    // previous R1 and R2 switched.
    // Quality scores: {26,24,18,12,10} and {28,29,30,31}.
    conf.min_length = 4;
    conf.min_qual = 20;
    conf.win_size = 3;
    conf.min_highqual_cnt = 2;

    conf.r1_reader.setInput(R"(@seq1
STACK
+
;93-+)");
    conf.r2_reader.setInput(R"(@seq1
GCAT
+
=>?@)");
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ("", r1_out.str());
    ASSERT_EQ("", r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ(R"(@seq1
GCAT
+
=>?@)", r2_singltn_out.str());
}


/*
 * Tests paired-read processing with explicit end trimming.
 */
TEST_F(SequenceProcessorTest, testRunPairedReads_EndTrimming) {
    ostringstream r1_out, r2_out;
    ostringstream r1_singltn_out, r2_singltn_out;

    conf.is_paired = true;

    conf.r1_trim_5p = 0;
    conf.r1_trim_3p = 0;
    conf.r2_trim_5p = 0;
    conf.r2_trim_3p = 0;

    // Quality scores: {28,29,30,31,28,29,30,31,28} and
    // {26,24,18,12,10,26,24,18}.
    string r1_input = R"(@seq1
GCATACTGA
+
=>?@=>?@=)";
    string r2_input = R"(@seq1
AGTATGCG
+
;93-+;93)";

    // Paired sequences, no explicit trimming.
    conf.r1_reader.setInput(r1_input);
    conf.r2_reader.setInput(r2_input);
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ(r1_input, r1_out.str());
    ASSERT_EQ(r2_input, r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());

    // Test all end trimming options.
    conf.r1_trim_5p = 1;
    conf.r1_trim_3p = 2;
    conf.r2_trim_5p = 3;
    conf.r2_trim_3p = 4;

    conf.r1_reader.setInput(r1_input);
    conf.r2_reader.setInput(r2_input);
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ(R"(@seq1
CATACT
+
>?@=>?)", r1_out.str());
    ASSERT_EQ(R"(@seq1
A
+
-)", r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());

    // Test trimming that completely deletes a sequence.
    conf.r2_trim_3p = 5;

    conf.r1_reader.setInput(r1_input);
    conf.r2_reader.setInput(r2_input);
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ("", r1_out.str());
    ASSERT_EQ("", r2_out.str());
    ASSERT_EQ(R"(@seq1
CATACT
+
>?@=>?)", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());
}


/*
 * Tests paired-read processing with alignment-guided trimming.
 */
TEST_F(SequenceProcessorTest, testRunPairedReads_AlignTrim) {
    ostringstream r1_out, r2_out;
    ostringstream r1_singltn_out, r2_singltn_out;

    conf.is_paired = true;

    // Paired sequences, no alignment trimming.  Note that both these sequences
    // would be trimmed if alignment trimming were enabled.
    // Quality scores: {28,29,30,31,28,29,30,31,28} and
    // {26,24,18,12,10,26,24,18}.
    // When the R2 sequence is reverse complemented, they align as follows:
    // -GCATACTGA
    // CGCATACT--
    conf.r1_reader.setInput(R"(@seq1
GCATACTGA
+
=>?@=>?@=)");
    conf.r2_reader.setInput(R"(@seq1
AGTATGCG
+
;93-+;93)");
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ(R"(@seq1
GCATACTGA
+
=>?@=>?@=)", r1_out.str());
    ASSERT_EQ(R"(@seq1
AGTATGCG
+
;93-+;93)", r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());

    // Enable alignment trimming and process the same sequences again.
    // When the R2 sequence is reverse complemented, they align as follows:
    // -GCATACTGA
    // CGCATACT--
    conf.align_trim = true;

    conf.r1_reader.setInput(R"(@seq1
GCATACTGA
+
=>?@=>?@=)");
    conf.r2_reader.setInput(R"(@seq1
AGTATGCG
+
;93-+;93)");
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ(R"(@seq1
GCATACT
+
=>?@=>?)", r1_out.str());
    ASSERT_EQ(R"(@seq1
AGTATGC
+
;93-+;9)", r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());

    // Reverse the sequence order and reverse complement the sequences, which
    // should result in no trimming.  When the new R2 is reverse complemented,
    // the alignment will be:
    // CGCATACT--
    // -GCATACTGA
    conf.align_trim = true;

    conf.r1_reader.setInput(R"(@seq1
CGCATACT
+
;93-+;93)");
    conf.r2_reader.setInput(R"(@seq1
TCAGTATGC
+
=>?@=>?@=)");
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ(R"(@seq1
CGCATACT
+
;93-+;93)", r1_out.str());
    ASSERT_EQ(R"(@seq1
TCAGTATGC
+
=>?@=>?@=)", r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());

    // A pair of sequences with overhanging 3' regions, but an alignment that
    // is indistinguishable from a random alignment.
    // When the R2 sequence is reverse complemented, they align as follows:
    // -ATACTGA
    // CATACT--
    conf.align_trim = true;

    conf.r1_reader.setInput(R"(@seq1
ATACTGA
+
=>?@=>?)");
    conf.r2_reader.setInput(R"(@seq1
AGTATG
+
;93-+;)");
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ(R"(@seq1
ATACTGA
+
=>?@=>?)", r1_out.str());
    ASSERT_EQ(R"(@seq1
AGTATG
+
;93-+;)", r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());

}


/*
 * Tests explicit adapter trimming.
 */
TEST_F(SequenceProcessorTest, testDoAdapterTrimming) {
    ostringstream r1_out, r2_out;
    ostringstream r1_singltn_out, r2_singltn_out;

    conf.is_paired = true;
    conf.align_trim = true;

    // Paired sequences, no trimming of any kind.
    // When the R2 sequence is reverse complemented, they align as follows:
    // GCATACTGAA--
    // -CGTA-TGACTT
    // The score is much too low to trigger alignment-guided trimming (and the
    // overhang is on the wrong ends).
    string r1_input = R"(@seq1
GCATACTGAA
+
=>?@=>?@==)";
    string r2_input = R"(@seq1
AAGTCATACG
+
;93-+;93-+)";

    conf.r1_reader.setInput(r1_input);
    conf.r2_reader.setInput(r2_input);
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ(r1_input, r1_out.str());
    ASSERT_EQ(r2_input, r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());

    // Set adapter sequences that match at the very beginning.
    conf.r1_adapter = NucleotideSequence("GCATAC");
    conf.r2_adapter = NucleotideSequence("AAGTCA");

    conf.r1_reader.setInput(r1_input);
    conf.r2_reader.setInput(r2_input);
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ("", r1_out.str());
    ASSERT_EQ("", r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());

    // Set adapter sequences that match in the middle.
    conf.r1_adapter = NucleotideSequence("TACTG");
    conf.r2_adapter = NucleotideSequence("GTCAT");

    conf.r1_reader.setInput(r1_input);
    conf.r2_reader.setInput(r2_input);
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ(R"(@seq1
GCA
+
=>?)", r1_out.str());
    ASSERT_EQ(R"(@seq1
AA
+
;9)", r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());

    // Set adapter sequences that match with an ambiguous base.  The mean
    // sequence identity for both adapter alignments should be 0.917.
    conf.r1_adapter = NucleotideSequence("CATNCTGAA");
    conf.r2_adapter = NucleotideSequence("AGTCNTACG");

    conf.r1_reader.setInput(r1_input);
    conf.r2_reader.setInput(r2_input);
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ(R"(@seq1
G
+
=)", r1_out.str());
    ASSERT_EQ(R"(@seq1
A
+
;)", r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());

    // Set adapter sequences that do not match well enough to trigger trimming.
    conf.r1_adapter = NucleotideSequence("TAAAGCC");
    conf.r2_adapter = NucleotideSequence("TAAAGCC");

    conf.r1_reader.setInput(r1_input);
    conf.r2_reader.setInput(r2_input);
    clearPairedWriteBuffers(r1_out, r2_out, r1_singltn_out, r2_singltn_out);

    seq_processor.run();
    ASSERT_EQ(R"(@seq1
GCATACTGAA
+
=>?@=>?@==)", r1_out.str());
    ASSERT_EQ(R"(@seq1
AAGTCATACG
+
;93-+;93-+)", r2_out.str());
    ASSERT_EQ("", r1_singltn_out.str());
    ASSERT_EQ("", r2_singltn_out.str());
}

